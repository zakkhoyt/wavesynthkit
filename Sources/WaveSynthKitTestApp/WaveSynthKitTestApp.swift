//
//  WaveSynthKitTestApp.swift
//  WaveSynthKit
//
//  Created by Zakk Hoyt.
//  Copyright © 2023 Zakk Hoyt. All rights reserved.
//

import Foundation
import os
import WaveSynthKit

@main
public struct WaveSynthKitTestApp {
    public private(set) var text = "Hello, from WaveSynthKitTestApp!"
    public static func main() async {
        //        guard let voice = WaveSynthHelper.shared.xVoice else {
        //            return
        //        }
        print(WaveSynthKitTestApp().text)
        
        func askNote() -> String {
            let notes = ["a", "b", "c", "d", "e", "f", "g"]
            print("Select a note: [\(notes.joined(separator: ", "))]")
            let note = readLine()
            return note ?? ""
        }
                
        func readSTDIN() -> String? {
            var input: String?

            while let line = readLine() {
                if input == nil {
                    input = line
                } else {
                    input! += "\n" + line
                }
            }

            return input
        }
//        let task = readSTDIN()
//        let task = readLine()
//        let note = askNote()
//        print("you entered \(note)")

//        print("Enter a duration.")
//        let duration = TimeInterval(readLine() ?? "") ?? 0.25
//        print("you entered \(duration)")

        let duration = TimeInterval(0.4)
        
        let noteNames: [Note.Name] = [
            //            .c7, .c6, .c5, .c6, .c7, .c8
            .c7, .c6, .c5, .a5, .g5
//            .a7, .d7, .e7
        ]
        
        for voice in WaveSynthHelper.shared.voices {
            for noteName in noteNames {
                await WaveSynthHelper.shared.play(
                    frequency: Notes.note(name: noteName).frequency,
                    voice: voice,
                    for: voice.name == "xVoice" || voice.name == "Harmony" ? duration / 2 : duration
                )
            }
        }
    }
}

class WaveSynthHelper {
    static let shared = WaveSynthHelper()
    var waveSynth = WaveSynthesizer(sampleRate: 11025)
    
    enum VoiceKeys: String {
        case pianoVoiceKey = "Piano"
        case pianoHVoiceKey = "Piano Harmony"
        case xVoiceKey = "xVoice"
        case yVoiceKey = "yVoice"
        case square = "Square"
        case harmony = "Harmony"
        case noiseKey = "Noise"
    }
    
    var voices: [Voice] = []
    
    init() {
        setup()
        
        // Get our singleton to init
        _ = Notes.note(at: 0)
    }
    
    private func setup() {
        waveSynth.start()
        
        let vibrato = Vibrato()
        vibrato.delay.value = 0
        vibrato.frequency.value = 10
        vibrato.isEnabled.enumValue = .yes
        
        let pop = Pop()
//        pop.delay.value
        pop.delay.value = 0
        pop.duration.value = 0.05
        
        let bitcrusher = BitCrusher()
        
        let sweep = Sweep()
        sweep.delay.value = 0
        
        let harmony = Harmony()
        harmony.isEnabled.enumValue = Enabled.yes
        harmony.steps.value = 3
        
        do {
            let voice = Voice(name: WaveSynthHelper.VoiceKeys.pianoHVoiceKey.rawValue)
            voice.waveformRenderer.waveform.enumValue = .square
            voice.waveformRenderer.square.dutyCycle.value = 0.2
//            voice.appendOptionalEffect(effect: vibrato)
//            voice.frequency.octave.value = 2
            voice.adsr.releaseDuration.value = 0.05
            waveSynth.addVoice(voice)
            voices.append(voice)
        }

        do {
            let voice = Voice(name: WaveSynthHelper.VoiceKeys.xVoiceKey.rawValue)
            voice.frequency = Frequency(frequency: 200, minimumFrequency: 27.5, maximumFrequency: 110)
            voice.adsr.releaseDuration.value = 0.2
            voice.waveformRenderer.waveform.enumValue = .sin
            voice.appendOptionalEffect(effect: pop)
            waveSynth.addVoice(voice)
            voices.append(voice)
        }
        
        do {
            let yVoice = Voice(name: WaveSynthHelper.VoiceKeys.yVoiceKey.rawValue)
            yVoice.adsr.releaseDuration.value = 0.05
            yVoice.waveformRenderer.waveform.enumValue = .square
            waveSynth.addVoice(yVoice)
            voices.append(yVoice)
        }
        
        do {
            let voice = Voice(name: WaveSynthHelper.VoiceKeys.square.rawValue)
            voice.adsr.releaseDuration.value = 0.05
            voice.waveformRenderer.waveform.enumValue = .sawtooth
            voice.frequency.octave.value = -3
            voice.appendOptionalEffect(effect: sweep)
            waveSynth.addVoice(voice)
            voices.append(voice)
        }
        
        do {
            let voice = Voice(name: WaveSynthHelper.VoiceKeys.pianoVoiceKey.rawValue)
//            voice.waveformRenderer.waveform.enumValue = .triangle
//            voice.appendOptionalEffect(effect: vibrato)
//            voice.frequency.octave.value = 2
//            voice.adsr.releaseDuration.value = 0.05
            voice.adsr.releaseDuration.value = 0.05
            voice.waveformRenderer.waveform.enumValue = .sawtoothFourier
            voice.frequency.octave.value = -3
            voice.appendOptionalEffect(effect: sweep)

            waveSynth.addVoice(voice)
            voices.append(voice)
        }
        
        do {
            let voice = Voice(name: WaveSynthHelper.VoiceKeys.harmony.rawValue)
            voice.waveformRenderer.waveform.enumValue = .sawtoothFourier
            voice.adsr.releaseDuration.value = 0.2
            voice.frequency.octave.value = -2
            voice.appendOptionalEffect(effect: pop)
//            voice.appendOptionalEffect(effect: harmony)
            waveSynth.addVoice(voice)
            voices.append(voice)
        }
        
        do {
            let voice = Voice(name: WaveSynthHelper.VoiceKeys.noiseKey.rawValue)
            voice.waveformRenderer.waveform.enumValue = .noise
            voice.adsr.releaseDuration.value = 0.05
            voice.frequency.octave.value = 3
            waveSynth.addVoice(voice)
            voices.append(voice)
        }
    }
}

extension WaveSynthHelper {
    @available(*, renamed: "play(frequency:voice:for:)")
    func play(
        frequency: Double,
        voice: Voice,
        for duration: TimeInterval
    ) async {
        voice.frequency.note = Note(frequency: frequency)
        voice.keyDown()
        return await withCheckedContinuation { continuation in
            DispatchQueue.global().asyncAfter(
                deadline: .now() + duration
            ) {
                voice.keyUp()
                DispatchQueue.global().asyncAfter(
                    deadline: .now() + voice.adsr.releaseDuration.value
                ) {
                    continuation.resume(returning: ())
                }
            }
        }
    }
}
