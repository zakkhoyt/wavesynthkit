//
//  WaveSynthesizer.swift
//  WaveSynthesizer
//
//  Created by Zakk Hoyt on 6/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  http://www.bfxr.net/

import Foundation

public protocol WaveSynthesizerAudioBufferDelegate: AnyObject {
    func waveSynthesizer(
        _ waveSynthesizer: WaveSynthesizer,
        audioBufferWasFilled buffer: [Float]
    )
}

public protocol WaveSynthesizerVisualBufferDelegate: AnyObject {
    func waveSynthesizer(
        _ waveSynthesizer: WaveSynthesizer,
        visualBufferWasFilled buffer: [Float]
    )
}

public class WaveSynthesizer: Codable {
    public var sequencer: MidiSequencer?
    public weak var audioBufferDelegate: WaveSynthesizerAudioBufferDelegate?
    public weak var visualBufferDelegate: WaveSynthesizerVisualBufferDelegate?

    // TODO: We dont' need to allocate this unless visualBufferDelegate is set
    private let visualBufferManager = BufferManager(bufferSize: 1024)
    
    public private(set) var voices: [String: Voice] = [:]
    public var sortedVoices: [Voice] {
        [Voice](voices.values).sorted {
            $0.name < $1.name
        }
    }
    
    public var sampleRate: Double {
        audioManager.sampleRate
    }
    
    private let audioManager: AudioManager
    
    public init(sampleRate: Double) {
        self.audioManager = AudioManager(sampleRate: sampleRate)
        audioManager.generatorBufferDataSource = self
        audioManager.generatorBufferDelegate = self
        visualBufferManager.delegate = self
    }
    
    private enum CodingKeys: String, CodingKey {
        case sampleRate
        case state
        case voices
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let sampleRate = try container.decode(Double.self, forKey: .sampleRate)
        self.state = try container.decode(State.self, forKey: .state)
        self.voices = try container.decode([String: Voice].self, forKey: .voices)
        
        self.audioManager = AudioManager(sampleRate: sampleRate)
        audioManager.generatorBufferDataSource = self
        audioManager.generatorBufferDelegate = self
        visualBufferManager.delegate = self

        for (_, value) in voices {
            value.timeDataSource = self
        }
    }
    
    deinit {
        stop()
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(sampleRate, forKey: .sampleRate)
        try container.encode(state, forKey: .state)
        try container.encode(voices, forKey: .voices)
    }

    public enum State: String, Codable {
        case stopped = "Stopped"
        case started = "Started"
    }

    public private(set) var state: State = .stopped
    
    public func start() {
        try? audioManager.start()
    }
    
    public func stop() {
        try? audioManager.stop()
    }
}

extension WaveSynthesizer {
    public func addVoice(_ voice: Voice) {
        voices[voice.name] = voice
        voice.timeDataSource = self
    }
}

extension WaveSynthesizer: AudioManagerGeneratorDataSource {
    func audioManager(
        _ audioManager: AudioManager,
        fillBuffer buffer: UnsafeMutablePointer<Float32>,
        timeIntervals: [TimeInterval],
        sampleRate: Double
    ) {
//        if let timeInterval = timeIntervals.first {
        ////            sequencer?.update(timeInterval: timeInterval)
//        }
        
        let unmutedVoices = voices.filter { $0.value.volume.isEnabled.enumValue == .yes }
        let unmutedVoicesCount = unmutedVoices.count
        let activeVoice = unmutedVoices.filter { $0.value.adsr.shouldCalculate }
        if activeVoice.isEmpty {
            // Do nothing. Still 100% CPU on sim (103-106)
            for i in 0..<timeIntervals.count {
                buffer[i] = 0
            }
        } else {
            for i in 0..<timeIntervals.count {
                let timeInterval = timeIntervals[i]
                buffer[i] = Float(voices.reduce(Double(0)) { $0 + $1.value.render(timeInterval: timeInterval, sampleRate: sampleRate) } / Double(unmutedVoicesCount))
            }
        }
    }
}

extension WaveSynthesizer: TimeDataSource {
    var timeInterval: TimeInterval {
        // TODO: I'm not sure this is good enought
        // Doesn't this latch to every 4XX samples?
        audioManager.calculatedTimeInterval
    }
}

extension WaveSynthesizer: AudioManagerGeneratorBufferDelegate {
    func audioManager(
        _ audioManager: AudioManager,
        bufferWasFilled buffer: UnsafeMutablePointer<Float32>,
        count: UInt32,
        timeInterval: TimeInterval
    ) {
        // If we have a visualBufferDelegage, go ahead and keep track of the buffers
        if let _ = visualBufferDelegate {
            visualBufferManager.addStuff(buffer: buffer, count: count)
        }
        
        guard let audioBufferDelegate else { return }

        var copiedBuffer = [Float](repeating: 0, count: Int(count))
        for i in 0..<Int(count) {
            copiedBuffer[i] = buffer[i]
        }
        
        DispatchQueue.main.async {
            audioBufferDelegate.waveSynthesizer(self, audioBufferWasFilled: copiedBuffer)
        }
    }
}

extension WaveSynthesizer: BufferManagerDelegate {
    func bufferManager(_ bufferManager: BufferManager, didSwapVisualBuffer buffer: [Float]) {
        guard let visualBufferDelegate else { return }
        DispatchQueue.main.async {
            visualBufferDelegate.waveSynthesizer(self, visualBufferWasFilled: buffer)
        }
    }
}
