//
//  Encodable+Json.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/19/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Encodable {
    var jsonDescription: String? {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(self)
            return String(data: data, encoding: .utf8)
        } catch {
            assertionFailure(error.localizedDescription)
            return nil
        }
    }
}
