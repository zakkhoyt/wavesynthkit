//
//  Note.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public struct Note: Codable, CustomStringConvertible {
    public private(set) var frequency: Double
    public private(set) var names: [String] = []
    public private(set) var octave: Int?
    public private(set) var keyIndex: Int?
    
    public var name: String { names.first ?? "" }
    
    public var description: String {
        var output = "frequency: \(String(format: "%.2f", frequency)) "
        if let keyIndex {
            output += "keyIndex: \(keyIndex) "
        }
        if let octave {
            output += "octave: \(octave) "
        }
        if !names.isEmpty {
            output += "names: \(names) "
        }
        return output
    }
    
    public init(frequency: Double) {
        self.frequency = frequency
    }
    
    init(
        frequency: Double,
        names: [String],
        octave: Int,
        keyIndex: Int
    ) {
        self.frequency = frequency
        self.names = names
        self.octave = octave
        self.keyIndex = 21 + keyIndex
    }
}

//// 88 Key piano starts at:
//    0 A0 27.5
//    1 A#0
//    2 B0
//    3 C1 32.7
//    39 C4 261.63
//    48 A4 440.00
//    87 C8 4186.03
//
//  Min index = 0
//  Max index = 111
//
//    index: 0 frequency: 27.50 note: A octave:0
//    index: 1 frequency: 29.14 note: A# octave:0
//    index: 2 frequency: 30.87 note: B octave:0
//    index: 3 frequency: 32.70 note: C octave:1
//    index: 4 frequency: 34.65 note: C# octave:1
//    index: 5 frequency: 36.71 note: D octave:1
//    index: 6 frequency: 38.89 note: D# octave:1
//    index: 7 frequency: 41.20 note: E octave:1
//    index: 8 frequency: 43.65 note: F octave:1
//    index: 9 frequency: 46.25 note: F# octave:1
//    index: 10 frequency: 49.00 note: G octave:1
//    index: 11 frequency: 51.91 note: G# octave:1
//    index: 12 frequency: 55.00 note: A octave:1
//    index: 13 frequency: 58.27 note: A# octave:1
//    index: 14 frequency: 61.74 note: B octave:1
//    index: 15 frequency: 65.41 note: C octave:2
//    index: 16 frequency: 69.30 note: C# octave:2
//    index: 17 frequency: 73.42 note: D octave:2
//    index: 18 frequency: 77.78 note: D# octave:2
//    index: 19 frequency: 82.41 note: E octave:2
//    index: 20 frequency: 87.31 note: F octave:2
//    index: 21 frequency: 92.50 note: F# octave:2
//    index: 22 frequency: 98.00 note: G octave:2
//    index: 23 frequency: 103.83 note: G# octave:2
//    index: 24 frequency: 110.00 note: A octave:2
//    index: 25 frequency: 116.54 note: A# octave:2
//    index: 26 frequency: 123.47 note: B octave:2
//    index: 27 frequency: 130.81 note: C octave:3
//    index: 28 frequency: 138.59 note: C# octave:3
//    index: 29 frequency: 146.83 note: D octave:3
//    index: 30 frequency: 155.56 note: D# octave:3
//    index: 31 frequency: 164.81 note: E octave:3
//    index: 32 frequency: 174.61 note: F octave:3
//    index: 33 frequency: 185.00 note: F# octave:3
//    index: 34 frequency: 196.00 note: G octave:3
//    index: 35 frequency: 207.65 note: G# octave:3
//    index: 36 frequency: 220.00 note: A octave:3
//    index: 37 frequency: 233.08 note: A# octave:3
//    index: 38 frequency: 246.94 note: B octave:3
//    index: 39 frequency: 261.63 note: C octave:4
//    index: 40 frequency: 277.18 note: C# octave:4
//    index: 41 frequency: 293.67 note: D octave:4
//    index: 42 frequency: 311.13 note: D# octave:4
//    index: 43 frequency: 329.63 note: E octave:4
//    index: 44 frequency: 349.23 note: F octave:4
//    index: 45 frequency: 370.00 note: F# octave:4
//    index: 46 frequency: 392.00 note: G octave:4
//    index: 47 frequency: 415.31 note: G# octave:4
//    index: 48 frequency: 440.00 note: A octave:4
//    index: 49 frequency: 466.16 note: A# octave:4
//    index: 50 frequency: 493.88 note: B octave:4
//    index: 51 frequency: 523.25 note: C octave:5
//    index: 52 frequency: 554.37 note: C# octave:5
//    index: 53 frequency: 587.33 note: D octave:5
//    index: 54 frequency: 622.26 note: D# octave:5
//    index: 55 frequency: 659.26 note: E octave:5
//    index: 56 frequency: 698.46 note: F octave:5
//    index: 57 frequency: 739.99 note: F# octave:5
//    index: 58 frequency: 783.99 note: G octave:5
//    index: 59 frequency: 830.61 note: G# octave:5
//    index: 60 frequency: 880.00 note: A octave:5
//    index: 61 frequency: 932.33 note: A# octave:5
//    index: 62 frequency: 987.77 note: B octave:5
//    index: 63 frequency: 1046.51 note: C octave:6
//    index: 64 frequency: 1108.73 note: C# octave:6
//    index: 65 frequency: 1174.66 note: D octave:6
//    index: 66 frequency: 1244.51 note: D# octave:6
//    index: 67 frequency: 1318.51 note: E octave:6
//    index: 68 frequency: 1396.92 note: F octave:6
//    index: 69 frequency: 1479.98 note: F# octave:6
//    index: 70 frequency: 1567.99 note: G octave:6
//    index: 71 frequency: 1661.22 note: G# octave:6
//    index: 72 frequency: 1760.01 note: A octave:6
//    index: 73 frequency: 1864.66 note: A# octave:6
//    index: 74 frequency: 1975.54 note: B octave:6
//    index: 75 frequency: 2093.01 note: C octave:7
//    index: 76 frequency: 2217.47 note: C# octave:7
//    index: 77 frequency: 2349.33 note: D octave:7
//    index: 78 frequency: 2489.02 note: D# octave:7
//    index: 79 frequency: 2637.03 note: E octave:7
//    index: 80 frequency: 2793.84 note: F octave:7
//    index: 81 frequency: 2959.97 note: F# octave:7
//    index: 82 frequency: 3135.98 note: G octave:7
//    index: 83 frequency: 3322.45 note: G# octave:7
//    index: 84 frequency: 3520.01 note: A octave:7
//    index: 85 frequency: 3729.32 note: A# octave:7
//    index: 86 frequency: 3951.08 note: B octave:7
//    index: 87 frequency: 4186.03 note: C octave:8
//    index: 88 frequency: 4434.94 note: C# octave:8
//    index: 89 frequency: 4698.66 note: D octave:8
//    index: 90 frequency: 4978.05 note: D# octave:8
//    index: 91 frequency: 5274.06 note: E octave:8
//    index: 92 frequency: 5587.68 note: F octave:8
//    index: 93 frequency: 5919.94 note: F# octave:8
//    index: 94 frequency: 6271.95 note: G octave:8
//    index: 95 frequency: 6644.90 note: G# octave:8
//    index: 96 frequency: 7040.03 note: A octave:8
//    index: 97 frequency: 7458.65 note: A# octave:8
//    index: 98 frequency: 7902.17 note: B octave:8
//    index: 99 frequency: 8372.06 note: C octave:9
//    index: 100 frequency: 8869.89 note: C# octave:9
//    index: 101 frequency: 9397.32 note: D octave:9
//    index: 102 frequency: 9956.11 note: D# octave:9
//    index: 103 frequency: 10548.13 note: E octave:9
//    index: 104 frequency: 11175.36 note: F octave:9
//    index: 105 frequency: 11839.88 note: F# octave:9
//    index: 106 frequency: 12543.92 note: G octave:9
//    index: 107 frequency: 13289.82 note: G# octave:9
//    index: 108 frequency: 14080.07 note: A octave:9
//    index: 109 frequency: 14917.31 note: A# octave:9
//    index: 110 frequency: 15804.35 note: B octave:9
//    index: 111 frequency: 16744.12 note: C octave:10

extension Note {
    public enum Name {
        // A singleton is okay here. There is no need to re-calculate these over and over.
        // But calculating them once is better than a look up table.
        public static let shared = Notes()
        
        public static let indexMin = 0
        public static let indexMax = 111
        
        case a0
        case aSharp0
        case bFlat0
        case b0
        case c1
        case cSharp1
        case dFlat1
        case d1
        case dSharp1
        case eFlat1
        case e1
        case f1
        case fSharp1
        case gFlat1
        case g1
        case gSharp1
        case aFlat1
        case a1
        case aSharp1
        case bFlat1
        case b1
        case c2
        case cSharp2
        case dFlat2
        case d2
        case dSharp2
        case eFlat2
        case e2
        case f2
        case fSharp2
        case gFlat2
        case g2
        case gSharp2
        case aFlat2
        case a2
        case aSharp2
        case bFlat2
        case b2
        case c3
        case cSharp3
        case dFlat3
        case d3
        case dSharp3
        case eFlat3
        case e3
        case f3
        case fSharp3
        case gFlat3
        case g3
        case gSharp3
        case aFlat3
        case a3
        case aSharp3
        case bFlat3
        case b3
        case c4
        case cSharp4
        case dFlat4
        case d4
        case dSharp4
        case eFlat4
        case e4
        case f4
        case fSharp4
        case gFlat4
        case g4
        case gSharp4
        case aFlat4
        case a4
        case aSharp4
        case bFlat4
        case b4
        case c5
        case cSharp5
        case dFlat5
        case d5
        case dSharp5
        case eFlat5
        case e5
        case f5
        case fSharp5
        case gFlat5
        case g5
        case gSharp5
        case aFlat5
        case a5
        case aSharp5
        case bFlat5
        case b5
        case c6
        case cSharp6
        case dFlat6
        case d6
        case dSharp6
        case eFlat6
        case e6
        case f6
        case fSharp6
        case gFlat6
        case g6
        case gSharp6
        case aFlat6
        case a6
        case aSharp6
        case bFlat6
        case b6
        case c7
        case cSharp7
        case dFlat7
        case d7
        case dSharp7
        case eFlat7
        case e7
        case f7
        case fSharp7
        case gFlat7
        case g7
        case gSharp7
        case aFlat7
        case a7
        case aSharp7
        case bFlat7
        case b7
        case c8
        case cSharp8
        case dFlat8
        case d8
        case dSharp8
        case eFlat8
        case e8
        case f8
        case fSharp8
        case gFlat8
        case g8
        case gSharp8
        case aFlat8
        case a8
        case aSharp8
        case bFlat8
        case b8
        case c9
        case cSharp9
        case dFlat9
        case d9
        case dSharp9
        case eFlat9
        case e9
        case f9
        case fSharp9
        case gFlat9
        case g9
        case gSharp9
        case aFlat9
        case a9
        case aSharp9
        case bFlat9
        case b9
        case c10
        
        public var index: Int {
            switch self {
            case .a0: return 0
            case .aSharp0: return 1
            case .bFlat0: return 1
            case .b0: return 2
            case .c1: return 3
            case .cSharp1: return 4
            case .dFlat1: return 4
            case .d1: return 5
            case .dSharp1: return 6
            case .eFlat1: return 6
            case .e1: return 7
            case .f1: return 8
            case .fSharp1: return 9
            case .gFlat1: return 9
            case .g1: return 10
            case .gSharp1: return 11
            case .aFlat1: return 11
            case .a1: return 12
            case .aSharp1: return 13
            case .bFlat1: return 13
            case .b1: return 14
            case .c2: return 15
            case .cSharp2: return 16
            case .dFlat2: return 16
            case .d2: return 17
            case .dSharp2: return 18
            case .eFlat2: return 18
            case .e2: return 19
            case .f2: return 20
            case .fSharp2: return 21
            case .gFlat2: return 21
            case .g2: return 22
            case .gSharp2: return 23
            case .aFlat2: return 23
            case .a2: return 24
            case .aSharp2: return 25
            case .bFlat2: return 25
            case .b2: return 26
            case .c3: return 27
            case .cSharp3: return 28
            case .dFlat3: return 28
            case .d3: return 29
            case .dSharp3: return 30
            case .eFlat3: return 30
            case .e3: return 31
            case .f3: return 32
            case .fSharp3: return 33
            case .gFlat3: return 33
            case .g3: return 34
            case .gSharp3: return 35
            case .aFlat3: return 35
            case .a3: return 36
            case .aSharp3: return 37
            case .bFlat3: return 37
            case .b3: return 38
            case .c4: return 39
            case .cSharp4: return 40
            case .dFlat4: return 40
            case .d4: return 41
            case .dSharp4: return 42
            case .eFlat4: return 42
            case .e4: return 43
            case .f4: return 44
            case .fSharp4: return 45
            case .gFlat4: return 45
            case .g4: return 46
            case .gSharp4: return 47
            case .aFlat4: return 47
            case .a4: return 48
            case .aSharp4: return 49
            case .bFlat4: return 49
            case .b4: return 50
            case .c5: return 51
            case .cSharp5: return 52
            case .dFlat5: return 52
            case .d5: return 53
            case .dSharp5: return 54
            case .eFlat5: return 54
            case .e5: return 55
            case .f5: return 56
            case .fSharp5: return 57
            case .gFlat5: return 57
            case .g5: return 58
            case .gSharp5: return 59
            case .aFlat5: return 59
            case .a5: return 60
            case .aSharp5: return 61
            case .bFlat5: return 61
            case .b5: return 62
            case .c6: return 63
            case .cSharp6: return 64
            case .dFlat6: return 64
            case .d6: return 65
            case .dSharp6: return 66
            case .eFlat6: return 66
            case .e6: return 67
            case .f6: return 68
            case .fSharp6: return 69
            case .gFlat6: return 69
            case .g6: return 70
            case .gSharp6: return 71
            case .aFlat6: return 71
            case .a6: return 72
            case .aSharp6: return 73
            case .bFlat6: return 73
            case .b6: return 74
            case .c7: return 75
            case .cSharp7: return 76
            case .dFlat7: return 76
            case .d7: return 77
            case .dSharp7: return 78
            case .eFlat7: return 78
            case .e7: return 79
            case .f7: return 80
            case .fSharp7: return 81
            case .gFlat7: return 81
            case .g7: return 82
            case .gSharp7: return 83
            case .aFlat7: return 83
            case .a7: return 84
            case .aSharp7: return 85
            case .bFlat7: return 85
            case .b7: return 86
            case .c8: return 87
            case .cSharp8: return 88
            case .dFlat8: return 88
            case .d8: return 89
            case .dSharp8: return 90
            case .eFlat8: return 90
            case .e8: return 91
            case .f8: return 92
            case .fSharp8: return 93
            case .gFlat8: return 93
            case .g8: return 94
            case .gSharp8: return 95
            case .aFlat8: return 95
            case .a8: return 96
            case .aSharp8: return 97
            case .bFlat8: return 97
            case .b8: return 98
            case .c9: return 99
            case .cSharp9: return 100
            case .dFlat9: return 100
            case .d9: return 101
            case .dSharp9: return 102
            case .eFlat9: return 102
            case .e9: return 103
            case .f9: return 104
            case .fSharp9: return 105
            case .gFlat9: return 105
            case .g9: return 106
            case .gSharp9: return 107
            case .aFlat9: return 107
            case .a9: return 108
            case .aSharp9: return 109
            case .bFlat9: return 109
            case .b9: return 110
            case .c10: return 111
            }
        }
    }
}

extension Notes {
    /// Expects an input in the range of midi notes (21-108). Index will be clipped into this range
    public static func note(
        name: Note.Name
    ) -> Note {
        note(at: name.index)
    }
}
