//
//  Square.swift
//  WaveSynth
//
//  Created by Zakk Hoyt on 6/25/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import Foundation

private let RMS = 0.707

public class Square: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .square
    
    public private(set) var dutyCycle = Parameter(name: "Duty Cycle", value: 0.5, minimum: 0, maximum: 1)
    
    public init() {}
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        if theta < 2.0 * Double.pi * dutyCycle.value {
            return RMS
        } else {
            return -RMS
        }
    }
}

public class SquareFourier: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .squareFourier
    public private(set) var fourierAccuracy = Parameter(name: "Fourier Accuracy", value: 6, minimum: 1, maximum: 20, snapTo: 1.0)
    public init() {}
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        var output: Double = 0
        for i in 0..<fourierAccuracy.intValue {
            var n = Double(i)
            if n == 0 { n = 1 }
            //            let scale: Double = 4 / Double.pi
            let constant = 1.0 / n
            if i % 2 == 0 { continue }
            let v: Double = sin(n * theta)
            output += constant * v
        }
        return output
    }
}
