//
//  Sine.swift
//  WaveSynthiOS
//
//  Created by Zakk Hoyt on 3/13/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

public class Sine: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .sin
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        sin(theta)
    }

    public init() {}
}
