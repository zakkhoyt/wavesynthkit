//
//  Waveform.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

// https://www.digitalflapjack.com/blog/2018/5/29/encoding-and-decoding-polymorphic-objects-in-swift
public enum WaveformType: Int, Codable, CaseIterable, CustomStringConvertible {
    case sin
    case square
    case squareFourier
    case triangle
    case triangleFourier
    case sawtooth
    case sawtoothFourier
    case noise
    case whiteNoise

    public var description: String {
        switch self {
        case .sin: return "Sine"
        case .square: return "Square"
        case .squareFourier: return "Square (Fourier)"
        case .triangle: return "Triangle"
        case .triangleFourier: return "Triangle (Fourier)"
        case .sawtooth: return "Sawtooth"
        case .sawtoothFourier: return "Sawtooth (Fourier)"
        case .noise: return "Noise"
        case .whiteNoise: return "White Noise"
        }
    }
    
    var waveCalculatableType: WaveCalculatable.Type {
        switch self {
        case .sin: return Sine.self
        case .square: return Square.self
        case .squareFourier: return SquareFourier.self
        case .triangle: return Triangle.self
        case .triangleFourier: return TriangleFourier.self
        case .sawtooth: return Sawtooth.self
        case .sawtoothFourier: return SawtoothFourier.self
        case .noise: return Noise.self
        case .whiteNoise: return WhiteNoise.self
        }
    }
}

public protocol WaveCalculatable: FrequencyModifier {
    var waveformType: WaveformType { get }
}
