//
//  Noise.swift
//  WaveSynth
//
//  Created by Zakk Hoyt on 6/24/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//
//  http://www.firstpr.com.au/dsp/pink-noise/
//  ftp://ftp-svr.eng.cam.ac.uk/pub/comp.speech/info/Colors_of_noise_pseudo_FAQ.txt
//
import Foundation

public class Noise: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .noise
    
    private var noise: Double = 0
    private var timeThreshold: TimeInterval = 0
    
    public required init() {}

    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        if frequency == 0 {
            return 0
        }
        
        if timeInterval > timeThreshold {
            let timePerCycle = 1.0 / frequency
            timeThreshold = timeInterval + TimeInterval(timePerCycle)
            var r = Double(arc4random_uniform(256)) / Double(255)
            r = r * 2.0 - 1.0
            noise = r
        }
        return noise
    }
}
