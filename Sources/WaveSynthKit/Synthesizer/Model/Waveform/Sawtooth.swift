//
//  Sawtooth.swift
//  WaveSynthiOS
//
//  Created by Zakk Hoyt on 3/10/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

public class Sawtooth: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .sawtooth
    
    public init() {}
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        let f = theta / timeInterval / 2.0 / Double.pi
        return 2.0 * (f * fmod(timeInterval, 1.0 / f)) - 1.0
    }
}

public class SawtoothFourier: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .sawtoothFourier
    
    public private(set) var fourierAccuracy = Parameter(name: "Fourier Accuracy", value: 3, minimum: 1, maximum: 20, snapTo: 1.0)
    
    public init() {}
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        var output: Double = 0
        for i in 1...fourierAccuracy.intValue {
            if i == 0 { continue }
            let n = Double(i)
            let constant: Double = 1 / n
            let v = sin(n * theta)
            output += constant * v
        }
        //        let scale: Double = (1 / 2) - (1 / Double.pi)
        return output
    }
}
