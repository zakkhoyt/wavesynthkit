//
//  Triangle.swift
//  WaveSynthiOS
//
//  Created by Zakk Hoyt on 3/10/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

public class Triangle: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .triangle
    
    public init() {}
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        asin(sin(theta)) / (Double.pi / 2.0)
    }
}

public class TriangleFourier: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .triangleFourier
    
    public private(set) var fourierAccuracy = Parameter(name: "Fourier Accuracy", value: 3, minimum: 1, maximum: 20, snapTo: 1.0)
    
    public init() {}
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        var output: Double = 0
        for i in 0..<fourierAccuracy.intValue {
            if i % 2 == 0 { continue }
            let n = Double(i)
            //            let scale: Double = 8 / pow(n, 2)
            let constant: Double = {
                let cTop: Double = pow(-1, (n - 1) / 2)
                let cBottom: Double = pow(n, 2)
                return cTop / cBottom
            }()
            let v: Double = sin(n * theta)
            output += constant * v
        }
        return output
    }
}
