//
//  WhiteNoise.swift
//  WaveSynth
//
//  Created by Zakk Hoyt on 7/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import Foundation

public class WhiteNoise: WaveCalculatable {
    public private(set) var waveformType: WaveformType = .whiteNoise
    
    public init() {}

    // A random value between -1.0 and 1.0
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        let threshold = 1024
        let random = Int(arc4random_uniform(UInt32(threshold))) // 0...1024
        var value = Double(random) / Double(threshold) // 0.0 ... 1.0
        value = value * 2.0 - 1.0 // -1.0 ... 1.0
        return value
    }
}
