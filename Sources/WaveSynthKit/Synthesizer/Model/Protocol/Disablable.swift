//
//  Disablable.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/23/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public protocol Disableable: AnyObject {
    var isEnabled: EnumParameter<Enabled> { get } // (value: Enabled.yes)
}
