//
//  Renderable.swift
//  WaveSynthesizer
//
//  Created by Zakk Hoyt on 6/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

// public protocol Copyable: AnyObject {
//    func copy() -> Self
// }
//
// extension Copyable {
//    func copy() -> Self {
//        return self
//    }
// }

public protocol AmplitudeModifier: Effect {
    func calculate(
        input: Double,
        frequency: Double,
        timeInterval: TimeInterval,
        sampleRate: Double
    ) -> Double
}

public protocol AmplitudeEffect: AmplitudeModifier {}

public protocol FrequencyModifier: Codable {
    func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double
}

public protocol FrequencyEffect: FrequencyModifier, Effect {}
