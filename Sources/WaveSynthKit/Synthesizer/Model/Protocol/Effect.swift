//
//  Effect.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/19/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public protocol Effect: Codable, Disableable {
    var effectType: EffectType { get }
    init()
}

extension Effect {
    public func randomize() {
        let parameters: [Parameter] = Mirror.reflectInstances(of: self)
        parameters.forEach {
            guard $0.name != "Enabled" else { return }
            guard $0.name != "Amplitude" else { return }
            $0.randomize()
        }
    }
}

public enum EffectType: String, Codable, CaseIterable {
    case waveformRenderer = "WaveformRenderer"
    case volume = "Volume"
    case vibrato = "Vibrato"
    case harmony = "Harmony"
    case frequency = "Frequency"
    
//    case transpose = "Transpose"

    case sweep = "Sweep"
    case pop = "Pop"
    case adsr = "ADSR"
    case roll = "Roll"
    case bitCrusher = "Bit Crusher"
    case adcCrusher = "ADC Crusher"
    
    public static let superflousTypes: [EffectType] = {
        [
            .vibrato,
            .sweep,
            .pop,
            .roll,
            .bitCrusher,
            .adcCrusher
        ]
    }()
    
    public var metaType: Effect.Type {
        switch self {
        case .waveformRenderer: return WaveformRenderer.self
        case .volume: return Volume.self
        case .vibrato: return Vibrato.self
        case .harmony: return Harmony.self
//        case .pitch: return Frequency.self
        case .frequency: return Frequency.self
//        case .transpose: return Transpose.self
        case .sweep: return Sweep.self
        case .pop: return Pop.self
        case .adsr: return ADSR.self
        case .roll: return Roll.self
        case .bitCrusher: return BitCrusher.self
        case .adcCrusher: return AdcCrusher.self
        }
    }
}

struct EffectWrapper {
    var effect: Effect
}

extension EffectWrapper: Codable {
    private enum CodingKeys: CodingKey {
        case type
        case effect
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let effectType = try container.decode(EffectType.self, forKey: .type)
        self.effect = try effectType.metaType.init(from: container.superDecoder(forKey: .effect))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(effect.effectType, forKey: .type)
        try effect.encode(to: container.superEncoder(forKey: .effect))
    }
}
