//
//  Randomizable.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/25/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public protocol Randomizable {
    func randomize()
}
