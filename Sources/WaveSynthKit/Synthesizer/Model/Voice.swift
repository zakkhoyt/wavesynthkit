//
//  Voice.swift
//  WaveSynthesizer
//
//  Created by Zakk Hoyt on 6/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

protocol TimeDataSource: AnyObject {
    var timeInterval: TimeInterval { get }
}

public class Voice: TimeDataSource, Codable {
    weak var timeDataSource: TimeDataSource?
    internal var timeInterval: TimeInterval {
        timeDataSource?.timeInterval ?? 0
    }
    
    // MARK: Public vars

    public let name: String
    public var adsr = ADSR()
    public private(set) var waveformRenderer = WaveformRenderer(waveformType: .sin)
    public private(set) var volume = Volume()
    public var frequency = Frequency()

    public var requiredEffects: [Effect] {
        [adsr, volume, frequency, waveformRenderer]
    }

    public private(set) var optionalEffects: [Effect] = []
    
    public func appendOptionalEffect(effect: Effect) {
        optionalEffects.append(effect)
    }
    
    public func removeOptionalEffect(at index: Int) {
        optionalEffects.remove(at: index)
    }

    // MARK: Internal vars
    
    var timedItems: [TimedItem] {
        var output: [TimedItem] = []
        
        if let optional = (optionalEffects.filter { $0 is TimedItem } as? [TimedItem]) {
            output.append(contentsOf: optional)
        }
        
        if let required = (requiredEffects.filter { $0 is TimedItem } as? [TimedItem]) {
            output.append(contentsOf: required)
        }

        return output
    }
    
    var amplitudeChangers: [AmplitudeEffect] {
        var output: [AmplitudeEffect] = [adsr, volume]
//        output.append(contentsOf: (requiredEffects.filter { $0 is AmplitudeEffect } as? [AmplitudeEffect] ?? []))
        output.append(contentsOf: (optionalEffects.filter { $0 is AmplitudeEffect } as? [AmplitudeEffect] ?? []))
        return output
    }
    
    var frequencyChangers: [FrequencyEffect] {
        optionalEffects.filter { $0 is FrequencyEffect } as? [FrequencyEffect] ?? []
    }

    // MARK: Private vars
    
    private var theta: Double = 0

    // We need a way to batch process these categories
    // effects amplitude
    // effects frequency
    // timed events (keyDown/keyUp)
    
    // MARK: Public constructors
    
    public init(
        name: String,
        note: Note,
        keyDown: Bool = false
    ) {
        self.name = name
        frequency.note = note

        commonSetup()
        
        if keyDown {
            self.keyDown()
        }
    }

    public init(name: String) {
        self.name = name
        commonSetup()
    }
    
    private func commonSetup() {}
    
    enum CodingKeys: String, CodingKey {
        case name
        case isMuted
        case note

        case items
        case effects
        
        case waveformRenderer
        case adsr
        case volume
        case frequency
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.frequency = try container.decode(Frequency.self, forKey: .frequency)
        self.waveformRenderer = try container.decode(WaveformRenderer.self, forKey: .waveformRenderer)
        self.adsr = try container.decode(ADSR.self, forKey: .adsr)
        self.volume = try container.decode(Volume.self, forKey: .volume)
        self.frequency = try container.decode(Frequency.self, forKey: .frequency)
        let effectWrappers = try container.decode([EffectWrapper].self, forKey: .effects)
        self.optionalEffects = effectWrappers.map { $0.effect }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(waveformRenderer, forKey: .waveformRenderer)
        try container.encode(adsr, forKey: .adsr)
        try container.encode(volume, forKey: .volume)
        try container.encode(frequency, forKey: .frequency)
        let effectWrappers: [EffectWrapper] = optionalEffects.map { EffectWrapper(effect: $0) }
        try container.encode(effectWrappers, forKey: .effects)
    }
    
    /// Call this functino when an instruments note should be played
    /// E.g.: When a user presses a piano key
    public func keyDown() {
        adsr.keyDown(timeInterval: timeInterval)
//        print("KeyDown for \(String(describing: adsr)) \(adsr.address)")
        for timedItem in timedItems {
//            print("KeyDown for \(String(describing: timedItem)) \(timedItem.address)")
            timedItem.keyDown(timeInterval: timeInterval)
        }
    }
    
    public func keyUp() {
        adsr.keyUp(timeInterval: timeInterval)
//        print("KeyUp for \(String(describing: adsr)) \(adsr.address)")
        for timedItem in timedItems {
//            print("KeyUp for \(String(describing: timedItem)) \(timedItem.address)")
            timedItem.keyUp(timeInterval: timeInterval)
        }
    }
    
    public func press(key: Int, for duration: TimeInterval) {
        frequency.note = Notes.note(at: Int(key))
        keyDown()
        DispatchQueue.global().asyncAfter(deadline: .now() + duration) { [weak self] in
            self?.keyUp()
        }
    }
    
    // MARK: Private functions
}

extension Voice {
    func render(timeInterval: TimeInterval, sampleRate: Double) -> Double {
//        guard adsr.shouldCalculate else { return 0 }
        guard volume.isEnabled.enumValue == .yes else { return 0 }
        
        // Calculate Frequency
        let f: Double = {
            // Base frequency
            var f = frequency.calculate(
                theta: theta,
                frequency: 0,
                timeInterval: timeInterval
            )
            // Apply modifiers
            frequencyChangers.forEach {
                f = $0.calculate(
                    theta: theta,
                    frequency: f,
                    timeInterval: timeInterval
                )
            }
            return f
        }()
        
        
        // Calculate our theta delta
        defer {
            theta += 2.0 * Double.pi * f / sampleRate
            theta = theta.truncatingRemainder(dividingBy: 2.0 * Double.pi)
        }
        
        // Calculate Amplitude
        let output: Double = {
            // Get our initial amplitude
            var output = waveformRenderer.calculate(
                theta: theta,
                frequency: f,
                timeInterval: timeInterval
            )

            // Alter our amplitude
            amplitudeChangers.forEach {
                output = $0.calculate(
                    input: output,
                    frequency: f,
                    timeInterval: timeInterval,
                    sampleRate: sampleRate
                )
            }
            return output
        }()
        return output
    }
}

extension Voice {
    public func randomize() {
        requiredEffects.forEach { $0.randomize() }
        optionalEffects.forEach { $0.randomize() }
    }
}
