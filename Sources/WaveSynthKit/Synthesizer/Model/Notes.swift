//
//  Notes.swift
//  WaveSynth
//
//  Created by Zakk Hoyt on 6/24/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import Foundation

private let f_0 = 27.5
private let a = pow(2, Double(1) / Double(12))

public class Notes {
    public static let middleC = 60
    public static let minimumIndex = 21 // A0 = 27.5 Hz
    public static let maximumIndex = 108 // C8 = 4186 Hz
    public static let stepsPerOctave = 12

    // A singleton is okay here. There is no need to re-calculate these over and over.
    // But calculating them once is better than a look up table.
    public static let shared = Notes()
    
    public init() {}

    //    Output: indexes 0 - 111 where indexes 0 - 87 are the notes of an 88 key piano
    //    The outputs keep going up to index 111 (C10) @ 16744.12 Hz, which is at the limit of typical human ears.//
    private static let notes: [Note] = {
        //    The basic formula for the frequencies of the notes of the equal tempered scale is given by
        //    fn = f0 * (a)n
        //    where
        //    f0 = the frequency of one fixed note which must be defined. A common choice is setting the A above middle C (A4) at f0 = 440 Hz.
        //    n = the number of half steps away from the fixed note you are. If you are at a higher note, n is positive. If you are on a lower note, n is negative.
        //    fn = the frequency of the note n half steps away.
        //    a = (2)1/12 = the twelth root of 2 = the number which when multiplied by itself 12 times equals 2 = 1.059463094359...
        
        var frequencies: [Double] = []
        var notes: [Note] = []
        
        for i in Notes.minimumIndex...Notes.maximumIndex {
            let frequency = Notes.frequency(for: Double(i))
            
            let n = i - Notes.minimumIndex
            
            frequencies.append(frequency)
            
            let i_l = n % 12
            let octave = (n + 9) / 12
            var names: [String] = []
            switch i_l {
            case 0:
                names = ["A\(octave)"]
            case 1:
                names = ["A♯\(octave)", "B♭\(octave)"]
            case 2:
                names = ["B\(octave)"]
            case 3:
                names = ["C\(octave)"]
            case 4:
                names = ["C♯\(octave)", "D♭\(octave)"]
            case 5:
                names = ["D\(octave)"]
            case 6:
                names = ["D♯\(octave)", "E♭\(octave)"]
            case 7:
                names = ["E\(octave)"]
            case 8:
                names = ["F\(octave)"]
            case 9:
                names = ["F♯\(octave)", "G♭\(octave)"]
            case 10:
                names = ["G\(octave)"]
            case 11:
                names = ["G♯\(octave)", "A♭\(octave)"]
            default:
                names = ["?"]
            }
            
            let note = Note(frequency: frequency, names: names, octave: octave, keyIndex: n)
            notes.append(note)
        }
        
        return notes
    }()
    
    /// Expects an input in the range of midi notes (21-108). Index will be clipped into this range
    public static func note(at index: Int) -> Note {
        // Midi notes have a value from 21-108. Our array is 0 based
        let safeIndex = min(max(Notes.minimumIndex, index), Notes.maximumIndex)
        let adjustedIndex = safeIndex - Notes.minimumIndex
        return notes[adjustedIndex]
    }

    public static func printNotes() {
        for note in notes {
            print(note.description)
        }
    }
    
    // Find the next note up
    public static func fretted(_ frequency: Double) -> Double {
        let frequencies = notes.map { $0.frequency }
        if let index = binarySearch(frequencies, key: frequency, range: 0..<frequencies.count) {
            return frequencies[index]
        }
        return frequency
    }
}

extension Notes {
    // These are taken from the list of midi notes
    // https://newt.phys.unsw.edu.au/jw/notes.html
    public static func frequency(for noteAtIndex: Double) -> Double {
        let zeroBasedIndex = noteAtIndex - Double(Notes.minimumIndex)
        return f_0 * pow(a, zeroBasedIndex)
    }
    
    public static func frequency(steps: Double, from frequency: Double) -> Double {
        frequency * pow(a, steps)
    }
    
    public static func frequencyDelta(steps: Double, from frequency: Double) -> Double {
        frequency * pow(a, steps) - frequency
    }
}

func binarySearch<T: Comparable>(_ a: [T], key: T, range: Range<Int>) -> Int? {
    if range.lowerBound >= range.upperBound {
        return range.upperBound
    } else {
        let midIndex = range.lowerBound + (range.upperBound - range.lowerBound) / 2
        if a[midIndex] > key {
            return binarySearch(a, key: key, range: range.lowerBound..<midIndex)
        } else if a[midIndex] < key {
            return binarySearch(a, key: key, range: midIndex + 1..<range.upperBound)
        } else {
            return midIndex
        }
    }
}
