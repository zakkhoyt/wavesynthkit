//
//  Volume.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

// public class Volume: Parameter, AmplitudeEffect {
public class Volume: AmplitudeEffect {
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    
    public private(set) var effectType: EffectType = .volume
    
    public var amplitude = Parameter(name: "Amplitude", value: 1, minimum: 0, maximum: 1)
    public var curve = EnumParameter<Curve>(name: "Volume", value: Curve.linear)
    
    public required init() {}
    
    public func calculate(
        input: Double,
        frequency: Double,
        timeInterval: TimeInterval,
        sampleRate: Double
    ) -> Double {
        guard isEnabled.enumValue == .yes else { return 0 }

//        let curved = curve.enumValue.interpolate(x: input)
//
//        let amped = input * amplitude.value
        
//        return curve.enumValue.interpolate(x: input)
        let out = input * curve.enumValue.interpolate(x: amplitude.value)
        return out
    }
}
