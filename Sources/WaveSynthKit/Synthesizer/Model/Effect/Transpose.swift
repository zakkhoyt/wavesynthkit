//
//  Transpose.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/15/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

// public class Transpose: Parameter, FrequencyEffect {
//    public required init() {
//
//    }
//
//    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
//
//    public let effectType: EffectType = .transpose
//
//    public init(value: Double,
//         minimum: Double,
//         maximum: Double) {
//        super.init(name: "Transpose",
//                   value: value,
//                   minimum: minimum,
//                   maximum: maximum)
//    }
//
//    required public init(from decoder: Decoder) throws {
//        try super.init(from: decoder)
//    }
//
//
//    public func calculate(theta: Double,frequency: Double, timeInterval: TimeInterval) -> Double {
//        guard isEnabled.enumValue == .yes else { return frequency }
//        return Notes.frequency(steps: value, from: frequency)
//    }
// }
