//
//  Frequency.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

// public class Frequency: Parameter {
////    public required init() {
////        super.init(name: "Frequency",
////                   value: value,
////                   minimum: minimum,
////                   maximum: maximum)
////    }
//
//    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
//
////    public let effectType: EffectType = .frequency
//
//    init(value: Double,
//         minimum: Double,
//         maximum: Double) {
//        super.init(name: "Frequency",
//                   value: value,
//                   minimum: minimum,
//                   maximum: maximum)
//    }
//
//    required public init(from decoder: Decoder) throws {
//        try super.init(from: decoder)
//    }
//
// }

public class Frequency: FrequencyEffect {
    public var effectType: EffectType = .frequency
    
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    public var hz: Parameter
    public var note: Note?
    public var transpose = Parameter(name: "Transpose", value: 0, minimum: -12, maximum: 12, snapTo: 1)
    public var octave = Parameter(name: "Octave", value: 0, minimum: -8, maximum: 8, snapTo: 1)
    
    public required init() {
        self.hz = Parameter(name: "Hz", value: 220, minimum: 20, maximum: 400)
    }

    public required init(frequency: Double, minimumFrequency: Double, maximumFrequency: Double) {
        self.hz = Parameter(name: "Hz", value: frequency, minimum: minimumFrequency, maximum: maximumFrequency)
    }

    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        var frequency = note?.frequency ?? hz.value
        frequency = Notes.frequency(steps: octave.value * 12, from: frequency)
        frequency = Notes.frequency(steps: transpose.value, from: frequency)
        return frequency
    }
    
    private enum CodingKeys: String, CodingKey {
        case isEnabled
        case hz
        case note
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.isEnabled = try container.decode(EnumParameter<Enabled>.self, forKey: .isEnabled)
        self.hz = try container.decode(Parameter.self, forKey: .hz)
        if let note = try container.decodeIfPresent(Note.self, forKey: .note) {
            self.note = note
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(isEnabled, forKey: .isEnabled)
        try container.encode(hz, forKey: .hz)
        if let note {
            try container.encode(note, forKey: .note)
        }
    }
}
