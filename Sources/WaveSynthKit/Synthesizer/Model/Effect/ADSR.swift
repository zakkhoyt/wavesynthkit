//
//  ADSR.swift
//  WaveSynthesizer
//
//  Created by Zakk Hoyt on 6/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class ADSR: TimedItem, AmplitudeEffect {
    // TODO: Possibly rename this property or make it internal + expose a public wrapper more aptly named.
    // If false, then bypass needless math
    public private(set) var shouldCalculate = false
    
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    
    public let effectType: EffectType = .adsr
    
    public var attack = Parameter(name: "Attack Level", value: 0.9, minimum: 0, maximum: 1)
    public var attackDuration = Parameter(name: "Attack Duration", value: 0.05, minimum: 0, maximum: TimeInterval.maximumDuration)
    public var attackCurve = EnumParameter<Curve>(name: "Attack Curve", value: Curve.linear)
    public var decayDuration = Parameter(name: "Decay Duration", value: 0.25, minimum: 0, maximum: TimeInterval.maximumDuration)
    public var decayCurve = EnumParameter<Curve>(name: "Decay Curve", value: Curve.linear)
    public var sustain = Parameter(name: "Sustain Level", value: 0.5, minimum: 0, maximum: 1)
    public var releaseDuration = Parameter(name: "Release Duration", value: 0.25, minimum: 0, maximum: TimeInterval.maximumDuration)
    public var releaseCurve = EnumParameter<Curve>(name: "Release Curve", value: Curve.linear)
    
    private var attackTimeInterval: TimeInterval = 0
    private var decayTimeInterval: TimeInterval = 0
    private var releaseTimeInterval: TimeInterval = 0
    
    private var previousOutput: Double = 0
    private var attackFromValue: Double = 0
    
    public enum EnvelopeState: CustomStringConvertible {
        case waiting
        case attack
        case decay
        case sustain
        case release
        case finished
        
        public var description: String {
            switch self {
            case .waiting: return "Waiting"
            case .attack: return "Attack"
            case .decay: return "Decay"
            case .sustain: return "Sustain"
            case .release: return "Release"
            case .finished: return "Finished"
            }
        }
    }
    
    public private(set) var envelopeState: EnvelopeState = .waiting {
        didSet(oldValue) {
            guard oldValue != envelopeState else { return }
            // print("ADSR state changed from \(oldValue) to \(envelopeState)")
            
            if envelopeState == .attack, oldValue == .release {
                // print("Captured attack to release: \(abs(previousOutput))")
                attackFromValue = abs(previousOutput)
            } else {
                attackFromValue = 0
            }
            
            if envelopeState == .finished, oldValue == .release {
                shouldCalculate = false
            }
        }
    }
    
    override public required init() {
        super.init()
    }
    
    private enum CodingKeys: String, CodingKey {
        case attack
        case attackDuration
        case attackCurve
        case decayDuration
        case decayCurve
        case sustain
        case releaseDuration
        case releaseCurve
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        self.attack = try container.decode(Parameter.self, forKey: .attack)
        self.attackDuration = try container.decode(Parameter.self, forKey: .attackDuration)
        self.attackCurve = try container.decode(EnumParameter.self, forKey: .attackCurve)
        self.decayDuration = try container.decode(Parameter.self, forKey: .decayDuration)
        self.decayCurve = try container.decode(EnumParameter.self, forKey: .decayCurve)
        self.sustain = try container.decode(Parameter.self, forKey: .sustain)
        self.releaseDuration = try container.decode(Parameter.self, forKey: .releaseDuration)
        self.releaseCurve = try container.decode(EnumParameter.self, forKey: .releaseCurve)
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(attack, forKey: .attack)
        try container.encode(attackDuration, forKey: .attackDuration)
        try container.encode(attackCurve, forKey: .attackCurve)
        try container.encode(decayDuration, forKey: .decayDuration)
        try container.encode(decayCurve, forKey: .decayCurve)
        try container.encode(sustain, forKey: .sustain)
        try container.encode(releaseDuration, forKey: .releaseDuration)
        try container.encode(releaseCurve, forKey: .releaseCurve)
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }

    override func keyDown(timeInterval: TimeInterval) {
        super.keyDown(timeInterval: timeInterval)
        attackTimeInterval = timeInterval + attackDuration.value
        decayTimeInterval = timeInterval + attackDuration.value + decayDuration.value
        shouldCalculate = true
    }
    
    override func keyUp(timeInterval: TimeInterval) {
        super.keyUp(timeInterval: timeInterval)
        releaseTimeInterval = timeInterval + releaseDuration.value
    }
}

extension ADSR {
    public func calculate(
        input: Double,
        frequency: Double,
        timeInterval: TimeInterval,
        sampleRate: Double
    ) -> Double {
        guard isEnabled.enumValue == .yes else {
            return input
        }
        
        func multiplier() -> Double {
            // TODO: Calculate enum first then, switch to do math
            guard let keyDownTimeInterval,
                  timeInterval > keyDownTimeInterval else {
                // Key not pressed yet
                envelopeState = .waiting
                return 0
            }
            
            if let keyUpTimeInterval, keyUpTimeInterval > keyDownTimeInterval {
                guard timeInterval < releaseTimeInterval else {
                    // we are past release
                    envelopeState = .finished
                    return 0
                }
                
                // interpolate release
                envelopeState = .release
                return releaseCurve.enumValue.interpolate(
                    inputValue: timeInterval,
                    minimumInputValue: keyUpTimeInterval,
                    maximumInputValue: releaseTimeInterval,
                    minimumOutputValue: sustain.value,
                    maximumOutputValue: 0
                )
            } else if timeInterval < attackTimeInterval {
                // interpolate attack
                envelopeState = .attack
                return attackCurve.enumValue.interpolate(
                    inputValue: timeInterval,
                    minimumInputValue: keyDownTimeInterval,
                    maximumInputValue: attackTimeInterval,
                    minimumOutputValue: attackFromValue,
                    maximumOutputValue: attack.value
                )
            } else if timeInterval >= attackTimeInterval, timeInterval < decayTimeInterval {
                // interpoalte decay
                envelopeState = .decay
                return decayCurve.enumValue.interpolate(
                    inputValue: timeInterval,
                    minimumInputValue: attackTimeInterval,
                    maximumInputValue: decayTimeInterval,
                    minimumOutputValue: attack.value,
                    maximumOutputValue: sustain.value
                )
            } else {
                // sustain
                envelopeState = .sustain
                //                return input * sustain
                return sustain.value
            }
        }
        
        let m = multiplier()
        let current = m * input
        previousOutput = current
        return current
    }
}

extension ADSR {
    public static func stringed() -> ADSR {
        let adsr = ADSR()
        adsr.attackDuration.value = 0.01
        adsr.decayDuration.value = 0.1
        adsr.releaseDuration.value = 0.25
        return adsr
    }
    
    public static func bowed() -> ADSR {
        let adsr = ADSR()
        adsr.attackDuration.value = 0.25
        adsr.decayDuration.value = 0.5
        adsr.releaseDuration.value = 0.25
        return adsr
    }
}
