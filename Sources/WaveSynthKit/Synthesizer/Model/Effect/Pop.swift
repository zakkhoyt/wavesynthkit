//
//  Pop.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/14/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class Pop: TimedItem, FrequencyEffect, Disableable {
    public let effectType: EffectType = .pop
    
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    
    public var delay = Parameter(name: "Delay", value: 0, minimum: 0, maximum: TimeInterval.maximumDuration)
    public var duration = Parameter(name: "Duration", value: 0.1, minimum: 0, maximum: TimeInterval.maximumDuration)
    public var startSteps = Parameter(name: "Start Steps", value: 0, minimum: -36, maximum: 36, snapTo: 1.0)
    public var endSteps = Parameter(name: "End Steps", value: 12, minimum: -36, maximum: 36, snapTo: 1.0)
    
    public var curve = EnumParameter<Curve>(name: "Curve", value: Curve.linear)
    
    private var startTimeInterval: TimeInterval = 0
    private var endTimeInterval: TimeInterval = 0
    
    override public required init() {
        super.init()
    }
    
    private enum CodingKeys: String, CodingKey {
        case enabled
        case delay
        case duration
        case startSteps
        case endSteps
        case curve
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        self.delay = try container.decode(Parameter.self, forKey: .delay)
        self.duration = try container.decode(Parameter.self, forKey: .duration)
        self.startSteps = try container.decode(Parameter.self, forKey: .startSteps)
        self.endSteps = try container.decode(Parameter.self, forKey: .endSteps)
        self.curve = try container.decode(EnumParameter<Curve>.self, forKey: .curve)
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(delay, forKey: .delay)
        try container.encode(duration, forKey: .duration)
        try container.encode(startSteps, forKey: .startSteps)
        try container.encode(endSteps, forKey: .endSteps)
        try container.encode(curve, forKey: .curve)
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }

    override func keyDown(timeInterval: TimeInterval) {
        super.keyDown(timeInterval: timeInterval)
        startTimeInterval = Double.greatestFiniteMagnitude
    }
    
    override func keyUp(timeInterval: TimeInterval) {
        super.keyUp(timeInterval: timeInterval)
        startTimeInterval = delay.value + timeInterval
    }
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        guard isEnabled.enumValue == .yes else { return frequency }
        
        if timeInterval >= startTimeInterval {
            return frequency + Notes.frequencyDelta(steps: endSteps.value, from: frequency)
        } else {
            return frequency + Notes.frequencyDelta(steps: startSteps.value, from: frequency)
        }
    }
}
