//
//  Roll.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/14/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class Roll: TimedItem, FrequencyEffect, Disableable {
    public let effectType: EffectType = .roll

    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    
    public var delay = Parameter(name: "Delay", value: 0.25, minimum: 0, maximum: TimeInterval.maximumDuration)
    public var cycleDuration = Parameter(name: "Cycle Duration", value: 0.05, minimum: 0, maximum: TimeInterval.maximumDuration)
    public var cycleDelay = Parameter(name: "Cycle Delay", value: 0.1, minimum: 0, maximum: TimeInterval.maximumDuration)
    
    public var maximumCycles = Parameter(name: "Maximum Cycles", value: 12, minimum: 0, maximum: 24, snapTo: 1.0)
    
    public var steps = Parameter(name: "Steps", value: 1.0, minimum: -36, maximum: 36)
    
    public var curve = EnumParameter<Curve>(name: "Curve", value: Curve.linear)
    
    private var startTimeInterval: TimeInterval = Double.greatestFiniteMagnitude
    private var endTimeInterval: TimeInterval = Double.greatestFiniteMagnitude
    
    override public required init() {
        super.init()
    }
    
    private enum CodingKeys: String, CodingKey {
        case enabled
        case delay
        case cycleDuration
        case cycleDelay
        case steps
        case curve
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        self.isEnabled = try container.decode(EnumParameter<Enabled>.self, forKey: .enabled)
        self.delay = try container.decode(Parameter.self, forKey: .delay)
        self.cycleDuration = try container.decode(Parameter.self, forKey: .cycleDuration)
        self.cycleDelay = try container.decode(Parameter.self, forKey: .cycleDelay)
        self.steps = try container.decode(Parameter.self, forKey: .steps)
        self.curve = try container.decode(EnumParameter<Curve>.self, forKey: .curve)
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(isEnabled, forKey: .enabled)
        try container.encode(delay, forKey: .delay)
        try container.encode(cycleDuration, forKey: .cycleDuration)
        try container.encode(steps, forKey: .steps)
        try container.encode(curve, forKey: .curve)
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }
    
    override func keyDown(timeInterval: TimeInterval) {
        super.keyDown(timeInterval: timeInterval)
        // startTimeInterval = Double.greatestFiniteMagnitude
        startTimeInterval = timeInterval
    }
    
    override func keyUp(timeInterval: TimeInterval) {
        super.keyUp(timeInterval: timeInterval)
        startTimeInterval = Double.greatestFiniteMagnitude
    }
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        guard isEnabled.enumValue == .yes else { return frequency }
        
        if timeInterval >= startTimeInterval {
            let elapsed = (timeInterval - startTimeInterval) - delay.value
            guard elapsed > 0 else { return frequency }
            
            let cycleTime = cycleDelay.value + cycleDuration.value
            let cycles = min(floor(elapsed / cycleTime), Double(maximumCycles.value + 1))
            
            let startTime = startTimeInterval + delay.value + cycles * cycleTime + cycleDelay.value
            let endTime = startTime + cycleDuration.value
            
            let startSteps = cycles * steps.value
            let startFrequency = frequency + Notes.frequencyDelta(steps: startSteps, from: frequency)
            
            let endSteps = startSteps >= 0 ? startSteps + 1 : startSteps - 1
            let endFrequency = frequency + Notes.frequencyDelta(steps: endSteps, from: frequency)
            
            return curve.enumValue.interpolate(
                inputValue: timeInterval,
                minimumInputValue: startTime,
                maximumInputValue: endTime,
                minimumOutputValue: startFrequency,
                maximumOutputValue: endFrequency
            )
        } else {
            return 0
        }
    }
}
