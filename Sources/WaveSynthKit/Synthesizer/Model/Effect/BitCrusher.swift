//
//  BitCrusher.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/17/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class BitCrusher: TimedItem, AmplitudeEffect {
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    
    public var effectType: EffectType = .bitCrusher
    
    public private(set) var levels = Parameter(name: "Amplitude Levels", value: 8, minimum: 2, maximum: 16)
    
    override public required init() {
        super.init()
    }
    
    private enum CodingKeys: String, CodingKey {
        case levels
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        self.levels = try container.decode(Parameter.self, forKey: .levels)
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(levels, forKey: .levels)
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }
    
    public func calculate(
        input: Double,
        frequency: Double,
        timeInterval: TimeInterval,
        sampleRate: Double
    ) -> Double {
        guard isEnabled.enumValue == .yes else { return input }
        
        if levels.value == 0 { return input }
        let nearest = Double(1) / Double(levels.value)
        return input.round(nearest: nearest)
    }
}
