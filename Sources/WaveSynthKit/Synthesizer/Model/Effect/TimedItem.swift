//
//  KeyedItem.swift
//  WaveSynthesizer
//
//  Created by Zakk Hoyt on 6/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class TimedItem: Codable {
    // MARK: Voice.State
    
    public enum State: String, Codable {
        case unknown = "Unknown"
        case keyDown = "Key Down"
        case keyUp = "Key Up"
    }
    
    var state: State = .unknown
    var keyDownTimeInterval: TimeInterval?
    var keyUpTimeInterval: TimeInterval?

    func keyDown(timeInterval: TimeInterval) {
        state = .keyDown
        keyDownTimeInterval = timeInterval
    }

    func keyUp(timeInterval: TimeInterval) {
        state = .keyUp
        keyUpTimeInterval = timeInterval
    }
    
    var address: String {
        var me = self
        var output = ""
        withUnsafePointer(to: &me) {
            output = "<\($0)>"
        }
        return output
    }
    
    private enum CodingKeys: String, CodingKey {
        case state
        case keyDownTimeInterval = "keyDown"
        case keyUpTimeInterval = "keyUp"
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.state = try container.decode(State.self, forKey: .state)
        if let keyDownTimeInterval = try container.decodeIfPresent(TimeInterval.self, forKey: .keyDownTimeInterval) {
            self.keyDownTimeInterval = keyDownTimeInterval
        }
        
        if let keyUpTimeInterval = try container.decodeIfPresent(TimeInterval.self, forKey: .keyUpTimeInterval) {
            self.keyUpTimeInterval = keyUpTimeInterval
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(state, forKey: .state)
        if let keyDownTimeInterval {
            try container.encode(keyDownTimeInterval, forKey: .keyDownTimeInterval)
        }
        if let keyUpTimeInterval {
            try container.encode(keyUpTimeInterval, forKey: .keyUpTimeInterval)
        }
    }
    
    public init() {}
}
