//
//  AdcCrusher.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/17/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class AdcCrusher: TimedItem, AmplitudeEffect {
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    
    public var effectType: EffectType = .adcCrusher
    
    public private(set) var count = Parameter(name: "Count", value: 5, minimum: 1, maximum: 128, snapTo: 1.0)
    
    private var counter = 0
    
    override public required init() {
        super.init()
    }

    private enum CodingKeys: String, CodingKey {
        case count
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        self.count = try container.decode(Parameter.self, forKey: .count)
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(count, forKey: .count)
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }
    
    override func keyDown(timeInterval: TimeInterval) {
        super.keyDown(timeInterval: timeInterval)
        counter = 0
    }
    
    public func calculate(
        input: Double,
        frequency: Double,
        timeInterval: TimeInterval,
        sampleRate: Double
    ) -> Double {
        guard isEnabled.enumValue == .yes else { return input }
        /// Reduces vertical resolution
        let maxAmplitude = max(1, count.value)
        let amplitude = floor(input * maxAmplitude)
        return amplitude / maxAmplitude
    }
}
