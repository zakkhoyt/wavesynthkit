//
//  Vibrato.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class Vibrato: TimedItem, FrequencyEffect, Disableable {
    public let effectType: EffectType = .vibrato

    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)
    
    // public var delay: TimeInterval = 0.25
    public var delay = Parameter(name: "Delay", value: 0.25, minimum: 0, maximum: TimeInterval.maximumDuration, snapTo: 0.1)
    
    /// Frequency of vibrato (cycles per second)
//    public var frequency: Double = 5
    public var frequency = Parameter(name: "Frequency", value: 5, minimum: 0, maximum: 20, snapTo: 1)
    
    /// Number of steps up and down
//    public var steps: Double = 0.5
    public var steps = Parameter(name: "Steps", value: 0.5, minimum: 0, maximum: 12, snapTo: 0.5)

    private var duration: TimeInterval {
        1 / frequency.value
    }
    
    private var startTimeInterval: TimeInterval = 0
    private var endTimeInterval: TimeInterval = 0

    override func keyDown(timeInterval: TimeInterval) {
        super.keyDown(timeInterval: timeInterval)
        startTimeInterval = delay.value + timeInterval
        endTimeInterval = startTimeInterval + duration
    }
    
    override public required init() {
        super.init()
    }
    
    private enum CodingKeys: String, CodingKey {
        case isEnabled
        case delay
        case frequency
        case steps
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)

        self.isEnabled = try container.decode(EnumParameter<Enabled>.self, forKey: .isEnabled)
        self.delay = try container.decode(Parameter.self, forKey: .delay)
        self.frequency = try container.decode(Parameter.self, forKey: .frequency)
        self.steps = try container.decode(Parameter.self, forKey: .steps)
    }

    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(isEnabled, forKey: .isEnabled)
        try container.encode(delay, forKey: .delay)
        try container.encode(frequency, forKey: .frequency)
        try container.encode(steps, forKey: .steps)
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }

    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        guard isEnabled.enumValue == .yes else { return frequency }
        
        if timeInterval < startTimeInterval {
            return frequency
        } else {
            let elapsed = timeInterval - startTimeInterval
            let magnitude = (sin(self.frequency.value * elapsed * 2.0 * Double.pi) * 2 - 1)
            let sign: Double = magnitude < 0 ? 1.0 : -1.0
            let maxDelta: Double = sign * Notes.frequencyDelta(steps: sign * steps.value, from: frequency)
            return frequency + magnitude * maxDelta
        }
    }
}
