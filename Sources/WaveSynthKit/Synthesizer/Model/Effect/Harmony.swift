//
//  Harmony.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/15/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

// TODO: How to make this work as a FrequencyEffect?
// Does `Voice` need to have an array of frequencies?
public class Harmony: FrequencyEffect {
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)

    public private(set) var effectType: EffectType = .harmony

    public required init() {}
    
    public private(set) var steps = Parameter(name: "Steps", value: 5, minimum: -36, maximum: 36, snapTo: 1)
    
    var theta: Double = 0
    
//    init(value: Double,
//         minimum: Double,
//         maximum: Double) {
//        super.init(name: "Frequency",
//                   value: value,
//                   minimum: minimum,
//                   maximum: maximum)
//    }

//    required public init(from decoder: Decoder) throws {
//        try super.init(from: decoder)
//    }
    
    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        return frequency
    }
}
