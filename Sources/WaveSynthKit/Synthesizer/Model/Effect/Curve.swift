//
//  Curve.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public enum MathMode: Int, Codable, CaseIterable, CustomStringConvertible {
    case function
    case fourier
    
    public static func count() -> Int {
        2
    }
    
    public var description: String {
        switch self {
        case .function:
            return "Function"
        case .fourier:
            return "Fourier"
        }
    }
}

public enum Enabled: Int, Codable, CaseIterable, CustomStringConvertible {
    case no
    case yes
    public var description: String {
        switch self {
        case .no: return "No"
        case .yes: return "Yes"
        }
    }
}

public enum Curve: Int, Codable, CaseIterable, CustomStringConvertible {
    case linear
    case quadratic
    
    public var description: String {
        switch self {
        case .linear: return "Linear"
        case .quadratic: return "Quadratic"
        }
    }
    
    /// Domain of 0 ... 1
    /// Range of 0 ... 1
    public func interpolate(x: Double) -> Double {
        switch self {
        case .linear:
            return x
        case .quadratic:
            return 1.0 - pow(x - 1.0, 2)
        }
    }
    
    public func interpolate(
        inputValue: Double,
        minimumInputValue: Double,
        maximumInputValue: Double,
        minimumOutputValue: Double,
        maximumOutputValue: Double
    ) -> Double {
        guard inputValue >= minimumInputValue, inputValue < maximumInputValue else { return minimumOutputValue }
        guard minimumInputValue < maximumInputValue else { return minimumOutputValue }
        let progress = (inputValue - minimumInputValue) / (maximumInputValue - minimumInputValue)
        let eased = interpolate(x: progress)
        let levelDiff = maximumOutputValue - minimumOutputValue
        return minimumOutputValue + levelDiff * eased
    }
}
