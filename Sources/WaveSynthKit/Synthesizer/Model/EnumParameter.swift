//
//  EnumParameter.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/23/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class EnumParameter<T>: Parameter where T: RawRepresentable, T: CaseIterable, T.RawValue == Int {
    public init<T>(name: String, value: T)
        where T: RawRepresentable, T: CaseIterable, T.RawValue == Int {
        let type = T.self
        // let name = String(describing: type)
        let value = type.init(rawValue: value.rawValue)
            
        super.init(
            name: name,
            value: Double(value?.rawValue ?? 0),
            minimum: Double(0),
            maximum: Double(type.allCases.count - 1),
            snapTo: 1.0
        )
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        
        // self.count = try container.decode(Parameter.self, forKey: .count)
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        //        try container.encode(count, forKey: .count)
        
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }
    
    public var enumValue: T {
        get {
            T(rawValue: self.intValue)!
        }
        set {
            value = Double(newValue.rawValue)
        }
    }

    override public func randomize() {
        let value = Int(minimum + Double.random(in: 0.0...1.0) * (maximum - minimum))
        enumValue = T(rawValue: value)!
    }
}
