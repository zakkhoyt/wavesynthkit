//
//  Parameter.swift
//  WaveSynthesizer
//
//  Created by Zakk Hoyt on 6/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class Parameter: Codable, Randomizable {
    public private(set) var name: String
    public private(set) var minimum: Double
    public private(set) var maximum: Double
    public private(set) var snapTo: Double?

    private var _value: Double
    public var value: Double {
        get { _value }
        set {
            let clipped = min(max(minimum, newValue), maximum)
            if let snapTo {
                _value = clipped.round(nearest: snapTo)
            } else {
                _value = clipped
            }
        }
    }
    
    public var intValue: Int {
        Int(value)
    }

    public init(
        name: String,
        value: Double,
        minimum: Double,
        maximum: Double,
        snapTo: Double? = nil
    ) {
        self.name = name
        self._value = value
        self.minimum = minimum
        self.maximum = maximum
        self.snapTo = snapTo
    }

    enum CodingKeys: String, CodingKey {
        case name
        case value
        case minimum
        case maximum
        case snapTo
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self._value = try container.decode(Double.self, forKey: .value)
        self.minimum = try container.decode(Double.self, forKey: .minimum)
        self.maximum = try container.decode(Double.self, forKey: .maximum)
        if let snapTo = try container.decodeIfPresent(Double.self, forKey: .snapTo) {
            self.snapTo = snapTo
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(value, forKey: .value)
        try container.encode(minimum, forKey: .minimum)
        try container.encode(maximum, forKey: .maximum)
        try container.encode(snapTo, forKey: .snapTo)
    }

    // MARK: Randomizable
    
    public func randomize() {
        value = minimum + Double.random(in: 0.0...1.0) * (maximum - minimum)
    }
}

extension TimeInterval {
    static let maximumDuration: TimeInterval = 1
}
