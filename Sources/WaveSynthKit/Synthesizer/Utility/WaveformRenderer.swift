//
//  WaveformRenderer.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/14/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class WaveformRenderer: TimedItem, Effect {
    public var effectType: EffectType = .waveformRenderer
    public var isEnabled = EnumParameter<Enabled>(name: "Enabled", value: Enabled.yes)

    public var sine = Sine()
    public var square = Square()
    public var squareFourier = SquareFourier()
    public var triangle = Triangle()
    public var triangleFourier = TriangleFourier()
    public var sawtooth = Sawtooth()
    public var sawtoothFourier = SawtoothFourier()
    public var noise = Noise()
    public var whiteNoise = WhiteNoise()
    
    public var waveform = EnumParameter<WaveformType>(name: "Waveform", value: WaveformType.sin)
    
    public var isFadeEnabled = EnumParameter<Enabled>(name: "Fade Enabled", value: Enabled.no)
    public var fadeToWaveform = EnumParameter<WaveformType>(name: "Fade Waveform", value: WaveformType.sawtooth)
    public var delay = Parameter(name: "Fade Delay", value: 1, minimum: 0, maximum: TimeInterval.maximumDuration, snapTo: 0.1)
    public var duration = Parameter(name: "Fade Duration", value: 1, minimum: 0, maximum: TimeInterval.maximumDuration)
    
    public init(waveformType: WaveformType) {
        waveform.enumValue = waveformType
        super.init()
    }
    
    override public required init() {
        waveform.enumValue = .sin
        super.init()
    }
    
    public required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }

    private var startTimeInterval: TimeInterval = 0

    override func keyDown(timeInterval: TimeInterval) {
        super.keyDown(timeInterval: timeInterval)
        startTimeInterval = timeInterval + delay.value
    }
    
    override func keyUp(timeInterval: TimeInterval) {
        super.keyUp(timeInterval: timeInterval)
        // startTimeInterval = TimeInterval.greatestFiniteMagnitude
    }

    private func waveformCalculator(for waveform: EnumParameter<WaveformType>) -> WaveCalculatable {
        switch waveform.enumValue {
        case .sin: return sine
        case .square: return square
        case .squareFourier: return squareFourier
        case .triangle: return triangle
        case .triangleFourier: return triangleFourier
        case .sawtooth: return sawtooth
        case .sawtoothFourier: return sawtoothFourier
        case .noise: return noise
        case .whiteNoise: return whiteNoise
        }
    }

    public func calculate(
        theta: Double,
        frequency: Double,
        timeInterval: TimeInterval
    ) -> Double {
        let from = waveformCalculator(for: waveform).calculate(
            theta: theta,
            frequency: frequency,
            timeInterval: timeInterval
        )

        if isFadeEnabled.enumValue == .no || timeInterval < startTimeInterval {
            return from
        } else {
            let to = waveformCalculator(for: fadeToWaveform).calculate(
                theta: theta,
                frequency: frequency,
                timeInterval: timeInterval
            )
            
            if timeInterval < startTimeInterval + duration.value {
                let elapsed = timeInterval - startTimeInterval
                return Curve.linear.interpolate(inputValue: elapsed, minimumInputValue: 0, maximumInputValue: duration.value, minimumOutputValue: from, maximumOutputValue: to)
            } else {
                return to
            }
        }
    }
}
