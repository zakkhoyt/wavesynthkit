//
//  MotionMonitofr.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/14/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)

import CoreMotion
import Foundation

// open var attitude: CMAttitude { get }
// open var rotationRate: CMRotationRate { get }
// open var gravity: CMAcceleration { get }
// open var userAcceleration: CMAcceleration { get }
// open var magneticField: CMCalibratedMagneticField { get }
// open var heading: Double { get }

public class MotionMonitor {
    private var manager: CMMotionManager
    private var queue = OperationQueue()
    public init() {
        self.manager = CMMotionManager()
    }
    
    public func start(motionChanged: ((CMDeviceMotion) -> Void)?) {
        manager.startDeviceMotionUpdates(to: queue) { (motion: CMDeviceMotion?, error: Error?) in
            if let error {
                print("Motion error: \(error.localizedDescription)")
                return
            }
            
            guard let motion else {
                return
            }
            
            motionChanged?(motion)
        }
    }
    
    public func stop() {
        manager.stopDeviceMotionUpdates()
    }
}
#endif
