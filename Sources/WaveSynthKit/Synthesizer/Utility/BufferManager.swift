//
//  BufferManager.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/9/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

protocol BufferManagerDelegate: AnyObject {
    func bufferManager(_ bufferManager: BufferManager, didSwapVisualBuffer buffer: [Float])
}

/// Manages two buffers. Captures audio samples. When one buffer is full, we present it as active and continue colelcting samples to the other buffer
class BufferManager {
    weak var delegate: BufferManagerDelegate?
    
    private var bufferSize: Int
    private var buffers: [[Float]] = []
    private var bufferExternalIndex = 0
    private var bufferInternalIndex = 0
    private let queue = DispatchQueue(label: "BufferManagerQueue")
    
    init(bufferSize: Int) {
        self.bufferSize = bufferSize

        // Allocate two buffers
        for _ in 0..<2 {
            let buffer = [Float](repeating: 0, count: Int(bufferSize))
            buffers.append(buffer)
        }
    }
    
    func addStuff(buffer audio: UnsafeMutablePointer<Float32>, count: UInt32) {
        func process() {
            for i in 0..<Int(count) {
                if bufferInternalIndex == bufferSize {
                    // Copy the full buffer for our delegate, then swap buffers and keep going
                    if let delegate {
                        let buffer = buffers[bufferExternalIndex]
                        delegate.bufferManager(self, didSwapVisualBuffer: buffer)
                    }
                    
                    bufferExternalIndex = (bufferExternalIndex + 1) % 2
                    bufferInternalIndex = 0
                }

                buffers[bufferExternalIndex][bufferInternalIndex] = audio[i]
                bufferInternalIndex += 1
            }
        }
        
        queue.async(flags: .barrier) {
            process()
        }
    }
}
