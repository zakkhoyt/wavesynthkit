//
//  AudioStreamBasicDescription+Factory.swift
//  RTPAudioEngine
//
//  Created by Zakk Hoyt on 3/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import CoreAudio
import Foundation

extension AudioStreamBasicDescription {
    static func linearFloat32(sampleRate: Double) -> AudioStreamBasicDescription {
        // Set format to 32 bit, floating point, linear PCM
        var ioFormat = AudioStreamBasicDescription()
        ioFormat.mSampleRate = sampleRate
        ioFormat.mFormatID = kAudioFormatLinearPCM
        ioFormat.mFormatFlags = kAudioFormatFlagsNativeFloatPacked //  | kAudioFormatFlagIsNonInterleaved
        ioFormat.mBytesPerPacket = 4
        ioFormat.mFramesPerPacket = 1
        ioFormat.mBytesPerFrame = 4
        ioFormat.mChannelsPerFrame = 1
        ioFormat.mBitsPerChannel = 32
        return ioFormat
    }
}
