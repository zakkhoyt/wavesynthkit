//
//  AudioManager.swift
//  WaveSynthesizer
//
//  Created by Zakk Hoyt on 3/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Accelerate
import AudioToolbox
import AVFoundation
import CoreAudio
import Foundation

#if os(iOS) || os(tvOS)
public enum AudioOutputPath: Int, CaseIterable, CustomStringConvertible {
    case receiver
    case speaker
    
    public var description: String {
        switch self {
        case .receiver: return "Receiver"
        case .speaker: return "Speaker"
        }
    }
    
    internal var outputPort: AVAudioSession.PortOverride {
        switch self {
        case .receiver: return .none
        case .speaker: return .speaker
        }
    }
}
#endif

protocol AudioManagerGeneratorDataSource: AnyObject {
    func audioManager(
        _ audioManager: AudioManager,
        fillBuffer buffer: UnsafeMutablePointer<Float32>,
        timeIntervals: [TimeInterval],
        sampleRate: Double
    )
}

protocol AudioManagerGeneratorBufferDelegate: AnyObject {
    func audioManager(
        _ audioManager: AudioManager,
        bufferWasFilled buffer: UnsafeMutablePointer<Float32>,
        count: UInt32,
        timeInterval: TimeInterval
    )
}

class AudioManager {
    weak var generatorBufferDataSource: AudioManagerGeneratorDataSource?
    weak var generatorBufferDelegate: AudioManagerGeneratorBufferDelegate?

    // MARK: Internal vars

#if os(iOS) || os(tvOS)
    var outputPath: AudioOutputPath = .speaker
    private let audioSession: AVAudioSession = .sharedInstance()
#endif
    
    var isRunning = false
    
    private(set) var audioUnit: AudioUnit?
    private(set) var sampleRate: Double
    
    private let inputBus: UInt32 = 0
    
    internal var numberOfChannels = 1
    
    private var dateOfFirstSample: Date?
    private(set) var calculatedTimeInterval: TimeInterval = 0
    
    private var shouldPerformDCOffsetRejection = false
    private var audioChainIsBeingReconstructed = false
    private var mute = false
    
    init(sampleRate: Double) {
        self.sampleRate = sampleRate
        
//        do {
//            try setupAudioChain()
//            print("Finished init. AudioUnit and AudioSession are ready")
//        } catch {
//            print(error)
//            return nil
//        }
        
        // TODO: Deal with this force unwrap
        try! setupAudioChain()
#if os(iOS) || os(tvOS)
        print("Finished init. AudioUnit and AudioSession are ready")
#else
        print("Finished init. AudioUnit is ready")
#endif
    }
    
    deinit {
#if os(iOS) || os(tvOS)
        tearDownAudioChain()
#endif
    }
    
    func start() throws {
        print(#function)
        
        guard let audioUnit else {
            let error = ErrorFactory.errorWith(localDescription: "audioUnit is nil")
            print(error)
            return
        }
        
        do {
            let code = AudioOutputUnitStart(audioUnit)
            if code != 0 {
                let error = ErrorFactory.errorWith(localDescription: "Failed to create new IOUnit with AudioComponentInstanceNew: \(code)")
                print(error)
                throw error
            }
        }

        isRunning = true
    }
    
    func stop() throws {
        print(#function)
        
        guard let audioUnit else {
            let error = ErrorFactory.errorWith(localDescription: "audioUnit is nil")
            print(error)
            throw error
        }
        
        isRunning = false
        
        do {
            let code = AudioOutputUnitStop(audioUnit)
            if code != 0 {
                let error = ErrorFactory.errorWith(localDescription: "Failed to AudioOutputUnitStop: \(code)")
                print(error)
                throw error
            }
        }
    }
    
#if os(iOS) || os(tvOS)
    func preferReciever() {
        do {
            try audioSession.overrideOutputAudioPort(.none)
            outputPath = .receiver
        } catch let error as NSError {
            print("Failed to override output to speakers: " + error.localizedDescription)
            return
        }
    }
    
    func preferSpeaker() {
        do {
            try audioSession.overrideOutputAudioPort(.speaker)
            outputPath = .speaker
        } catch let error as NSError {
            print("Failed to override output to speakers: " + error.localizedDescription)
            return
        }
    }
    
    private func prefer(port: AVAudioSession.PortOverride) {
        print(#function)
    }
#endif
    
    private func setupAudioChain() throws {
#if os(iOS) || os(tvOS)
        setupAudioSession()
#endif
        
        try setupAudioUnit()
        try setupGenerator()
    }
    
#if os(iOS) || os(tvOS)
    private func tearDownAudioChain() {
        guard let audioUnit else {
            return
        }
        
        do {
            try stop()
            var osErr: OSStatus = 0
            osErr = AudioUnitUninitialize(audioUnit)
            assert(osErr == noErr, "*** AudioUnitUninitialize err \(osErr)")
            try audioSession.setActive(false)
        } catch {
            print("*** error: \(error)")
        }
    }
    
    private func setupAudioSession() {
        print(#function)
        
        do {
            try audioSession.setCategory(
                .multiRoute,
                mode: .default,
                options: [.defaultToSpeaker, .allowAirPlay, .mixWithOthers]
            )
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }
        
        // "Appropriate for applications that wish to minimize the effect of system-supplied signal processing for input and/or output audio signals."
        // NB: This turns off the high-pass filter that CoreAudio normally applies.
        do {
            try audioSession.setMode(AVAudioSession.Mode.measurement)
        } catch let error as NSError {
            print(error.localizedDescription)
            return
        }
        
        do {
            try audioSession.setPreferredSampleRate(sampleRate)
        } catch let error as NSError {
            print(error)
            return
        }
        
        // This will have an impact on CPU usage. .01 gives 512 samples per frame on iPhone. (Probably .01 * 44100 rounded up.)
        // NB: This is considered a 'hint' and more often than not is just ignored.
        do {
            try audioSession.setPreferredIOBufferDuration(0.005)
        } catch let error as NSError {
            print(error)
            return
        }
        
        do {
            try audioSession.setActive(true)
        } catch let error as NSError {
            print(error)
            return
        }
        
        print("Finished audioSession setup")
    }
#endif
    
    private func setupAudioUnit() throws {
        var description = AudioComponentDescription()
        description.componentType = kAudioUnitType_Output
#if os(iOS) || os(tvOS)
        description.componentSubType = kAudioUnitSubType_RemoteIO
#else
        description.componentSubType = kAudioUnitSubType_HALOutput
#endif
        description.componentManufacturer = kAudioUnitManufacturer_Apple
        description.componentFlags = 0
        description.componentFlagsMask = 0
        
        // Get an audio component matching our description.
        guard let component = AudioComponentFindNext(nil, &description) else {
            let error = ErrorFactory.errorWith(localDescription: "Failed to find next audio component")
            print(error)
            throw error
        }
        
        // Create an instance of the AudioUnit
        var audioUnit: AudioUnit?
        do {
            let code = AudioComponentInstanceNew(component, &audioUnit)
            if code != 0 {
                let error = ErrorFactory.errorWith(localDescription: "Failed to create new IOUnit with AudioComponentInstanceNew: \(code)")
                print(error)
                throw error
            }
        }
        self.audioUnit = audioUnit
    }
    
    private func setupGenerator() throws {
        guard let audioUnit else {
            let error = ErrorFactory.errorWith(localDescription: "AudioUnit is nil")
            print(error)
            throw error
        }
        
        let pointer = Unmanaged.passUnretained(self).toOpaque()
        var renderCallbackStruct = AURenderCallbackStruct(
            inputProc: generatorCallback as AURenderCallback,
            inputProcRefCon: pointer
        )
        do {
            let code = AudioUnitSetProperty(
                audioUnit,
                AudioUnitPropertyID(kAudioUnitProperty_SetRenderCallback),
                AudioUnitScope(kAudioUnitScope_Input),
                inputBus,
                &renderCallbackStruct,
                UInt32(MemoryLayout<AURenderCallbackStruct>.size)
            )
            if code != 0 {
                let error = ErrorFactory.errorWith(localDescription: "Set kAudioUnitProperty_SetRenderCallback failed: \(code)")
                print(error)
                throw error
            }
        }

        var ioFormat = AudioStreamBasicDescription.linearFloat32(sampleRate: sampleRate)
        do {
            let code = AudioUnitSetProperty(
                audioUnit,
                kAudioUnitProperty_StreamFormat,
                kAudioUnitScope_Input,
                inputBus,
                &ioFormat,
                UInt32(MemoryLayout<AudioStreamBasicDescription>.size)
            )
            if code != 0 {
                let error = ErrorFactory.errorWith(localDescription: "Set kAudioUnitProperty_StreamFormat failed: \(code)")
                print(error)
                throw error
            }
        }

#if os(iOS)
        do {
            var frameCount = UInt32(256)
            let code = AudioUnitSetProperty(
                audioUnit,
                kAudioUnitProperty_MaximumFramesPerSlice,
                kAudioUnitScope_Input,
                inputBus,
                &frameCount,
                UInt32(MemoryLayout<UInt32>.size)
            )
            if code != 0 {
                let error = ErrorFactory.errorWith(localDescription: "Set kAudioUnitProperty_MaximumFramesPerSlice failed: \(code)")
                print(error)
                throw error
            }
        }
        
#endif

        do {
            let code = AudioUnitInitialize(audioUnit)
            if code != 0 {
                let error = ErrorFactory.errorWith(localDescription: "Failed to initialize ioUnit: \(code)")
                print(error)
                throw error
            }
        }
    }
    
    private var once = false
    fileprivate func updateGeneratorBuffer(_ buffer: UnsafeMutablePointer<Float32>, count: UInt32) -> OSStatus {
        if !once {
            once = true
            print("AudioEngine needs \(count) samples per callback")
        }
        
        guard let generatorDataSource = generatorBufferDataSource else {
            return kAudioServicesSystemSoundUnspecifiedError
        }
        
        if dateOfFirstSample == nil {
            dateOfFirstSample = Date()
        }
        
        let timePerSample = TimeInterval(1.0) / TimeInterval(sampleRate)
        
        if isRunning {
            func generateTimeIntervals() -> [TimeInterval] {
//                // Looping (CPU driven)
//                var output = [TimeInterval](repeating: 0, count: Int(count))
//                for i in 0..<Int(count) {
//                    let timeInterval = calculatedTimeInterval + TimeInterval(i) * timePerSample
//                    output[i] = timeInterval
//                }
//                return output

                // Accelerate (GPU driven)
                var startTime = TimeInterval(calculatedTimeInterval + timePerSample)
                var endTime = TimeInterval(calculatedTimeInterval + TimeInterval(count) * timePerSample)
                var output = [TimeInterval](repeating: 0, count: Int(count))
                // "generate" array interpolating values between startTime and en   dTime
                vDSP_vgenD(
                    &startTime,
                    &endTime,
                    &output,
                    1,
                    vDSP_Length(count)
                )
                return output
            }
            let timeIntervals = generateTimeIntervals()
            generatorDataSource.audioManager(
                self,
                fillBuffer: buffer,
                timeIntervals: timeIntervals,
                sampleRate: sampleRate
            )
        } else {
            // TODO: Use SIMD
            // Fill with silence
            for i in 0..<Int(count) {
                buffer[i] = 0
            }
        }
        calculatedTimeInterval += timePerSample * TimeInterval(count)

        // Delegate
        generatorBufferDelegate?.audioManager(
            self,
            bufferWasFilled: buffer,
            count: count,
            timeInterval: calculatedTimeInterval
        )
        return 0
    }
}

private func generatorCallback(
    inRefCon: UnsafeMutableRawPointer,
    ioActionFlags: UnsafeMutablePointer<AudioUnitRenderActionFlags>,
    inTimeStamp: UnsafePointer<AudioTimeStamp>,
    inBusNumber: UInt32,
    inNumberFrames: UInt32,
    ioData: UnsafeMutablePointer<AudioBufferList>?
) -> OSStatus {
    // This callback is a static function which passes in the object to call.
    // Unwrap that object as AudioManager
    let audioManager = unsafeBitCast(inRefCon, to: AudioManager.self)
    let bufferList = UnsafeMutableAudioBufferListPointer(ioData)
    
    guard let buffer = bufferList?[0].mData?.assumingMemoryBound(to: Float32.self) else {
        print("Failed to get reference to audio buffer bytes")
        return kAudioServicesSystemSoundUnspecifiedError
    }
    
    return audioManager.updateGeneratorBuffer(buffer, count: inNumberFrames)
}
