//
//  Logger.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/7/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

struct Logger {
    static func trace(
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        Logger.log(
            level: "trace",
            message: "",
            file: file,
            function: function,
            line: line,
            isError: false
        )
    }

    static func info(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        Logger.log(
            level: "info",
            message: message,
            file: file,
            function: function,
            line: line,
            isError: false
        )
    }

    static func debug(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        Logger.log(
            level: "debug",
            message: message,
            file: file,
            function: function,
            line: line,
            isError: false
        )
    }
    
    static func warn(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        Logger.log(
            level: "warn",
            message: message,
            file: file,
            function: function,
            line: line,
            isError: false
        )
    }

    static func error(
        _ error: Error,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        Logger.error(
            error.localizedDescription,
            file: file,
            function: function,
            line: line
        )
    }
    
    static func error(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        Logger.log(
            level: "error",
            message: message,
            file: file,
            function: function,
            line: line,
            isError: true
        )
    }
    
    private static func log(
        level: String,
        message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line,
        isError: Bool
    ) {
        let delimiter = " : "
        var output = "[\(level)] " + message
        output += delimiter + file
        output += delimiter + function
        output += delimiter + "\(line)"
        
        print(output)
        
#if DEBUG
        if isError {
            assertionFailure("message")
        }
#endif
    }
}
