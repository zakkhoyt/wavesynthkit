//
//  PianoView.swift
//  WaveSynthiOS
//
//  Created by Zakk Hoyt on 12/22/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit
#else
import Cocoa
#endif

public protocol PianoViewDatasource: AnyObject {
    func pianoView(_ pianoView: PianoView, titleForKeyAt index: Int) -> String
}
    
public protocol PianoViewDelegate: AnyObject {
    func pianoView(_ pianoView: PianoView, keyDown index: Int)
    func pianoView(_ pianoView: PianoView, keyUp index: Int)
}

private class KeyLayer: CAShapeLayer {
    var keyIndex = 0
    var keyDown = false
}

public class PianoView: CommonView {
    public weak var delegate: PianoViewDelegate?
    public var dataSource: PianoViewDatasource = DefaultPianoViewDataSource()
    
    private var whiteKeyLayers: [KeyLayer] = []
    private var blackKeyLayers: [KeyLayer] = []
    private var whiteLabels: [UILabel] = []
    private var blackLabels: [UILabel] = []

    override public var canBecomeFirstResponder: Bool { true }
    override public var canResignFirstResponder: Bool { true }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        updateLayers()
    }
    
#if os(iOS) || os(tvOS)
    override public func layoutSubviews() {
        super.layoutSubviews()
        updateLayers()
        isMultipleTouchEnabled = true
    }
#else
    override public func layout() {
        super.layout()
        updateLayers()
    }
#endif
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        // Start listening for keyboard events
        becomeFirstResponder()
    }
    
    private var fontSize: CGFloat { frame.width / (12 * 4) }
    private func updateLayers() {
        ensureLayersExist()
        
        do {
            let count = 7
            let width: CGFloat = bounds.width / CGFloat(count)
            let height: CGFloat = bounds.height
            for i in 0..<count {
                let keyLayer = whiteKeyLayers[i]
                let x = CGFloat(i) * width
                let y: CGFloat = 0
                let frame = CGRect(x: x, y: y, width: width, height: height)
                let path = BezierPath(rect: frame)
                keyLayer.path = path.cgPath
                keyLayer.strokeColor = Color.black.cgColor
                if keyLayer.keyDown {
                    keyLayer.fillColor = Color.gray.cgColor
                } else {
                    keyLayer.fillColor = Color.white.cgColor
                }
                //                #if os(OSX)
                //                    addTrackingRect(frame, owner: self, userData: nil, assumeInside: false)
                //                #endif

                let label = whiteLabels[i]
                label.font = UIFont.systemFont(ofSize: fontSize)
                label.frame = frame.bottomHalf
                label.text = dataSource.pianoView(self, titleForKeyAt: keyLayer.keyIndex)
            }
        }
        
        do {
            let whiteKeyWidth: CGFloat = bounds.width / CGFloat(7)
            let width: CGFloat = frame.width / 5.0 / 2.0
            let height: CGFloat = frame.height / 2.0
            for i in 0..<5 {
                let keyLayer = blackKeyLayers[i]
                var x: CGFloat = whiteKeyWidth - width / 2.0
                switch i {
                case 1: x += 1 * whiteKeyWidth
                case 2: x += 3 * whiteKeyWidth
                case 3: x += 4 * whiteKeyWidth
                case 4: x += 5 * whiteKeyWidth
                default: break
                }
                
                let y: CGFloat = 0
                let frame = CGRect(x: x, y: y, width: width, height: height)
                let path = BezierPath(rect: frame)
                keyLayer.path = path.cgPath
                keyLayer.strokeColor = Color.black.cgColor
                if keyLayer.keyDown {
                    keyLayer.fillColor = Color.gray.cgColor
                } else {
                    keyLayer.fillColor = Color.black.cgColor
                }
                //                #if os(OSX)
                //                addTrackingRect(frame, owner: self, userData: nil, assumeInside: false)
                //                #endif

                let label = blackLabels[i]
                label.font = UIFont.systemFont(ofSize: fontSize)
                label.frame = frame // .bottomHalf
                label.text = dataSource.pianoView(self, titleForKeyAt: keyLayer.keyIndex)
            }
        }
    }
    
    private func ensureLayersExist() {
        func styledLabel() -> UILabel {
            let label = UILabel()
            label.numberOfLines = 0
            // label.backgroundColor = UIColor.purple.withAlphaComponent(0.2)
            label.textAlignment = .center
            label.textColor = .gray
            return label
        }
        
        if whiteKeyLayers.isEmpty {
            for i in 0..<7 {
                let keyLayer = KeyLayer()
                switch i {
                case 0: keyLayer.keyIndex = 0
                case 1: keyLayer.keyIndex = 2
                case 2: keyLayer.keyIndex = 4
                case 3: keyLayer.keyIndex = 5
                case 4: keyLayer.keyIndex = 7
                case 5: keyLayer.keyIndex = 9
                case 6: keyLayer.keyIndex = 11
                default: break
                }
                whiteKeyLayers.append(keyLayer)
#if os(iOS) || os(tvOS)
                layer.addSublayer(keyLayer)
#else
                layer?.addSublayer(keyLayer)
#endif

                let label = styledLabel()
                addSubview(label)
                whiteLabels.append(label)
            }
        }
        
        if blackKeyLayers.isEmpty {
            for i in 0..<5 {
                let keyLayer = KeyLayer()
                switch i {
                case 0: keyLayer.keyIndex = 1
                case 1: keyLayer.keyIndex = 3
                case 2: keyLayer.keyIndex = 6
                case 3: keyLayer.keyIndex = 8
                case 4: keyLayer.keyIndex = 10
                default: break
                }
                blackKeyLayers.append(keyLayer)
#if os(iOS) || os(tvOS)
                layer.addSublayer(keyLayer)
#else
                layer?.addSublayer(keyLayer)
#endif
                
                let label = styledLabel()
                addSubview(label)
                blackLabels.append(label)
            }
        }
    }
    
#if os(iOS) || os(tvOS)
    
    private func keyLayerFrom(touch: UITouch?) -> KeyLayer? {
        guard let touch else { return nil }
        
        let point = touch.location(in: self)
        // print("touch event at point: " + point.debugDescription)
        
        // check black keys first since they are on top z-wise
        for keyLayer in blackKeyLayers {
            guard let path = keyLayer.path else { continue }
            if path.contains(point) {
                return keyLayer
            }
        }
        for keyLayer in whiteKeyLayers {
            guard let path = keyLayer.path else { continue }
            if path.contains(point) {
                return keyLayer
            }
        }
        return nil
    }

    // public override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        guard let touch = touches.first else { return }
        //        let point = touch.location(in: self)
        //        guard self.bounds.contains(point) else { return }
        for touch in touches {
            let point = touch.location(in: self)
            guard bounds.contains(point) else { continue }
            
            if let keyLayer = keyLayerFrom(touch: touch) {
                keyLayer.keyDown = true
                delegate?.pianoView(self, keyDown: keyLayer.keyIndex)
            }
        }
        updateLayers()
    }
    
    //    public override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        guard let touch = touches.first else { return }
        //        let point = touch.location(in: self)
        //        guard self.bounds.contains(point) else { return }
        for touch in touches {
            let point = touch.location(in: self)
            guard bounds.contains(point) else { continue }
            
            if let keyLayer = keyLayerFrom(touch: touch) {
                keyLayer.keyDown = false
                delegate?.pianoView(self, keyUp: keyLayer.keyIndex)
            }
        }
        updateLayers()
    }
    
    private let keyboardMap: [String: Int] = [
        "z": 0,
        "s": 1,
        "x": 2,
        "d": 3,
        "c": 4,
        "v": 5,
        "g": 6,
        "b": 7,
        "h": 8,
        "n": 9,
        "j": 10,
        "m": 11
    ]
    
    private var pressedKeyIndexSet = Set<Int>()
    
    override public var keyCommands: [UIKeyCommand]? {
        keyboardMap.map { UIKeyCommand(input: $0.key, modifierFlags: UIKeyModifierFlags(), action: #selector(keyPressed)) }
    }
    
    @objc
    private func keyPressed(sender: UIKeyCommand) {
        switch sender.input {
        case "z":
            print("z pressed")
        default:
            break
        }
        
        guard let keyMap = keyboardMap.first(where: { $0.key == sender.input }) else {
            return
        }
        
        if pressedKeyIndexSet.contains(keyMap.value) {
            // Do nothing. Key is already pressed (suppress repeats)
        } else {
            // Press then release shortly after
            pressedKeyIndexSet.insert(keyMap.value)
            delegate?.pianoView(self, keyDown: keyMap.value)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) { [weak self] in
                guard let safeSelf = self else { return }
                safeSelf.pressedKeyIndexSet.remove(keyMap.value)
                safeSelf.delegate?.pianoView(safeSelf, keyUp: keyMap.value)
            }
        }
    }
    
    //    public override func cancelTracking(with event: UIEvent?) {
    //        if let keyLayer = keyLayerFrom(touch: touch) {
    //            keyLayer.keyDown = false
    //            delegate?.pianoView(self, keyUp: keyLayer.keyIndex)
    //        }
    //        updateLayers()
    //    }
    
#else
    private func keyLayerFrom(event: NSEvent) -> KeyLayer? {
        let windowPoint = event.locationInWindow
        let point = convert(windowPoint, from: nil)
        print("mouse event at point: " + point.debugDescription)
        
        // check black keys first since they are on top z-wise
        for keyLayer in blackKeyLayers {
            guard let path = keyLayer.path else { continue }
            if path.contains(point) {
                return keyLayer
            }
        }
        for keyLayer in whiteKeyLayers {
            guard let path = keyLayer.path else { continue }
            if path.contains(point) {
                return keyLayer
            }
        }
        return nil
    }
    
    override public func mouseEntered(with event: NSEvent) {
        let windowPoint = event.locationInWindow
        let point = convert(windowPoint, from: nil)
        print("mouse entered at point: " + point.debugDescription)
    }
    
    override public func mouseExited(with event: NSEvent) {
        let windowPoint = event.locationInWindow
        let point = convert(windowPoint, from: nil)
        print("mouse exited at point: " + point.debugDescription)
    }
    
    override public func mouseDown(with event: NSEvent) {
        if let keyLayer = keyLayerFrom(event: event) {
            keyLayer.keyDown = true
            delegate?.pianoView(self, keyDown: keyLayer.keyIndex)
        }
        updateLayers()
    }
    
    override public func mouseUp(with event: NSEvent) {
        if let keyLayer = keyLayerFrom(event: event) {
            keyLayer.keyDown = false
            delegate?.pianoView(self, keyUp: keyLayer.keyIndex)
        }
        updateLayers()
    }
#endif
}

private class DefaultPianoViewDataSource: PianoViewDatasource {
    func pianoView(_ pianoView: PianoView, titleForKeyAt index: Int) -> String {
        switch index {
        case 0: return "C"
        case 1: return "C♯\nD♭"
        case 2: return "D"
        case 3: return "D♯\nE♭"
        case 4: return "E"
        case 5: return "F"
        case 6: return "F♯\nF♭"
        case 7: return "G"
        case 8: return "G♯\nA♭"
        case 9: return "A"
        case 10: return "A♯\nB♭"
        case 11: return "B"
        default: return ""
        }
    }
}
