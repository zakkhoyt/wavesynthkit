//
//  AudioBufferView.swift
//  RTPAudioEngineDemo
//
//  Created by Zakk Hoyt on 2/19/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit

public class AudioBufferView: BufferView {
    override public func draw(_ rect: CGRect) {
        bufferColor.setStroke()
        let path = UIBezierPath()
        let halfHeight = bounds.height / 2.0
        for i in 0..<Int(bounds.width) {
            let percent = Float(i) / Float(bounds.width)
            let s = Int(Float(buffer.count) * percent)

            let point: CGPoint = {
                let x = CGFloat(i)
                guard s < buffer.count else {
                    let y = CGFloat(halfHeight)
                    return CGPoint(x: x, y: y)
                }
                let sample = buffer[s]
                var y = CGFloat(halfHeight - halfHeight * CGFloat(sample))
                if y.isNaN { y = halfHeight }
                return CGPoint(x: x, y: y)
            }()

            if i == 0 {
                path.move(to: point)
            } else {
                path.addLine(to: point)
            }
        }
        path.stroke()
    }
}
#endif
