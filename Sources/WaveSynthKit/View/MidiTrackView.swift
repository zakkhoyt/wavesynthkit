//
//  MidiTrackView.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit

public protocol MidiTrackViewDelegate: AnyObject {
    func midiTrackView(_ midiTrackView: MidiTrackView, keyDownAt index: Int)
    func midiTrackViewKeyUp(_ midiTrackView: MidiTrackView)
}

public class MidiTrackView: UIView {
    public weak var delegate: MidiTrackViewDelegate?
    private let midiFile: Midi.File
    private let trackIndex: Int
    private let track: Midi.File.TrackPayload
    private var lineWidth: CGFloat = 0.25
    private struct NoteRect {
        let startTime: Int
        let endTime: Int
        let noteIndex: Int
        let rect: CGRect
        var duration: TimeInterval {
            TimeInterval(endTime) - TimeInterval(startTime)
        }
    }

    private var noteRects: [NoteRect] = []

    public init(midiFile: Midi.File, trackIndex: Int) {
        self.midiFile = midiFile
        self.trackIndex = trackIndex
        self.track = midiFile.tracks[trackIndex]
        super.init(frame: .zero)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func draw(_ rect: CGRect) {
        // Converts time intervals where the key is down to a set of NoteRect.
        // This maps notes/times to screen rects
        func setupNoteRects() {
            noteRects.removeAll()
            
            let rect = bounds
            let keys = Notes.maximumIndex - Notes.minimumIndex
            let keyHeight: CGFloat = rect.height / CGFloat(keys)

            var clocks: UInt32 = 0
            var key: UInt8 = 0
            var clocksStart: UInt32?
            var clocksStop: UInt32?
            for index in 0..<track.events.count {
                let event = track.events[index]
                clocks += event.deltaTime
                // Do event then prepare for next event
                if let midiEvent = event.midiEvent as? Midi.File.TrackPayload.TimeEvent.MidiEvent,
                   let payload = midiEvent.eventPayload as? Midi.File.TrackPayload.TimeEvent.MidiEvent.MidiChannelVoiceMessagePayload {
                    switch payload {
                    case .noteOn(let tuple):
                        key = tuple.key
                        
                        if let _ = clocksStart {
                            // Sometimes tracks dont' use noteOff, only a bunch of noteOn
                            clocksStop = clocks
                        } else {
                            clocksStart = clocks
                        }
                    case .noteOff:
                        clocksStop = clocks
                    default:
                        break
                    }
                    
                    if let start = clocksStart, let stop = clocksStop {
                        let xStart = CGFloat.interpolate(
                            inputValue: CGFloat(start),
                            minimumInputValue: 0,
                            maximumInputValue: CGFloat(midiFile.lengthInClocks),
                            minimumOutputValue: 0,
                            maximumOutputValue: rect.width
                        )
                        let xStop = CGFloat.interpolate(
                            inputValue: CGFloat(stop),
                            minimumInputValue: 0,
                            maximumInputValue: CGFloat(midiFile.lengthInClocks),
                            minimumOutputValue: 0,
                            maximumOutputValue: rect.width
                        )
                        
                        let yStart = rect.height - (CGFloat(Int(key) - Notes.minimumIndex) * keyHeight)
                        let yStop = yStart + keyHeight
                        
                        let width = max(0, xStop - xStart)
                        let height = yStop - yStart
                        
                        let rect = CGRect(x: xStart, y: yStart, width: width, height: height)
                        let noteRect = NoteRect(startTime: Int(start), endTime: Int(stop), noteIndex: Int(key), rect: rect)
                        noteRects.append(noteRect)
                        
                        clocksStart = nil
                        clocksStop = nil
                    }
                }
            }
        }
        setupNoteRects()
        
        func drawNoteRects() {
            let context = UIGraphicsGetCurrentContext()
            context?.setLineWidth(lineWidth)

            UIColor.red.setStroke()
            UIColor.red.setFill()
            for noteRect in noteRects {
                context?.addRect(noteRect.rect)
                context?.fillPath()
            }
        }
        drawNoteRects()
    }

    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let point = touch.location(in: self)
        guard let noteRect = getNoteRect(point: point) else { return }
        
        delegate?.midiTrackView(self, keyDownAt: noteRect.noteIndex)
        
        let duration: TimeInterval = noteRect.duration * midiFile.clockDuration
        Timer.scheduledTimer(withTimeInterval: duration, repeats: false) { [weak self] _ in
            guard let safeSelf = self else { return }
            safeSelf.delegate?.midiTrackViewKeyUp(safeSelf)
        }
        
        let keyedNoteView = KeyedNoteView(frame: noteRect.rect)
        addSubview(keyedNoteView)
        keyedNoteView.play(duration: duration) {
            keyedNoteView.remove(duration: 0.25) {
                keyedNoteView.removeFromSuperview()
            }
        }
    }
    
    private func getNoteRect(point: CGPoint) -> NoteRect? {
        func closestNoteRect() -> NoteRect? {
            var smallestIndex: Int?
            var smallestDistance = CGFloat.greatestFiniteMagnitude
            
            for (i, noteRect) in noteRects.enumerated() {
                let distance = point.distance(from: noteRect.rect)
                if distance < smallestDistance {
                    smallestDistance = distance
                    smallestIndex = i
                }
            }
            
            guard let index = smallestIndex else { return nil }
            return noteRects[index]
        }

        if let noteRect = (noteRects.first { $0.rect.contains(point) }) {
            return noteRect
        } else if let noteRect = closestNoteRect() {
            return noteRect
        } else {
            return nil
        }
    }
}

extension CGPoint {
    func distance(from rect: CGRect) -> CGFloat {
        let dx = max(rect.minX - x, x - rect.maxX, 0)
        let dy = max(rect.minY - y, y - rect.maxY, 0)
        return dx * dy == 0 ? max(dx, dy) : hypot(dx, dy)
    }
}

extension CGFloat {
    static func interpolate(
        inputValue: CGFloat,
        minimumInputValue: CGFloat,
        maximumInputValue: CGFloat,
        minimumOutputValue: CGFloat,
        maximumOutputValue: CGFloat
    ) -> CGFloat {
        guard inputValue >= minimumInputValue, inputValue < maximumInputValue else { return minimumOutputValue }
        guard minimumInputValue < maximumInputValue else { return minimumOutputValue }
        let progress = (inputValue - minimumInputValue) / (maximumInputValue - minimumInputValue)
        let levelDiff = maximumOutputValue - minimumOutputValue
        return minimumOutputValue + levelDiff * progress
    }
}

class KeyedNoteView: UIView {
    var color: UIColor = .red
    var playColor: UIColor = .cyan
    
    private var playView: UIView?
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = color
    }
    
    func play(duration: TimeInterval, completion: (() -> Void)? = nil) {
        let playView = UIView(frame: .zero)
        playView.backgroundColor = playColor
        addSubview(playView)
        
        let startFrame = CGRect(x: 0, y: 0, width: 0, height: bounds.height)
        let endFrame = bounds
        
        playView.frame = startFrame

        UIView.animate(
            withDuration: duration,
            delay: 0,
            options: UIView.AnimationOptions.curveLinear,
            animations: {
                playView.frame = endFrame
            }
        ) { _ in
            completion?()
        }
        
        self.playView = playView
    }
    
    func remove(duration: TimeInterval, completion: (() -> Void)? = nil) {
        guard let playView else {
            completion?()
            return
        }
        
        UIView.animate(
            withDuration: duration,
            animations: { [weak self] in
                guard let safeSelf = self else { return }
                safeSelf.alpha = 0
            }
        ) { _ in
            playView.removeFromSuperview()
            completion?()
        }
    }
}
#endif
