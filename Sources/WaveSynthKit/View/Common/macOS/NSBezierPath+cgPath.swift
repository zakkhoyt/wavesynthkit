//
//  NSBezierPath+CGBezierPath.swift
//  WaveSynthDemoMacOS
//
//  Created by Zakk Hoyt on 12/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

#if os(macOS)
import AppKit

extension NSBezierPath {
    public var cgPath: CGPath {
        let path = CGMutablePath()
        var points = [CGPoint](repeating: .zero, count: 3)
        for i in 0..<elementCount {
            let firstPoint = points[0]
            let secondPoint = points[1]
            if firstPoint.x.isNaN || firstPoint.y.isNaN {
                print("Found nan at \(i)")
                continue
            }
            if secondPoint.x.isNaN || secondPoint.y.isNaN {
                print("Found nan at \(i)")
                continue
            }

            let type = element(at: i, associatedPoints: &points)
            switch type {
            case .moveTo:
                path.move(to: CGPoint(x: points[0].x, y: points[0].y))
            case .lineTo:
                path.addLine(to: CGPoint(x: points[0].x, y: points[0].y))
            case .curveTo:
                path.addCurve(
                    to: CGPoint(x: points[2].x, y: points[2].y),
                    control1: CGPoint(x: points[0].x, y: points[0].y),
                    control2: CGPoint(x: points[1].x, y: points[1].y)
                )
            case .closePath:
                path.closeSubpath()
            @unknown default:
                break
            }
        }
        return path
    }
    
    public func addLine(to point: NSPoint) {
        line(to: point)
    }
}
#endif
