//
//  TextLayer.swift
//  WaveSynthiOS
//
//  Created by Zakk Hoyt on 12/13/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit

public class TextLayer: CATextLayer {
    public var verticalAlignment: CATextLayerVerticalAlignmentMode = .bottom
    
    override public func draw(in ctx: CGContext) {
        let multipler: CGFloat = {
            guard let string = self.string as? String else { return 1 }
            return CGFloat((string.split(separator: "\n")).count)
        }()
        let height = frame.size.height
        let fontSize = fontSize * multipler

        switch verticalAlignment {
        case .center:
            let yDiff = (height - fontSize) / 2 - fontSize / 10
            ctx.saveGState()
            ctx.translateBy(x: 0.0, y: yDiff)
            super.draw(in: ctx)
            ctx.restoreGState()
        case .top:
            let yDiff: CGFloat = 0
            ctx.saveGState()
            ctx.translateBy(x: 0.0, y: yDiff)
            super.draw(in: ctx)
            ctx.restoreGState()
        case .bottom:
            let yDiff = height - fontSize
            ctx.saveGState()
            ctx.translateBy(x: 0.0, y: yDiff)
            super.draw(in: ctx)
            ctx.restoreGState()
        }
    }
}

public enum CATextLayerVerticalAlignmentMode {
    case top
    case bottom
    case center
}
#endif
