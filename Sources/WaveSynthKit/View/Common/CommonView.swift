//
//  CommonView.swift
//  WaveSynthiOS
//
//  Created by Zakk Hoyt on 12/22/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit

public typealias View = UIView
public typealias Color = UIColor
public typealias BezierPath = UIBezierPath
#else
import Cocoa

public typealias View = NSView
public typealias Color = NSColor
public typealias BezierPath = NSBezierPath
#endif

public class CommonView: View {
    public var lineWidth: CGFloat = 1.0
    public var bufferColor: Color = .systemGreen
    
#if os(macOS)
    public var backgroundColor: Color = NSColor.systemGray
#endif
    
    let bufferLayer = CAShapeLayer()
    let backgroundLayer = CAShapeLayer()
    
    override public func awakeFromNib() {
        super.awakeFromNib()
#if os(iOS) || os(tvOS)
//            self.transform = CGAffineTransform.init(scaleX: 1.0, y: -1.0)
#else
        // Tell our view to use a CALayer for backing
        wantsLayer = true
#endif
    }
    
#if os(OSX)
    func setNeedsDisplay() {
        setNeedsDisplay(bounds)
    }
#endif
}

public class DisplayLinkView: CommonView {
#if os(iOS) || os(tvOS)
    var displayLink: CADisplayLink?
#else
    let displayLink = NSDisplayLink(onQueue: DispatchQueue.main)
#endif

#if os(iOS) || os(tvOS)
    override public func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        backgroundColor = UIColor.systemBackground
        
#if os(iOS) || os(tvOS)
        let displayLink = CADisplayLink(target: self, selector: #selector(update))
        displayLink.add(to: .main, forMode: .default)
        self.displayLink = displayLink
#else
        let displayLink = CADisplayLink(target: self, selector: #selector(update))
        displayLink.callback = { [weak self] in
            self?.displayLinkUpdate()
        }
        displayLink.start()
        self.displayLink = displayLink
#endif
    }
#endif

#if os(iOS) || os(tvOS)
    override public func layoutSubviews() {
        super.layoutSubviews()
        update()
    }
#else
    override public func layout() {
        super.layout()
        update()
    }
#endif
    
    @objc
    func update() {
        setNeedsDisplay()
    }
}

public class BufferView: DisplayLinkView {
    public var buffer: [Float32] = []
}

public class FFTBufferView: BufferView {
    public var sampleRate: Double = 22050
    // public var colors: [UIColor] = [.blue, .magenta, .red]
    public var colors: [Color] = [.systemBlue, .systemTeal, .systemGreen]
    
    var fft: TempiFFT?
    
    override func update() {
        // Process FFT in background thread, then render on main thread
        func processFFT() {
            guard !buffer.isEmpty else { return }
            let fft = TempiFFT(withSize: buffer.count, sampleRate: Float(sampleRate))
            fft.windowType = TempiFFT.FFTType.hanning
            fft.fftForward(buffer)
            
            // Interpoloate the FFT data so there's one band per pixel.
            let screenWidth = UIScreen.main.bounds.size.width * UIScreen.main.scale
            fft.calculateLinearBands(minFrequency: 0, maxFrequency: fft.nyquistFrequency, numberOfBands: Int(screenWidth))
            self.fft = fft
            
            DispatchQueue.main.async { [weak self] in
                self?.setNeedsDisplay()
            }
        }
        
        DispatchQueue.global().async {
            processFFT()
        }
    }
}
