//
//  AudioFFT2View.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/13/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit

public class AudioFFT2View: FFTBufferView {
    private let maxDB: Float = 64.0
    private let minDB: Float = 0.0
    private var headroom: Float { maxDB - minDB }
    private func toLog(magnitude: Float) -> Float {
        var magnitudeDB = TempiFFT.toDB(magnitude)
        magnitudeDB = max(0, magnitudeDB + abs(minDB))
        let dbRatio = min(1.0, magnitudeDB / headroom)
        return dbRatio
    }
    
    override public func draw(_ rect: CGRect) {
        // updateLayers()
        
#if os(iOS) || os(tvOS)
        if let context = UIGraphicsGetCurrentContext() {
            drawFFT(context: context)
        }
#else
        if let context = NSGraphicsContext.current?.cgContext {
            drawFFT(context: context)
        }
#endif
    }
    
    private func drawFFT(context: CGContext) {
        guard let fft else { return }

        context.saveGState()
        context.scaleBy(x: 1, y: -1)
        context.translateBy(x: 0, y: -bounds.size.height)
        
        context.setFillColor(UIColor.systemBackground.cgColor)
        context.fill(bounds)

        let colors = colors.map { $0.cgColor }
        let gradient = CGGradient(
            colorsSpace: nil, // generic color space
            colors: colors as CFArray,
            locations: [0.0, 0.33, 0.66]
        )

        var x: CGFloat = 0.0
        let yStart: CGFloat = 0.0
        let colWidth: CGFloat = bounds.width / CGFloat(fft.numberOfBands)
        let colHeight = bounds.height - yStart
        
        for i in 0..<fft.numberOfBands {
            let dbRatio = toLog(magnitude: fft.magnitudeAtBand(i))
            let magnitudeNorm = CGFloat(dbRatio) * colHeight
            let colRect = CGRect(x: x, y: yStart, width: colWidth, height: magnitudeNorm)
            
            let startPoint = CGPoint(x: bounds.width / 2, y: 0)
            let endPoint = CGPoint(x: bounds.width / 2, y: bounds.height)
            
            context.saveGState()
            context.clip(to: colRect)
            context.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: 0))
            context.restoreGState()
            x += colWidth
        }
    }
}
#endif
