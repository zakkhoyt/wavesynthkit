//
//  PianoScrollView.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit

public protocol PianoScrollViewDelegate: AnyObject {
    func pianoScrollView(_ pianoScrollView: PianoScrollView, keyDown index: Int)
    func pianoScrollView(_ pianoScrollView: PianoScrollView, keyUp index: Int)
}

public class PianoScrollView: UIView {
    public var numberOfOctaves = 7
    public weak var delegate: PianoScrollViewDelegate?
    
    private let backingView = UIView(frame: .zero)
    private let scrollView = UIScrollView(frame: .zero)
    private let pianoStackView = UIStackView(frame: .zero)
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        scrollView.alwaysBounceVertical = false
        scrollView.alwaysBounceHorizontal = false
        scrollView.bounces = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollView)
        
        scrollView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true

        backingView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(backingView)
        
        backingView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        backingView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        backingView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        backingView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true

        backingView.heightAnchor.constraint(equalTo: scrollView.heightAnchor).isActive = true
        backingView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: CGFloat(numberOfOctaves)).isActive = true
        
        pianoStackView.axis = .horizontal
        pianoStackView.distribution = .fillEqually
        pianoStackView.translatesAutoresizingMaskIntoConstraints = false
        backingView.addSubview(pianoStackView)
        pianoStackView.leadingAnchor.constraint(equalTo: backingView.leadingAnchor).isActive = true
        pianoStackView.trailingAnchor.constraint(equalTo: backingView.trailingAnchor).isActive = true
        pianoStackView.bottomAnchor.constraint(equalTo: backingView.bottomAnchor).isActive = true
        pianoStackView.topAnchor.constraint(equalTo: backingView.topAnchor).isActive = true
        // pianoStackView.heightAnchor.constraint(equalTo: backingView.heightAnchor, multiplier: 0.5).isActive = true
        
        for i in 0..<numberOfOctaves {
            let pianoView = PianoView()
            pianoView.tag = i
            pianoView.dataSource = self
            pianoView.delegate = self
            pianoStackView.addArrangedSubview(pianoView)
        }
    }
    
    public func scroll(toOctave octave: Int, animated: Bool = true) {
        let x = scrollView.contentSize.width * CGFloat(octave) / CGFloat(numberOfOctaves)
        let y: CGFloat = 0
        let rect = CGRect(x: x, y: y, width: scrollView.bounds.width, height: scrollView.bounds.height)
        scrollView.scrollRectToVisible(rect, animated: animated)
    }
}

extension PianoScrollView: PianoViewDatasource {
    public func pianoView(_ pianoView: PianoView, titleForKeyAt index: Int) -> String {
        let octave = pianoView.tag
        let adjustedIndex = Notes.minimumIndex + octave * Notes.stepsPerOctave + index
        let note = Notes.note(at: adjustedIndex)
        // return note.name
//        return note.name + "\n\(Int(note.frequency))"
        return "\(Int(note.frequency))Hz\n" + note.name
//        return String(format: "%.1f", note.frequency)
//        return "\(adjustedIndex)"
    }
}

extension PianoScrollView: PianoViewDelegate {
    public func pianoView(_ pianoView: PianoView, keyDown index: Int) {
        let octave = pianoView.tag
        let adjustedIndex = Notes.minimumIndex + octave * Notes.stepsPerOctave + index
        delegate?.pianoScrollView(self, keyDown: adjustedIndex)
    }
    
    public func pianoView(_ pianoView: PianoView, keyUp index: Int) {
        let octave = pianoView.tag
        let adjustedIndex = Notes.minimumIndex + octave * Notes.stepsPerOctave + index
        delegate?.pianoScrollView(self, keyUp: adjustedIndex)
    }
}
#endif
