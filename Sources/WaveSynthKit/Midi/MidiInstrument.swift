//
//  MidiInstrument.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

// https://www.midi.org/specifications-old/item/gm-level-1-sound-set
public enum MidiInstrument: UInt8, CustomStringConvertible {
    case acousticGrandPiano = 1
    case brightAcousticPiano = 2
    case electricGrandPiano = 3
    case honkyTonkPiano = 4
    case electricPiano1 = 5
    case electricPiano2 = 6
    case harpsichord = 7
    case clavi = 8
    case celesta = 9
    case glockenspiel = 10
    case musicBox = 11
    case vibraphone = 12
    case marimba = 13
    case xylophone = 14
    case tubularBells = 15
    case dulcimer = 16
    case drawbarOrgan = 17
    case percussiveOrgan = 18
    case rockOrgan = 19
    case churchOrgan = 20
    case reedOrgan = 21
    case accordion = 22
    case harmonica = 23
    case tangoAccordion = 24
    case acousticGuitarNylon = 25
    case acousticGuitarSteel = 26
    case electricGuitarJazz = 27
    case electricGuitarClean = 28
    case electricGuitarMuted = 29
    case overdrivenGuitar = 30
    case distortionGuitar = 31
    case guitarharmonics = 32
    case acousticBass = 33
    case electricBassFinger = 34
    case electricBassPick = 35
    case fretlessBass = 36
    case slapBass1 = 37
    case slapBass2 = 38
    case synthBass1 = 39
    case synthBass2 = 40
    case violin = 41
    case viola = 42
    case cello = 43
    case contrabass = 44
    case tremoloStrings = 45
    case pizzicatoStrings = 46
    case orchestralHarp = 47
    case timpani = 48
    case stringEnsemble1 = 49
    case stringEnsemble2 = 50
    case synthStrings1 = 51
    case synthStrings2 = 52
    case choirAahs = 53
    case voiceOohs = 54
    case synthVoice = 55
    case orchestraHit = 56
    case trumpet = 57
    case trombone = 58
    case tuba = 59
    case mutedTrumpet = 60
    case frenchHorn = 61
    case brassSection = 62
    case synthBrass1 = 63
    case synthBrass2 = 64
    case sopranoSax = 65
    case altoSax = 66
    case tenorSax = 67
    case baritoneSax = 68
    case oboe = 69
    case englishHorn = 70
    case bassoon = 71
    case clarinet = 72
    case piccolo = 73
    case flute = 74
    case recorder = 75
    case panFlute = 76
    case blownBottle = 77
    case shakuhachi = 78
    case whistle = 79
    case ocarina = 80
    case lead1Square = 81
    case lead2Sawtooth = 82
    case lead3Calliope = 83
    case lead4Chiff = 84
    case lead5Charang = 85
    case lead6Voice = 86
    case lead7Fifths = 87
    case lead8BassLead = 88
    case pad1Newage = 89
    case pad2Warm = 90
    case pad3Polysynth = 91
    case pad4Choir = 92
    case pad5Bowed = 93
    case pad6Metallic = 94
    case pad7Halo = 95
    case pad8Sweep = 96
    case fX1Rain = 97
    case fX2Soundtrack = 98
    case fX3Crystal = 99
    case fX4Atmosphere = 100
    case fX5Brightness = 101
    case fX6Goblins = 102
    case fX7Echoes = 103
    case fX8SciFi = 104
    case sitar = 105
    case banjo = 106
    case shamisen = 107
    case koto = 108
    case kalimba = 109
    case bagpipe = 110
    case fiddle = 111
    case shanai = 112
    case tinkleBell = 113
    case agogo = 114
    case steelDrums = 115
    case woodblock = 116
    case taikoDrum = 117
    case melodicTom = 118
    case synthDrum = 119
    case reverseCymbal = 120
    case guitarFretNoise = 121
    case breathNoise = 122
    case seashore = 123
    case birdTweet = 124
    case telephoneRing = 125
    case helicopter = 126
    case applause = 127
    case gunshot = 128
    
    public var description: String {
        switch self {
        case .acousticGrandPiano: return "AcousticGrandPiano"
        case .brightAcousticPiano: return "BrightAcousticPiano"
        case .electricGrandPiano: return "ElectricGrandPiano"
        case .honkyTonkPiano: return "HonkyTonkPiano"
        case .electricPiano1: return "ElectricPiano1"
        case .electricPiano2: return "ElectricPiano2"
        case .harpsichord: return "Harpsichord"
        case .clavi: return "Clavi"
        case .celesta: return "Celesta"
        case .glockenspiel: return "Glockenspiel"
        case .musicBox: return "MusicBox"
        case .vibraphone: return "Vibraphone"
        case .marimba: return "Marimba"
        case .xylophone: return "Xylophone"
        case .tubularBells: return "TubularBells"
        case .dulcimer: return "Dulcimer"
        case .drawbarOrgan: return "DrawbarOrgan"
        case .percussiveOrgan: return "PercussiveOrgan"
        case .rockOrgan: return "RockOrgan"
        case .churchOrgan: return "ChurchOrgan"
        case .reedOrgan: return "ReedOrgan"
        case .accordion: return "Accordion"
        case .harmonica: return "Harmonica"
        case .tangoAccordion: return "TangoAccordion"
        case .acousticGuitarNylon: return "AcousticGuitarNylon"
        case .acousticGuitarSteel: return "AcousticGuitarSteel"
        case .electricGuitarJazz: return "ElectricGuitarJazz"
        case .electricGuitarClean: return "ElectricGuitarClean"
        case .electricGuitarMuted: return "ElectricGuitarMuted"
        case .overdrivenGuitar: return "OverdrivenGuitar"
        case .distortionGuitar: return "DistortionGuitar"
        case .guitarharmonics: return "Guitarharmonics"
        case .acousticBass: return "AcousticBass"
        case .electricBassFinger: return "ElectricBassFinger"
        case .electricBassPick: return "ElectricBassPick"
        case .fretlessBass: return "FretlessBass"
        case .slapBass1: return "SlapBass1"
        case .slapBass2: return "SlapBass2"
        case .synthBass1: return "SynthBass1"
        case .synthBass2: return "SynthBass2"
        case .violin: return "Violin"
        case .viola: return "Viola"
        case .cello: return "Cello"
        case .contrabass: return "Contrabass"
        case .tremoloStrings: return "TremoloStrings"
        case .pizzicatoStrings: return "PizzicatoStrings"
        case .orchestralHarp: return "OrchestralHarp"
        case .timpani: return "Timpani"
        case .stringEnsemble1: return "StringEnsemble1"
        case .stringEnsemble2: return "StringEnsemble2"
        case .synthStrings1: return "SynthStrings1"
        case .synthStrings2: return "SynthStrings2"
        case .choirAahs: return "ChoirAahs"
        case .voiceOohs: return "VoiceOohs"
        case .synthVoice: return "SynthVoice"
        case .orchestraHit: return "OrchestraHit"
        case .trumpet: return "Trumpet"
        case .trombone: return "Trombone"
        case .tuba: return "Tuba"
        case .mutedTrumpet: return "MutedTrumpet"
        case .frenchHorn: return "FrenchHorn"
        case .brassSection: return "BrassSection"
        case .synthBrass1: return "SynthBrass1"
        case .synthBrass2: return "SynthBrass2"
        case .sopranoSax: return "SopranoSax"
        case .altoSax: return "AltoSax"
        case .tenorSax: return "TenorSax"
        case .baritoneSax: return "BaritoneSax"
        case .oboe: return "Oboe"
        case .englishHorn: return "EnglishHorn"
        case .bassoon: return "Bassoon"
        case .clarinet: return "Clarinet"
        case .piccolo: return "Piccolo"
        case .flute: return "Flute"
        case .recorder: return "Recorder"
        case .panFlute: return "PanFlute"
        case .blownBottle: return "BlownBottle"
        case .shakuhachi: return "Shakuhachi"
        case .whistle: return "Whistle"
        case .ocarina: return "Ocarina"
        case .lead1Square: return "Lead1Square"
        case .lead2Sawtooth: return "Lead2Sawtooth"
        case .lead3Calliope: return "Lead3Calliope"
        case .lead4Chiff: return "Lead4Chiff"
        case .lead5Charang: return "Lead5Charang"
        case .lead6Voice: return "Lead6Voice"
        case .lead7Fifths: return "Lead7Fifths"
        case .lead8BassLead: return "Lead8BassLead"
        case .pad1Newage: return "Pad1Newage"
        case .pad2Warm: return "Pad2Warm"
        case .pad3Polysynth: return "Pad3Polysynth"
        case .pad4Choir: return "Pad4Choir"
        case .pad5Bowed: return "Pad5Bowed"
        case .pad6Metallic: return "Pad6Metallic"
        case .pad7Halo: return "Pad7Halo"
        case .pad8Sweep: return "Pad8Sweep"
        case .fX1Rain: return "FX1Rain"
        case .fX2Soundtrack: return "FX2Soundtrack"
        case .fX3Crystal: return "FX3Crystal"
        case .fX4Atmosphere: return "FX4Atmosphere"
        case .fX5Brightness: return "FX5Brightness"
        case .fX6Goblins: return "FX6Goblins"
        case .fX7Echoes: return "FX7Echoes"
        case .fX8SciFi: return "FX8SciFi"
        case .sitar: return "Sitar"
        case .banjo: return "Banjo"
        case .shamisen: return "Shamisen"
        case .koto: return "Koto"
        case .kalimba: return "Kalimba"
        case .bagpipe: return "Bagpipe"
        case .fiddle: return "Fiddle"
        case .shanai: return "Shanai"
        case .tinkleBell: return "TinkleBell"
        case .agogo: return "Agogo"
        case .steelDrums: return "SteelDrums"
        case .woodblock: return "Woodblock"
        case .taikoDrum: return "TaikoDrum"
        case .melodicTom: return "MelodicTom"
        case .synthDrum: return "SynthDrum"
        case .reverseCymbal: return "ReverseCymbal"
        case .guitarFretNoise: return "GuitarFretNoise"
        case .breathNoise: return "BreathNoise"
        case .seashore: return "Seashore"
        case .birdTweet: return "BirdTweet"
        case .telephoneRing: return "TelephoneRing"
        case .helicopter: return "Helicopter"
        case .applause: return "Applause"
        case .gunshot: return "Gunshot"
        }
    }
}

public enum MidiInstrumentFamily: CustomStringConvertible {
    case piano
    case chromaticPercussion
    case organ
    case guitar
    case bass
    case strings
    case ensemble
    case brass

    case reed
    case pipe
    case synthLead
    case synthPad
    case synthEffects
    case ethnic
    case percussive
    case soundEffects
    
    init?(instrument: MidiInstrument) {
        switch instrument.rawValue {
        case 1...8: self = .piano
        case 0...16: self = .chromaticPercussion
        case 17...24: self = .organ
        case 25...32: self = .guitar
        case 33...40: self = .bass
        case 41...48: self = .strings
        case 49...56: self = .ensemble
        case 57...64: self = .brass
        case 65...72: self = .reed
        case 73...80: self = .pipe
        case 81...88: self = .synthLead
        case 89...96: self = .synthPad
        case 97...104: self = .synthEffects
        case 105...112: self = .ethnic
        case 113...120: self = .percussive
        case 121...128: self = .soundEffects
        default: return nil
        }
    }
    
    public var description: String {
        switch self {
        case .piano: return "Piano"
        case .chromaticPercussion: return "Chromatic Percussion"
        case .organ: return "Organ"
        case .guitar: return "Guitar"
        case .bass: return "Bass"
        case .strings: return "Strings"
        case .ensemble: return "Ensemble"
        case .brass: return "Brass"
        case .reed: return "Reed"
        case .pipe: return "Pipe"
        case .synthLead: return "Synth  Lead"
        case .synthPad: return "Syntch Pad"
        case .synthEffects: return "Synth Effects"
        case .ethnic: return "Ethnic"
        case .percussive: return "Percussive"
        case .soundEffects: return "Sound Effects"
        }
    }
}
