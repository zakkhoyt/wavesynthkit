//
//  Midi+Header.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/7/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Midi.File {
    public struct HeaderPayload: MidiChunkPayload {
        public enum FileFormat: Int, CustomStringConvertible, CustomDebugStringConvertible {
            case format0
            case format1
            case format2

            public var description: String {
                switch self {
                case .format0: return "Format 0"
                case .format1: return "Format 1"
                case .format2: return "Format 2"
                }
            }

            public var debugDescription: String {
                switch self {
                case .format0:
                    return """
                    Format 0 MIDI files consist of a header-chunk and a single track-chunk.
                    The single track chunk will contain all the note and tempo information.
                    """
                case .format1:
                    return """
                    Consist of a header-chunk and one or more track-chunks, with all tracks being played simultaneously.
                    The first track of a Format 1 file is special, and is also known as the 'Tempo Map'.
                    It should contain all meta-events of the types Time Signature, and Set Tempo.
                    The meta-events Sequence/Track Name, Sequence Number, Marker, and SMTPE Offset.
                    It should also be on the first track of a Format 1 file.
                    """
                case .format2:
                    return """
                    Format 2 MIDI files consist of a header-chunk and one or more track-chunks, where each track represents an independant sequence.
                    """
                }
            }
        }
        
        public enum DivisionFormat: CustomStringConvertible {
            case perQuarterNote(unitsPerQuarterNote: UInt16)
            case perFrame(unitsPerSMTPEFrame: UInt8, smtpeFramesPerSecond: UInt8)
            
            public var description: String {
                switch self {
                case .perQuarterNote: return "perQuarterNote"
                case .perFrame: return "perFrame"
                }
            }
        }
        
        public let fileFormat: FileFormat
        public let tracks: UInt16
        public let divisionFormat: DivisionFormat
        
        init?(data: Data) {
            let fileFormatData = data[0..<2]
            let fileFormatValue: UInt16 = fileFormatData.unpack()
            guard let fileFormat = FileFormat(rawValue: Int(fileFormatValue)) else {
                return nil
            }
            self.fileFormat = fileFormat
            
            let tracks: UInt16 = Data(data[2..<4]).unpack()

            self.tracks = tracks

            let divisionFormatValue: UInt16 = data[4..<6].unpack()
            
            // if bit 15 is set, then
            if divisionFormatValue & 0b1000000000000000 == 0b1000000000000000 {
                let unitsPerSMTPEFrame = UInt8(divisionFormatValue & 0x00FF) // bits 0-7
                let smtpeFramesPerSecond = UInt8((divisionFormatValue & 0x7F00) >> 8) // bits 8-14
                self.divisionFormat = DivisionFormat.perFrame(unitsPerSMTPEFrame: unitsPerSMTPEFrame, smtpeFramesPerSecond: smtpeFramesPerSecond)
            } else {
                let unitsPerQuarterNote = divisionFormatValue & 0x7FFF
                self.divisionFormat = DivisionFormat.perQuarterNote(unitsPerQuarterNote: unitsPerQuarterNote)
            }
        }
    }
}
