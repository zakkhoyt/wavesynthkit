//
//  Midi+Reader.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Midi {
    public class Reader: DataParsable {
        public var file: Midi.File?
        
        var data: Data
        var index = 0
        
        public static func read(url: URL) -> Midi.File? {
            do {
                let data = try Data(contentsOf: url)
                let reader = Midi.Reader(data: data)
                return reader?.file
            } catch {
                print("""
                Failed to load midi file at:
                \(url.absoluteString)
                with error:
                \(error.localizedDescription))
                """)
                return nil
            }
        }

        required init?(data: Data) {
            self.data = data
            self.file = parseFile()
        }

        private func parseFile() -> Midi.File? {
            guard let header = readNextChunk()?.payload as? File.HeaderPayload else {
                Logger.error("No header found")
                return nil
            }
            
            var tracks: [Midi.File.TrackPayload] = []
            var unknowns: [Midi.File.UnknownPayload] = []
            for i in 0..<header.tracks {
                Logger.debug("Begin parsing track \(i)")
                guard let chunk = readNextChunk() else {
                    Logger.error("Expected another chunk according to header")
                    continue
                }
                if let _ = chunk.payload as? File.HeaderPayload {
                    // unexpected
                    Logger.error("We already have a header")
                } else if let track = chunk.payload as? File.TrackPayload {
                    Logger.debug("Appending track #\(tracks.count)")
                    tracks.append(track)
                } else if let unknown = chunk.payload as? File.UnknownPayload {
                    Logger.debug("Appending unknown #\(unknowns.count)")
                    unknowns.append(unknown)
                } else {
                    Logger.error("Unrecognized chunk")
                }
            }
            let file = File(
                header: header,
                tracks: tracks,
                unknowns: unknowns
            )
            return file
        }
    }
}

// Private Reader functionz
extension Midi.Reader {
    private func readNextChunk() -> Midi.File.Chunk? {
        guard let typeData = read(count: 4) else {
            print("Reached end of data")
            return nil
        }
        guard let typeString = String(data: typeData, encoding: .utf8) else {
            print("Failed to cast typeData to string")
            return nil
        }
        guard let chunkType = Midi.File.Chunk.ChunkType(rawValue: typeString) else {
            print("Failed to cast typeString to chunkType")
            return nil
        }
        
        guard let lengthData = read(count: 4) else {
            print("Reached end of data")
            return nil
        }

        let length: UInt32 = lengthData.unpack()

        // TODO: Check for variable lenght here
        
        guard let payloadData = read(count: Int(length)) else {
            print("Reached end of data")
            return nil
        }

        func extractPayload() -> MidiChunkPayload? {
            switch chunkType {
            case .header:
                guard let header = Midi.File.HeaderPayload(data: payloadData) else {
                    assertionFailure("failed to read header")
                    return nil
                }
                return header
            case .track:
                guard let track = Midi.File.TrackPayload(data: payloadData) else {
                    assertionFailure("Failed to read track")
                    return nil
                }
                return track
            case .unknown:
                let unknwown = Midi.File.UnknownPayload()
                return unknwown
            }
        }
        guard let payload: MidiChunkPayload = extractPayload() else {
            assertionFailure("Failed to parse chunk payload")
            return nil
        }
        
        return Midi.File.Chunk(
            length: length,
            payloadData: payloadData,
            payload: payload
        )
    }
}
