//
//  Midi+Unknown.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/7/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Midi.File {
    public class UnknownPayload: MidiChunkPayload {
        init() {}
    }
}
