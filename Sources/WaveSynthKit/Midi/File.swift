//
//  Midi.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/5/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  https://www.csie.ntu.edu.tw/~r92092/ref/midi/
//  http://www.music.mcgill.ca/~ich/classes/mumt306/StandardMIDIfileformat.html
//  https://www.midi.org/specifications-old/item/the-midi-1-0-specification
//  https://en.wikipedia.org/wiki/General_MIDI

import Foundation

public struct Midi {}

public protocol MidiChunkPayload {}

public protocol MidiTrackEvent: CustomStringConvertible {}

public protocol MidiChannelMessage: CustomStringConvertible {}

public protocol MidiChannelPayload: CustomStringConvertible {}

extension Midi {
    public class File {
        public class Chunk {
            public enum ChunkType: String {
                /// Unknown chunk type
                case unknown = "????"
                
                /// Header chunk
                case header = "MThd"
                
                /// Track chunk
                case track = "MTrk"
            }
            
            /// A 4-byte chunk type (ascii)
            public let type: ChunkType
            
            /// A 4-byte length (32 bits, msb first)
            public let length: UInt32
            
            /// `length` bytes of data
            public let data: Data
            
            public let payload: MidiChunkPayload
            
            init(
                length: UInt32,
                payloadData: Data,
                payload: MidiChunkPayload
            ) {
                self.type = {
                    switch payload {
                    case is HeaderPayload: return .header
                    case is TrackPayload: return .track
                    default: return .unknown
                    }
                }()
                
                self.length = length
                self.data = payloadData
                self.payload = payload
            }
        }
        
        public let header: HeaderPayload
        public internal(set) var tracks: [TrackPayload]
        public internal(set) var unknowns: [UnknownPayload]
        
        public var timeSignature: TimeSignature = .default()
        
        public var tempo: Tempo = .default()
        public private(set) var clockDuration: TimeInterval = 0.020833 // default value of (0.500000 / 24)
        
        public var duration: TimeInterval = 0
        public var lengthInClocks: UInt32 = 0
        
        public var quarterNoteDuration: TimeInterval {
            switch header.divisionFormat {
            case .perQuarterNote(let unitsPerQuarterNote):
                return TimeInterval(unitsPerQuarterNote) * clockDuration
            case .perFrame:
                return 0
            }
        }
        
        public var numberOfQuarterNotes: Int {
            Int(duration / quarterNoteDuration)
        }

        init(
            header: HeaderPayload,
            tracks: [TrackPayload],
            unknowns: [UnknownPayload]
        ) {
            self.header = header
            self.tracks = tracks
            self.unknowns = unknowns
            
            if let timeSignature = extractTimeSignature() {
                self.timeSignature = timeSignature
            }
            
            if let tempo = extractTempo() {
                self.tempo = tempo
            }
            
            self.clockDuration = extractClockDuration()
            self.duration = extractDuration()
            self.lengthInClocks = extractLengthInClocks()
        }
        
        private func extractTimeSignature() -> TimeSignature? {
            guard let track = tracks.first else { return nil }
            for event in track.events {
                if let midiEvent = event.midiEvent as? Midi.File.TrackPayload.TimeEvent.MetaEvent {
                    switch midiEvent.eventPayload {
                    case .timeSignature(let timeSignature): return timeSignature
                    default: continue
                    }
                }
            }
            return nil
        }

        private func extractTempo() -> Tempo? {
            guard let track = tracks.first else { return nil }
            for event in track.events {
                if let midiEvent = event.midiEvent as? Midi.File.TrackPayload.TimeEvent.MetaEvent {
                    switch midiEvent.eventPayload {
                    case .setTempo(let tempo): return tempo
                    default: continue
                    }
                }
            }
            return nil
        }

        private func extractClockDuration() -> TimeInterval {
            let rate = TimeInterval(tempo.rate)
            let clocksPerTick: TimeInterval = {
                switch header.divisionFormat {
                case .perQuarterNote(let unitsPerQuarterNote):
                    return TimeInterval(unitsPerQuarterNote)
                case .perFrame:
                    // TODO: Deal with this
                    return TimeInterval(24)
                }
            }()
//            let multiplier: TimeInterval = TimeInterval(clocksPerTick) / TimeInterval(timeSignature.clocksPerTick)
//            print("multiplier: \(multiplier)")
            return ((rate / clocksPerTick) / 1000000)
        }
        
        private func extractDuration() -> TimeInterval {
            TimeInterval(extractLengthInClocks()) * clockDuration
        }

        private func extractLengthInClocks() -> UInt32 {
            var maximum: UInt32 = 0
            for track in tracks {
                maximum = max(track.lengthInClocks, maximum)
            }
            return maximum
        }

        /// A MIDI file consists of a single header chunk followed by one or more track chunks.
        ///
        /// Since the length-field is mandatory in the structure of chunks, it is possible to accomodate chunks other than "MThd" or "MTrk" in a MIDI file, by skipping over their contents. The MIDI specification requires that software be able to handle unexpected chunk-types by ignoring the entire chunk.
        ///
        ///
        ///
        /// ### Value Representations
        
        /// Within a MIDI file, there is a variety of information in addition to the basic MIDI data, such as delta-times and meta-events.
        
        /// Where such information contains numeric values, these are generally represented in one of two formats:
        /// binary
        /// viraible length quantity
        ///
        ///
        ///
        /// ### Header Chunks
        /// The data part of a header chunk contains three 16-bit fields. These fields specify the format, number of tracks, and timing for the MIDI file.
        /// The length of the header chunk is 6-bytes. However, software which reads MIDI files is required to honour the length field, even if it is greater than expected. Any unexpected data must be ignored.
        ///
        ///
        ///
    }
}
