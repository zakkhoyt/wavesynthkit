//
//  DataParser.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/6/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public typealias VariableLengthQuantity = UInt32

protocol DataParsable: AnyObject {
    var index: Int { get set }
    var data: Data { get set }
    init?(data: Data)
    func readByte() -> UInt8
    func read(count: Int) -> Data?
    func readVariableLengthQuantity() -> VariableLengthQuantity?
}

extension DataParsable {
    //    init(data: Data) {
    //        self.data = data
    //        self.index = 0
    //    }
    
    func readByte() -> UInt8 {
        let byte = data[index]
        index += 1
        return byte
    }

    func read(count: Int) -> Data? {
        let start = index
        let end = index + count
        
        if start >= data.count || end > data.count {
            print("We have reached the end index of data. Returning nil")
            return nil
        }
        print("reading bytes \(start) ..< \(end) out of \(data.count)")
        let subdata = data[start..<end]
        index += count
        return Data(subdata)
    }
    
    func readVariableLengthQuantity() -> VariableLengthQuantity? {
        // Max of 4 bytes (32 bit)
        var output: UInt32 = 0
        for _ in 0..<4 {
            guard index < data.count else {
                Logger.warn("index is past endIndex")
                return nil
            }
            
            let byte = data[index]
            index += 1
            
            let valueMask: UInt8 = 0b01111111
            // Check if msb is set
            let msbMask: UInt8 = 0b10000000
            
            let value = byte & valueMask
            
            // Shift existing bits over by 7
            output = output << 7
            // Add the 7 bits we just read
            output += UInt32(value)
            
            if byte & msbMask == 0 {
                // MSB is clear. We are done
                break
            } else {
                // keep going
            }
        }
        return output
    }
}

public typealias UInt24 = UInt32
extension Data {
    /// Unpacks UIntX from data
    ///         let byte: UInt8 = data.unpack()
    ///         let sixteen: UInt16 = data.unpack()
    ///         let thirtyTwo: UInt32 = data.unpack()
    ///         print("byte: \(String(format: "%X", byte))")
    ///         print("sixteen: \(String(format: "%X", sixteen))")
    ///         print("thirtyTwo: \(String(format: "%X", thirtyTwo))")
    func unpack<T>() -> T where T: FixedWidthInteger, T: UnsignedInteger {
        let start = startIndex
        let size = MemoryLayout<T>.size
        let end = start + size
        return self[start..<end].withUnsafeBytes {
            $0.load(as: T.self).byteSwapped
        }
    }

    /// A special function fo extracting UInt24 (which is really UInt32)
    func unpackUInt24() -> UInt24 {
        let start = startIndex
        let size = 3
        let end = start + size
        
        // Extract the 3 bytes then pad it out to 4 bytes (basically add leading zeros)
        var paddedData = self[start..<end]
        paddedData.insert(0x00, at: 0)
        // Cast back out to UInt32 (aka UInt24)
        return paddedData.withUnsafeBytes {
            $0.load(as: UInt24.self).byteSwapped
        }
    }
}
