//
//  MidiClockSource.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

class MidiClockSource {
    public enum State: CustomStringConvertible {
        case stopped
        case started
        
        public var description: String {
            switch self {
            case .stopped: return "Stopped"
            case .started: return "Started"
            }
        }
    }
    
    private var state: State = .stopped
    private var startDate: Date
    private var update: ((TimeInterval) -> Void)?
    private var queue: DispatchQueue
    private var interval: TimeInterval
    init(interval: TimeInterval) {
        self.interval = interval
        self.queue = DispatchQueue(label: "sequencer")
        self.startDate = Date()
    }
    
    private var timer: Timer?
    private var lastDate = Date()
    func start(update: @escaping (TimeInterval) -> Void) {
        self.update = update
        startDate = Date()
        state = .started
//        func scheduleNext() {
//            guard state == .started else { return }
//            queue.asyncAfter(deadline: .now() + interval) { [weak self] in
//                guard let safeSelf = self else { return }
//                let interval = Date().timeIntervalSince(safeSelf.startDate)
//                self?.update?(interval)
//                scheduleNext()
//            }
//        }
//        scheduleNext()
        timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { [weak self] _ in
            guard let safeSelf = self else { return }
            let interval = Date().timeIntervalSince(safeSelf.startDate)
            self?.update?(interval)
        })
    }
    
    func stop() {
        state = .stopped
        timer?.invalidate()
        timer = nil
    }
}
