//
//  MidiSequencer.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/10/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

// Clock message is sent ever 1/24th of a beat (PPQ or pulses per quarter note)
//  Found in timeSignature.clocksPerTick
// A beat is defined by setTempo in terms of micro seconds
//  Found in tempo.rate
//
// clockDuration = tempo.rate / timeSignature.clocksPerTick

import Foundation

public protocol MidiSequencerDelegate: AnyObject {
    func midiSequencer(_ midiSequencer: MidiSequencer, didChangeState state: MidiSequencer.State)
    
    func midiSequencer(_ midiSequencer: MidiSequencer, midiEventTime: MidiEventTime, channel: Int, keyDown index: Int)
    func midiSequencerKeyUp(_ midiSequencer: MidiSequencer, midiEventTime: MidiEventTime, channel: Int)
}

public class MidiSequencer {
    public weak var delegate: MidiSequencerDelegate?
    
    public enum State: CustomStringConvertible {
        case stopped
        case started
        
        public var description: String {
            switch self {
            case .stopped: return "Stopped"
            case .started: return "Started"
            }
        }
    }
    
    public private(set) var state: State = .stopped {
        didSet {
            Logger.debug("did set MidiSequencer.state: \(state.description)")
            delegate?.midiSequencer(self, didChangeState: state)
        }
    }

    public private(set) var midiFile: Midi.File
    public private(set) var trackSequencers: [MidiTrackSequencer] = []
    private var midiClock: MidiClockSource
    private var clockCounter: UInt32 = 0
    private var nextClockTimeInterval: TimeInterval = 0

    public init(midiFile: Midi.File) {
        self.midiFile = midiFile
        
        self.midiClock = MidiClockSource(interval: midiFile.clockDuration)
    }
    
    public func start() {
        trackSequencers.removeAll()
        for track in midiFile.tracks {
            let trackSequencer = MidiTrackSequencer(
                track: track,
                clockDuration: midiFile.clockDuration,
                delegate: self
            )
            trackSequencers.append(trackSequencer)
        }
        
        state = .started

        midiClock.start { [weak self] timeInterval in
            self?.update(timeInterval: timeInterval)
        }
        
        for trackSequencer in trackSequencers {
            trackSequencer.start()
        }
    }
    
    public func stop() {
        midiClock.stop()
        for trackSequencer in trackSequencers {
            trackSequencer.start()
        }

        nextClockTimeInterval = 0
        clockCounter = 0
        state = .stopped
    }

    func update(timeInterval: TimeInterval) {
        // TODO: Can we unify these two cases?
        if nextClockTimeInterval == 0 {
            incrementMidiClock(timeInterval: timeInterval)
            nextClockTimeInterval = timeInterval + midiFile.clockDuration
        } else if timeInterval >= nextClockTimeInterval {
            incrementMidiClock(timeInterval: timeInterval)
            nextClockTimeInterval += midiFile.clockDuration
        } else {
            // keep waiting
        }
    }
    
    private func incrementMidiClock(timeInterval: TimeInterval) {
        clockCounter += 1
        for trackSequencer in trackSequencers {
            guard trackSequencer.state == .started else { continue }
            trackSequencer.update(clockCounter: clockCounter, timeInterval: timeInterval)
        }
    }
}

extension MidiSequencer: MidiTrackSequencerDelegate {
    func midiTrackSequencer(_ midiTrackSequencer: MidiTrackSequencer, midiEventTime: MidiEventTime, channel: Int, keyDown index: Int) {
        delegate?.midiSequencer(self, midiEventTime: midiEventTime, channel: Int(channel), keyDown: index)
    }
    
    func midiTrackSequencerKeyUp(_ midiTrackSequencer: MidiTrackSequencer, midiEventTime: MidiEventTime, channel: Int) {
        delegate?.midiSequencerKeyUp(self, midiEventTime: midiEventTime, channel: Int(channel))
    }
}

extension WaveSynthesizer: MidiSequencerDelegate {
    public func midiSequencer(_ midiSequencer: MidiSequencer, didChangeState state: MidiSequencer.State) {
        for (_, voice) in voices {
            voice.keyUp()
        }
    }
    
    public func midiSequencer(_ midiSequencer: MidiSequencer, midiEventTime: MidiEventTime, channel: Int, keyDown index: Int) {
        guard channel < voices.count else { return }
        let voices = Array(voices.values)
        let voice = voices[channel]
        let note = Notes.note(at: index)
        voice.frequency.note = note
        voice.keyDown()
        print("\(midiEventTime.description) voice: \(channel) keyDown: \(index) note: \(note.description)")
    }

    public func midiSequencerKeyUp(_ midiSequencer: MidiSequencer, midiEventTime: MidiEventTime, channel: Int) {
        guard channel < voices.count else { return }
        let voices = Array(voices.values)
        let voice = voices[channel]
        voice.keyUp()
        print("\(midiEventTime.description) voice: \(channel) keyUp")
    }
}
