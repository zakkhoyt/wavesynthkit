//
//  MidiTrackSequencer.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/12/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

protocol MidiTrackSequencerDelegate: AnyObject {
    func midiTrackSequencer(_ midiTrackSequencer: MidiTrackSequencer, midiEventTime: MidiEventTime, channel: Int, keyDown index: Int)
    func midiTrackSequencerKeyUp(_ midiTrackSequencer: MidiTrackSequencer, midiEventTime: MidiEventTime, channel: Int)
}

public struct MidiEventTime: CustomStringConvertible {
    public let clocks: UInt32
    public let eventClocks: UInt32
    public let duration: TimeInterval
    public var eventDuration: TimeInterval?
    
    public var description: String {
        let eventDurationString: String = {
            guard let eventDuration else { return "-" }
            return "\(eventDuration)"
        }()
        return "\(duration), eventDuration: \(eventDurationString), clocks: \(clocks) eventClocks: \(eventClocks)"
    }
}

public class MidiTrackSequencer {
    // MARK: EventTime
    
    // TODO: EventTime and MidiEventTime share a lot. Can we union them?
    private class EventTime: CustomStringConvertible {
        let index: Int
        let deltaTime: TimeInterval
        let expected: TimeInterval
        var actual: TimeInterval?
        init(
            index: Int,
            deltaTime: TimeInterval,
            expected: TimeInterval
        ) {
            self.index = index
            self.deltaTime = deltaTime
            self.expected = expected
        }
        
        var description: String {
            if let actual {
                if deltaTime == 0 {
                    return "index: \(index) deltaTime: \(deltaTime) expected: \(expected) actual: \(actual) difference: \(actual - expected) percent: n/a"
                } else {
                    let percent = (actual - expected) / deltaTime
                    return "index: \(index) deltaTime: \(deltaTime) expected: \(expected) actual: \(actual) difference: \(actual - expected) percent: \(percent)"
                }
            } else {
                return "index: \(index) deltaTime: \(deltaTime) expected: \(expected)"
            }
        }
    }

    private var eventTimes: [EventTime] = []
    private func buildEventTimes() {
        eventTimes.removeAll()
        var total: TimeInterval = 0
        for (i, event) in track.events.enumerated() {
            total += clockDuration * TimeInterval(event.deltaTime)
            eventTimes.append(EventTime(index: i, deltaTime: TimeInterval(event.deltaTime), expected: total))
        }
    }

    private func printEventTimes() {
        for (index, eventTime) in eventTimes.enumerated() {
            print("[\(index)] \(eventTime.description)")
        }
    }

    // MARK: State
    
    public enum State: CustomStringConvertible {
        case stopped
        case started
        
        public var description: String {
            switch self {
            case .stopped: return "Stopped"
            case .started: return "Started"
            }
        }
    }

    public private(set) var state: State = .stopped
    
    // MARK: Public vars
    
    public var isMuted = false {
        didSet {
            if isMuted, let previousKeyDownChannel {
                // Be sure that the key goes up. Future events will be ignored until !isMuted
                let midiEventTime = MidiEventTime(
                    clocks: clockCounter,
                    eventClocks: eventClockCounter,
                    duration: Date().timeIntervalSince1970 - firstTimeInterval
                )
                delegate.midiTrackSequencerKeyUp(self, midiEventTime: midiEventTime, channel: previousKeyDownChannel)
            }
        }
    }
    
    // MARK: Private vars

    private let clockDuration: TimeInterval
    private var clockCounter: UInt32 = 0
    private var eventClockCounter: UInt32 = 0
    
    private var numberOfClocks: UInt32 = 0
    private var trackEventIndex = 0
    private let track: Midi.File.TrackPayload
    
    private let delegate: MidiTrackSequencerDelegate
    private var previousKeyDownChannel: Int?
    private var firstTimeInterval: TimeInterval = 0
    init(
        track: Midi.File.TrackPayload,
        clockDuration: TimeInterval,
        delegate: MidiTrackSequencerDelegate
    ) {
        self.track = track
        self.clockDuration = clockDuration
        self.delegate = delegate
        buildEventTimes()
    }
    
    func start() {
        state = .started
    }
    
    func stop() {
        state = .stopped
        firstTimeInterval = 0
    }
    
    func update(clockCounter: UInt32, timeInterval: TimeInterval) {
        if firstTimeInterval == 0 {
            firstTimeInterval = timeInterval
        }
        eventClockCounter += 1
        guard state == .started else { return }
        guard !isMuted else { return }
        
        // start at nextIndex and work toward the ned looking for midi events
        for index in trackEventIndex..<track.events.count {
            let event = track.events[index]
            
            if eventClockCounter >= event.deltaTime {
                // Do event then prepare for next event
                if let midiEvent = event.midiEvent as? Midi.File.TrackPayload.TimeEvent.MidiEvent,
                   let payload = midiEvent.eventPayload as? Midi.File.TrackPayload.TimeEvent.MidiEvent.MidiChannelVoiceMessagePayload {
                    let duration = timeInterval - firstTimeInterval
                    let eventDuration = TimeInterval(event.deltaTime) * clockDuration
//                    let eventDurationString = String(format: "%.3f", eventDuration)
                    func executeEvent() {
                        let midiEventTime = MidiEventTime(
                            clocks: clockCounter,
                            eventClocks: eventClockCounter,
                            duration: duration,
                            eventDuration: eventDuration
                        )
                        switch payload {
                        case .noteOn(let tuple):
                            delegate.midiTrackSequencer(self, midiEventTime: midiEventTime, channel: Int(tuple.channel), keyDown: Int(tuple.key))
                            previousKeyDownChannel = Int(tuple.channel)
                        case .noteOff(let tuple):
                            delegate.midiTrackSequencerKeyUp(self, midiEventTime: midiEventTime, channel: Int(tuple.channel))
                        default:
                            break
                        }
                    }
                    executeEvent()
                    eventClockCounter = 0
                }
            } else {
                // Keep Waiting
                
                trackEventIndex = index
                
                return
            }
        }
        Logger.debug("No more track midi events to process ")
        stop()
    }
}
