//
//  MidiEvent.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Midi.File.TrackPayload.TimeEvent {
    public final class MidiEvent: MidiTrackEvent, DataParsable {
        public var description: String {
            // return "eventType: \(eventType?.description ?? "-"), eventPayload: \(eventPayload?.description ?? "-")"
            eventPayload?.description ?? "-"
        }
        
        public enum MidiChannelVoiceMessageType: UInt8, MidiChannelMessage {
            case noteOff = 0x80
            case noteOn = 0x90
            case polyphonicKeyPressure = 0xA0
            case controllerChange = 0xB0
            case programChange = 0xC0
            case channelKeyPressure = 0xD0
            case pitchBend = 0xE0
            
            var lengthInBytes: Int {
                switch self {
                case .noteOff: return 3
                case .noteOn: return 3
                case .polyphonicKeyPressure: return 3
                case .controllerChange: return 3
                case .programChange: return 2
                case .channelKeyPressure: return 2
                case .pitchBend: return 3
                }
            }
            
            //                    public var description: String {
            //                        return String(describing: self).capitalized
            //                    }
            
            public var description: String {
                switch self {
                case .noteOff: return "noteOff"
                case .noteOn: return "noteOn"
                case .polyphonicKeyPressure: return "polyphonicKeyPressure"
                case .controllerChange: return "controllerChange"
                case .programChange: return "programChange"
                case .channelKeyPressure: return "channelKeyPressure"
                case .pitchBend: return "pitchBend"
                }
            }
        }

        public enum MidiChannelVoiceMessagePayload: MidiChannelPayload {
            case noteOff((channel: UInt8, key: UInt8, velocity: UInt8))
            case noteOn((channel: UInt8, key: UInt8, velocity: UInt8))
            case polyphonicKeyPressure((channel: UInt8, key: UInt8, pressure: UInt8))
            case controllerChange((channel: UInt8, message: MidiVoiceControllerChangeMessage, controllerValue: UInt8))
            case programChange((channel: UInt8, programNumber: MidiInstrument))
            case channelKeyPressure((channel: UInt8, pressure: UInt8))
            case pitchBend((channel: UInt8, lsb: UInt8, msb: UInt8))

            init(noteOff data: Data) {
                self = .noteOff((data[0] & 0x0F, data[1], data[2]))
            }

            init(noteOn data: Data) {
                self = .noteOn((data[0] & 0x0F, data[1], data[2]))
            }

            init(polyphonicKeyPressure data: Data) {
                self = .polyphonicKeyPressure((data[0] & 0x0F, data[1], data[2]))
            }

            init?(controllerChange data: Data) {
                guard data.count >= 3 else {
                    Logger.warn("Data not long enough to convert to controllerChange")
                    return nil
                }
                
                let messageByte = data[1]
                guard let message = MidiVoiceControllerChangeMessage(rawValue: messageByte) else {
                    Logger.warn("Unknown kcontroller change message payload: \(messageByte.hexString)")
                    return nil
                }
                self = .controllerChange((data[0] & 0x0F, message, data[2]))
            }

            init(programChange data: Data) {
                let instrument = MidiInstrument(rawValue: data[1]) ?? .acousticGrandPiano
                self = .programChange((data[0] & 0x0F, instrument))
            }

            init(channelKeyPressure data: Data) {
                self = .channelKeyPressure((data[0] & 0x0F, data[1]))
            }

            init(pitchBend data: Data) {
                self = .pitchBend((data[0] & 0x0F, data[1], data[2]))
            }

            public var description: String {
                switch self {
                case .noteOff(let tuple): return "Note Off: channel=\(tuple.channel) key=\(tuple.key) velocity=\(tuple.velocity)"
                case .noteOn(let tuple): return "Note On: channel=\(tuple.channel) key=\(tuple.key) velocity=\(tuple.velocity)"
                case .polyphonicKeyPressure(let tuple): return "Polyphonic Key Pressure: channel=\(tuple.channel) key=\(tuple.key) pressure=\(tuple.pressure)"
                case .controllerChange(let tuple): return "Controller Change channel=\(tuple.channel) controllerNumber=\(tuple.message.description) controllerValue=\(tuple.controllerValue)"
                case .programChange(let tuple): return "Program Change: channel=\(tuple.channel) programNumber=\(tuple.programNumber.description)"
                case .channelKeyPressure(let tuple): return "Channel Key Pressure: channel=\(tuple.channel) pressure=\(tuple.pressure)"
                case .pitchBend(let tuple): return "Pitch Bend: channel=\(tuple.channel) lsb=\(tuple.lsb) msb=\(tuple.msb)"
                }
            }
        }
        
        public enum MidiChannelModeMessageType: UInt8, MidiChannelMessage {
            case allSoundOff = 0x78
            case resetAllControllers = 0x79
            case localControl = 0x7A
            case allNotesOff = 0x7B
            case omniModeOn = 0x7C
            case omniModeOff = 0x7D
            case monoModeOn = 0x7E
            case polyModeOn = 0x7F
            
            public var description: String {
                switch self {
                case .allSoundOff: return "allSoundOff"
                case .resetAllControllers: return "resetAllControllers"
                case .localControl: return "localControl"
                case .allNotesOff: return "allNotesOff"
                case .omniModeOn: return "omniModeOn"
                case .omniModeOff: return "omniModeOff"
                case .monoModeOn: return "monoModeOn"
                case .polyModeOn: return "polyModeOn"
                }
            }
        }
        
        public enum MidiChannelModeMessagePayload: MidiChannelPayload {
            case allSoundOff(UInt8)
            case resetAllControllers(UInt8)
            case localControl((channel: UInt8, value: UInt8))
            case allNotesOff(UInt8)
            case omniModeOn(UInt8)
            case omniModeOff(UInt8)
            case monoModeOn((channel: UInt8, numberOfChannels: UInt8))
            case polyModeOn(UInt8)

            init(allSoundOff data: Data) {
                self = .allSoundOff(data[0] & 0x0F)
            }
            
            init(resetAllControllers data: Data) {
                self = .resetAllControllers(data[0] & 0x0F)
            }
            
            init(localControl data: Data) {
                self = .localControl((data[0] & 0x0F, data[2]))
            }

            init(allNotesOff data: Data) {
                self = .allNotesOff(data[0] & 0x0F)
            }

            init(omniModeOn data: Data) {
                self = .omniModeOn(data[0] & 0x0F)
            }

            init(omniModeOff data: Data) {
                self = .omniModeOff(data[0] & 0x0F)
            }

            init(monoModeOn data: Data) {
                self = .monoModeOn((data[0] & 0x0F, data[2]))
            }

            init(polyModeOn data: Data) {
                self = .polyModeOn(data[0] & 0x0F)
            }

            public var description: String {
                switch self {
                case .allSoundOff(let channel): return "All Sound Off: channel \(channel)"
                case .resetAllControllers(let channel): return "Reset All Controllers: channel \(channel)"
                case .localControl(let tuple): return "Local Control: channel \(tuple.0) value: \(tuple.1)"
                case .allNotesOff(let channel): return "All Notes Off: channel \(channel)"
                case .omniModeOn(let channel): return "Omni Mode On: channel \(channel)"
                case .omniModeOff(let channel): return "Omni Mode Off: channel \(channel)"
                case .monoModeOn(let tuple): return "Mono Mode On: channel \(tuple.0) numberOfChannels: \(tuple.numberOfChannels)"
                case .polyModeOn(let channel): return "Poly Mode On: channel \(channel)"
                }
            }
        }
        
        var index: Int
        var data: Data
        var lengthInBytes = 0
        
        public private(set) var eventType: MidiChannelMessage?
        public private(set) var eventPayload: MidiChannelPayload?
        
        init?(data: Data) {
            self.data = data
            self.index = 0
            
            let byte: UInt8 = readByte()
            let statusByte = byte & 0xF0
            
            if let eventType = MidiChannelVoiceMessageType(rawValue: statusByte) {
                self.eventType = eventType
                switch eventType {
                case .noteOff: self.eventPayload = MidiChannelVoiceMessagePayload(noteOff: data)
                case .noteOn: self.eventPayload = MidiChannelVoiceMessagePayload(noteOn: data)
                case .polyphonicKeyPressure: self.eventPayload = MidiChannelVoiceMessagePayload(polyphonicKeyPressure: data)
                case .controllerChange: self.eventPayload = MidiChannelVoiceMessagePayload(controllerChange: data)
                case .programChange: self.eventPayload = MidiChannelVoiceMessagePayload(programChange: data)
                case .channelKeyPressure: self.eventPayload = MidiChannelVoiceMessagePayload(channelKeyPressure: data)
                case .pitchBend: self.eventPayload = MidiChannelVoiceMessagePayload(pitchBend: data)
                }
                self.lengthInBytes = eventType.lengthInBytes
            } else if let eventType = MidiChannelModeMessageType(rawValue: byte) {
                self.eventType = eventType
                switch eventType {
                case .allSoundOff: self.eventPayload = MidiChannelModeMessagePayload(allSoundOff: data)
                case .resetAllControllers: self.eventPayload = MidiChannelModeMessagePayload(resetAllControllers: data)
                case .localControl: self.eventPayload = MidiChannelModeMessagePayload(localControl: data)
                case .allNotesOff: self.eventPayload = MidiChannelModeMessagePayload(allNotesOff: data)
                case .omniModeOn: self.eventPayload = MidiChannelModeMessagePayload(omniModeOn: data)
                case .omniModeOff: self.eventPayload = MidiChannelModeMessagePayload(omniModeOff: data)
                case .monoModeOn: self.eventPayload = MidiChannelModeMessagePayload(monoModeOn: data)
                case .polyModeOn: self.eventPayload = MidiChannelModeMessagePayload(polyModeOn: data)
                }
                self.lengthInBytes = 3
            } else {
                Logger.error("Failed to cast statusBute to MidiVoiceChannelMessage")
                return nil
            }
        }
    }
}
