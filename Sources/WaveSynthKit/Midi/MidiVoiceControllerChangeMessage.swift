//
//  ControllerChangeMessage.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/10/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public enum MidiVoiceControllerChangeMessage: UInt8, CustomStringConvertible {
    case bankSelectCoarse = 0x00
    case modulationCoarse = 0x01
    case breathControlCoarse = 0x02
    case footControllerCoarse = 0x04
    case portamentoCoarse = 0x05
    case dataEntryCoarse = 0x06
    case channelVolumneCoarse = 0x07
    case balanceCoarse = 0x08
    case panCoarse = 0x0A
    case expressionCoarse = 0x0B
    case effect1Coarse = 0x0C
    case effect2Coarse = 0x0D
    case generalPurposeController1Coarse = 0x10
    case generalPurposeController2Coarse = 0x11
    case generalPurposeController3Coarse = 0x12
    case generalPurposeController4Coarse = 0x13
    
    case bankSelectFine = 0x20
    case modulationFine = 0x21
    case breathControlFine = 0x22
    case footControllerFine = 0x24
    case portamentoFine = 0x25
    case dataEntryFine = 0x26
    case channelVolumneFine = 0x27
    case balanceFine = 0x28
    case panFine = 0x2A
    case expressionFine = 0x2B
    case effect1Fine = 0x2C
    case effect2Fine = 0x2D
    case generalPurposeController1Fine = 0x30
    case generalPurposeController2Fine = 0x31
    case generalPurposeController3Fine = 0x32
    case generalPurposeController4Fine = 0x33
    
    case sustainOnOff = 0x40
    case portamentoOnOff = 0x41
    case sustenutoOnOff = 0x42
    case softOnOff = 0x43
    case legatoOnOff = 0x44
    case holdOnOff = 0x45
    
    case soundController1 = 0x46
    case soundController2 = 0x47
    case soundController3 = 0x48
    case soundController4 = 0x49
    case soundController5 = 0x4A
    case soundController6 = 0x4B
    case soundController7 = 0x4C
    case soundController8 = 0x4D
    case soundController9 = 0x4E
    case soundController10 = 0x4F
    case generalPurposeController5 = 0x50
    case generalPurposeController6 = 0x51
    case generalPurposeController7 = 0x52
    case generalPurposeController8 = 0x53
    
    case portamentoSourceNote = 0x54
    
    case effect1Depth = 0x5B
    case effect2Depth = 0x5C
    case effect3Depth = 0x5D
    case effect4Depth = 0x5E
    case effect5Depth = 0x5F
    
    case nonRegisteredParameterFine = 0x62
    case nonRegisteredParameterCoarse = 0x63
    case registeredParameterFine = 0x64
    case registeredParameterCoarse = 0x65
    
    case allSoundsOff = 0x78
    case resetAllControllers = 0x79
    case localControlOnOff = 0x7A
    case allNotesOff = 0x7B
    case omniModeOffAllNotesOff = 0x7C
    case omniModeOnAllNotesOff = 0x7D
    case polyModeOffAllNotesOff = 0x7E
    case polyModeOnAllNotesOff = 0x7F
    
    case undefined = 0xFF
    
    public var description: String {
        switch self {
        case .bankSelectCoarse: return "Bank Select (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .modulationCoarse: return "Modulation (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .breathControlCoarse: return "Breath Control (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .footControllerCoarse: return "Foot Controller (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .portamentoCoarse: return "Portamento (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .dataEntryCoarse: return "Data Entry (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .channelVolumneCoarse: return "Channel Volumne (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .balanceCoarse: return "Balance (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .panCoarse: return "Pan (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .expressionCoarse: return "Expression (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .effect1Coarse: return "Effect1 (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .effect2Coarse: return "Effect2 (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController1Coarse: return "Beneral Purpose Controller1 (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController2Coarse: return "Beneral Purpose Controller2 (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController3Coarse: return "Beneral Purpose Controller3 (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController4Coarse: return "Beneral Purpose Controller4 (Coarse) (\(rawValue) \(rawValue.hexString))"
            
        case .bankSelectFine: return "Bank Select (Fine) (\(rawValue) \(rawValue.hexString))"
        case .modulationFine: return "Modulation (Fine) (\(rawValue) \(rawValue.hexString))"
        case .breathControlFine: return "Breath Control (Fine) (\(rawValue) \(rawValue.hexString))"
        case .footControllerFine: return "Foot Controller (Fine) (\(rawValue) \(rawValue.hexString))"
        case .portamentoFine: return "Portamento (Fine) (\(rawValue) \(rawValue.hexString))"
        case .dataEntryFine: return "Data Entry (Fine) (\(rawValue) \(rawValue.hexString))"
        case .channelVolumneFine: return "Channel Volumne (Fine) (\(rawValue) \(rawValue.hexString))"
        case .balanceFine: return "Balance (Fine) (\(rawValue) \(rawValue.hexString))"
        case .panFine: return "Pan (Fine) (\(rawValue) \(rawValue.hexString))"
        case .expressionFine: return "Expression (Fine) (\(rawValue) \(rawValue.hexString))"
        case .effect1Fine: return "Effect1 (Fine) (\(rawValue) \(rawValue.hexString))"
        case .effect2Fine: return "Effect2 (Fine) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController1Fine: return "Beneral Purpose Controller1 (Fine) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController2Fine: return "Beneral Purpose Controller2 (Fine) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController3Fine: return "Beneral Purpose Controller3 (Fine) (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController4Fine: return "Beneral Purpose Controller4 (Fine) (\(rawValue) \(rawValue.hexString))"
            
        case .sustainOnOff: return "Sustain On/Off (\(rawValue) \(rawValue.hexString))"
        case .portamentoOnOff: return "Portamento On/Off (\(rawValue) \(rawValue.hexString))"
        case .sustenutoOnOff: return "Sustenuto On/Off (\(rawValue) \(rawValue.hexString))"
        case .softOnOff: return "Soft On/Off (\(rawValue) \(rawValue.hexString))"
        case .legatoOnOff: return "Legato On/Off (\(rawValue) \(rawValue.hexString))"
        case .holdOnOff: return "Hold On/Off (\(rawValue) \(rawValue.hexString))"
            
        case .soundController1: return "Sound Controller1 (\(rawValue) \(rawValue.hexString))"
        case .soundController2: return "Sound Controller2 (\(rawValue) \(rawValue.hexString))"
        case .soundController3: return "Sound Controller3 (\(rawValue) \(rawValue.hexString))"
        case .soundController4: return "Sound Controller4 (\(rawValue) \(rawValue.hexString))"
        case .soundController5: return "Sound Controller5 (\(rawValue) \(rawValue.hexString))"
        case .soundController6: return "Sound Controller6 (\(rawValue) \(rawValue.hexString))"
        case .soundController7: return "Sound Controller7 (\(rawValue) \(rawValue.hexString))"
        case .soundController8: return "Sound Controller8 (\(rawValue) \(rawValue.hexString))"
        case .soundController9: return "Sound Controller9 (\(rawValue) \(rawValue.hexString))"
        case .soundController10: return "Sound Controller10 (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController5: return "General Purpose Controller5 (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController6: return "General Purpose Controller6 (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController7: return "General Purpose Controller7 (\(rawValue) \(rawValue.hexString))"
        case .generalPurposeController8: return "General Purpose Controller8 (\(rawValue) \(rawValue.hexString))"
            
        case .portamentoSourceNote: return "Portamento Source Note (\(rawValue) \(rawValue.hexString))"
        case .effect1Depth: return "Effect 1 Depth (\(rawValue) \(rawValue.hexString))"
        case .effect2Depth: return "Effect 2 Depth (\(rawValue) \(rawValue.hexString))"
        case .effect3Depth: return "Effect 3 Depth (\(rawValue) \(rawValue.hexString))"
        case .effect4Depth: return "Effect 4 Depth (\(rawValue) \(rawValue.hexString))"
        case .effect5Depth: return "Effect 5 Depth (\(rawValue) \(rawValue.hexString))"
            
        case .nonRegisteredParameterFine: return "Non-Registered Parameter (Fine) (\(rawValue) \(rawValue.hexString))"
        case .nonRegisteredParameterCoarse: return "Non-RegisteredParameter (Coarse) (\(rawValue) \(rawValue.hexString))"
        case .registeredParameterFine: return "Registered Parameter (Fine) (\(rawValue) \(rawValue.hexString))"
        case .registeredParameterCoarse: return "Registered Parameter (Coarse) (\(rawValue) \(rawValue.hexString))"
            
        case .allSoundsOff: return "All Sounds Off (\(rawValue) \(rawValue.hexString))"
        case .resetAllControllers: return "Reset All Controllers (\(rawValue) \(rawValue.hexString))"
        case .localControlOnOff: return "Local Control On/Off (\(rawValue) \(rawValue.hexString))"
        case .allNotesOff: return "All Notes Off (\(rawValue) \(rawValue.hexString))"
        case .omniModeOffAllNotesOff: return "Omni Mode Off All Notes Off (\(rawValue) \(rawValue.hexString))"
        case .omniModeOnAllNotesOff: return "Omni Mode On All Notes Off (\(rawValue) \(rawValue.hexString))"
        case .polyModeOffAllNotesOff: return "Poly Mode Off All Notes Off (\(rawValue) \(rawValue.hexString))"
        case .polyModeOnAllNotesOff: return "Poly Mode On All Notes Off (\(rawValue) \(rawValue.hexString))"
            
        case .undefined: return "Undefined"
        }
    }
}
