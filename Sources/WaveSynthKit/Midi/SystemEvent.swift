//
//  SystemEvent.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Midi.File.TrackPayload.TimeEvent {
    public final class SystemEvent: MidiTrackEvent, DataParsable {
        public var description: String {
            "TODO"
        }
        
        public enum SystemEventType: UInt8, CustomStringConvertible {
            case startOfExclusive = 0xF0
            case escape = 0xF7
            
            public var description: String {
                switch self {
                case .startOfExclusive: return "startOfExclusive"
                case .escape: return "escape"
                }
            }
        }
        
        var index: Int
        var data: Data
        
        public private(set) var lengthInBytes = 0
        
        public private(set) var eventType: SystemEventType?
        public private(set) var length: VariableLengthQuantity?
        public private(set) var payload: Data?
        required init?(data: Data) {
            self.data = data
            self.index = 0
            
            let byte = readByte()
            
            guard let eventType = SystemEventType(rawValue: byte) else {
                Logger.warn("Failed to cast byte to SystemEventType")
                return nil
            }
            self.eventType = eventType
            
            guard let length = readVariableLengthQuantity() else {
                Logger.error("Failed to readVariableLengthQuantity")
                return nil
            }
            self.length = length
            
            guard let payload = read(count: Int(length)) else {
                Logger.error("Failed to extract payload")
                return nil
            }
            self.payload = payload
            
            self.lengthInBytes = index
        }
    }
}
