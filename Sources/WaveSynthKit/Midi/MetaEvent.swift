//
//  MetaEvent.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class TimeSignature: CustomStringConvertible {
    // (numerator: UInt8, denominator: UInt8, clocksPerTick: UInt8, thirtySecondNotes: UInt8)
    let numerator: UInt8
    let denominator: UInt8
    let clocksPerTick: UInt8
    let thirtySecondNotes: UInt8
    let ratioString: String
    
    init(
        numerator: UInt8,
        denominator: UInt8,
        clocksPerTick: UInt8,
        thirtySecondNotes: UInt8
    ) {
        self.numerator = numerator
        self.denominator = denominator
        self.clocksPerTick = clocksPerTick
        self.thirtySecondNotes = thirtySecondNotes
        
        let denominatorUnpacked = pow(2, Int(denominator))
        self.ratioString = "\(numerator)/\(denominatorUnpacked)"
    }

    public var description: String {
        "Time Signature: \(ratioString) numerator=\(numerator), denominator=2^\(denominator), clocksPerTick=\(clocksPerTick), 32nd notes per 24 clocks=\(thirtySecondNotes) "
    }
    
    static func `default`() -> TimeSignature {
        TimeSignature(numerator: 4, denominator: 2, clocksPerTick: 24, thirtySecondNotes: 8)
    }
}

public class Tempo: CustomStringConvertible {
    public let rate: UInt24
    public let bpm: UInt32
    
    init(rate: UInt24, bpm: UInt32) {
        self.rate = rate
        self.bpm = bpm
    }
    
    public var description: String {
        "value=\(rate) bpm=\(bpm)"
    }
    
    static func `default`() -> Tempo {
        // TODO: Verify value
        Tempo(rate: 500000, bpm: 120)
    }
}

extension Midi.File.TrackPayload.TimeEvent {
    public final class UnknownEvent: MidiTrackEvent, DataParsable {
        var index: Int
        
        var data: Data
        
        init?(data: Data) {
            self.data = data
            self.index = 0
        }
        
        public var description: String {
            "Unknown \(data.count) bytes"
        }
    }
}

extension Midi.File.TrackPayload.TimeEvent {
    public final class MetaEvent: MidiTrackEvent, DataParsable {
        public var description: String {
            eventPayload?.description ?? ""
        }

        public enum MetaEventType: UInt8, CustomStringConvertible {
            case sequenceNumber = 0x00
            case textEvent = 0x01
            case copywriteNotice = 0x02
            case trackName = 0x03
            case instrumentName = 0x04
            case lyric = 0x05
            case marker = 0x06
            case cuePoint = 0x07
            
            case midiChannelPrefix = 0x20
            case midiPort = 0x21
            case endOfTrack = 0x2F
            case setTempo = 0x51
            case stmpeOffset = 0x54
            case timeSignature = 0x58
            case keySignature = 0x59
            case sequencerSpecificMetaEvent = 0x7F
            
            public var description: String {
                switch self {
                case .sequenceNumber: return "sequenceNumber"
                case .textEvent: return "textEvent"
                case .copywriteNotice: return "copywriteNotice"
                case .trackName: return "trackName"
                case .instrumentName: return "instrumentName"
                case .lyric: return "lyric"
                case .marker: return "marker"
                case .cuePoint: return "cuePoint"
                case .midiChannelPrefix: return "midiChannelPrefix"
                case .midiPort: return "midiPort"
                case .endOfTrack: return "endOfTrack"
                case .setTempo: return "setTempo"
                case .stmpeOffset: return "stmpeOffset"
                case .timeSignature: return "timeSignature"
                case .keySignature: return "keySignature"
                case .sequencerSpecificMetaEvent: return "sequencerSpecificMetaEvent"
                }
            }
        }
        
        /// The lenght as read from the file (this is variable and can only be determined by reading it)
        
        //                    public let length: VariableLengthQuantity
        
        public enum MetaEventPayload: CustomStringConvertible {
            case sequenceNumber(UInt16)
            case textEvent(String)
            case copywriteNotice(String)
            case trackName(String)
            case instrumentName(String)
            case lyric(String)
            case marker(String)
            case cuePoint(String)
            case midiChannelPrefix(UInt8)
            case midiPort(UInt8)
            case endOfTrack
            case setTempo(Tempo)
            case smtpeOffset((hours: UInt8, minutes: UInt8, seconds: UInt8, frames: UInt8, fractionalFrame: UInt8))
            // case timeSignature((numerator: UInt8, denominator: UInt8, clocksPerTick: UInt8, thirtySecondNotes: UInt8))
            case timeSignature(TimeSignature)
            case keySignature((sharpsFlats: UInt8, key: UInt8))
            case sequencerSpecificMetaEvent((identifier: UInt32, data: UInt8))

            init(sequenceNumber data: Data) {
                let sequenceNumber: UInt16 = data.unpack()
                self = .sequenceNumber(sequenceNumber)
            }
            
            init?(textEvent data: Data) {
                guard let text = String(data: data, encoding: .ascii) else { return nil }
                self = .textEvent(text)
            }

            init?(copywriteNotice data: Data) {
                guard let text = String(data: data, encoding: .ascii) else { return nil }
                self = .copywriteNotice(text)
            }

            init?(trackName data: Data) {
                guard let text = String(data: data, encoding: .ascii) else { return nil }
                self = .trackName(text)
            }

            init?(instrumentName data: Data) {
                guard let text = String(data: data, encoding: .ascii) else { return nil }
                self = .instrumentName(text)
            }

            init?(lyric data: Data) {
                guard let text = String(data: data, encoding: .ascii) else { return nil }
                self = .lyric(text)
            }

            init?(marker data: Data) {
                guard let text = String(data: data, encoding: .ascii) else { return nil }
                self = .marker(text)
            }

            init?(cuePoint data: Data) {
                guard let text = String(data: data, encoding: .ascii) else { return nil }
                self = .cuePoint(text)
            }

            init(midiChannelPrefix data: Data) {
                let byte: UInt8 = data[0..<1].unpack()
                self = .midiChannelPrefix(byte)
            }

            init(midiPort data: Data) {
                let byte: UInt8 = data[0..<1].unpack()
                self = .midiPort(byte)
            }

            init(endOfTrack data: Data?) {
                self = .endOfTrack
            }
            
            init(setTempo data: Data) {
                var paddedData = data
                paddedData.append(0x00)
                let value: UInt24 = paddedData.unpackUInt24()
                let bpm = UInt32((Double(1000000) / Double(value)) * 60)
                self = .setTempo(Tempo(rate: value, bpm: bpm))
            }
            
            init(smtpeOffset data: Data) {
                self = .smtpeOffset((data[0], data[1], data[2], data[3], data[4]))
            }
            
            init(timeSignature data: Data) {
                self = .timeSignature(TimeSignature(numerator: data[0], denominator: data[1], clocksPerTick: data[2], thirtySecondNotes: data[3]))
            }
            
            init(keySignature data: Data) {
                self = .keySignature((data[0], data[1]))
            }
            
            init?(sequencerSpecificMetaEvent data: Data) {
                let idLength = data.count - 1
                let identifier: UInt32
                if idLength == 0 {
                    // 8 bit ID
                    identifier = 0
                } else if idLength == 1 {
                    // 8 bit ID
                    identifier = UInt32(data[0])
                } else if idLength == 2 {
                    // 24 bit ID
                    let u16: UInt16 = data[0..<2].unpack()
                    identifier = UInt32(u16)
                } else if idLength == 3 {
                    // 24 bit ID
                    identifier = data[0..<3].unpackUInt24()
                } else if idLength == 4 || idLength == 5 {
                    // 24 bit ID
                    identifier = data[0..<4].unpack()
                } else {
                    Logger.warn("failed to exract identifier")
                    return nil
                }
                
                let dataByte = data[data.count - 1]
                
                self = .sequencerSpecificMetaEvent((identifier, dataByte))
            }
            
            public var description: String {
                switch self {
                case .sequenceNumber(let sequenceNumber): return "Sequence Number: \(sequenceNumber)"
                case .textEvent(let text): return "Text Event: \(text)"
                case .copywriteNotice(let text): return "Copywrite Notice: \(text)"
                case .trackName(let text): return "Track Name: \(text)"
                case .instrumentName(let text): return "Instrument Name: \(text)"
                case .lyric(let text): return "Lyric: \(text)"
                case .marker(let text): return "Marker: \(text)"
                case .cuePoint(let text): return "Cue Point: \(text)"
                case .midiChannelPrefix(let prefix): return "Prefix: \(prefix)"
                case .midiPort(let port): return "Midi Port: \(port)"
                case .endOfTrack: return "End of Track"
                case .setTempo(let tempo): return "Set Tempo: \(tempo.description)"
                case .smtpeOffset(let tuple): return "SMTPE Offset: \(tuple.hours)h \(tuple.minutes)m \(tuple.seconds)s \(tuple.frames)f \(tuple.fractionalFrame)ff"
                case .timeSignature(let timeSignature): return "Time Signature: \(timeSignature.description)"
                case .keySignature(let tuple): return "Key Signature: \(tuple.key) \(tuple.sharpsFlats)"
                case .sequencerSpecificMetaEvent(let tuple): return "Sequencer Specified Meta Event: identifier=\(tuple.identifier) \(tuple.data)"
                }
            }
        }
        
        public private(set) var lengthInBytes = 0
        public let eventType: MetaEventType
        // TODO: Remove optional
        public var eventPayload: MetaEventPayload?
        
        var index = 0
        var data: Data
        init?(data: Data) {
            self.data = data
            self.index = 0
            
            let typeValue = data[0]
            index += 1
            
            // TODO: 0x21 unrecognized
            guard let eventType = MetaEventType(rawValue: typeValue) else {
                Logger.warn("Unknown event type")
                return nil
            }
            self.eventType = eventType
            
            guard let length = readVariableLengthQuantity() else {
                Logger.error("Failed ot read varable length")
                return nil
            }
            self.lengthInBytes = Int(length) + 2
            
            func readPayloadData() -> Data? {
                if length == 0 { return Data() }
                return read(count: Int(length))
            }
            guard let payloadData = readPayloadData() else {
                // print("reached end of data")
                Logger.error("reached end of data")
                return nil
            }
            
            switch eventType {
            case .sequenceNumber: self.eventPayload = MetaEventPayload(sequenceNumber: payloadData)
            case .textEvent: self.eventPayload = MetaEventPayload(textEvent: payloadData)
            case .copywriteNotice: self.eventPayload = MetaEventPayload(copywriteNotice: payloadData)
            case .trackName: self.eventPayload = MetaEventPayload(trackName: payloadData)
            case .instrumentName: self.eventPayload = MetaEventPayload(instrumentName: payloadData)
            case .lyric: self.eventPayload = MetaEventPayload(lyric: payloadData)
            case .marker: self.eventPayload = MetaEventPayload(marker: payloadData)
            case .cuePoint: self.eventPayload = MetaEventPayload(cuePoint: payloadData)
            case .midiChannelPrefix: self.eventPayload = MetaEventPayload(midiChannelPrefix: payloadData)
            case .midiPort: self.eventPayload = MetaEventPayload(midiPort: payloadData)
            case .endOfTrack: self.eventPayload = MetaEventPayload(endOfTrack: payloadData)
            case .setTempo: self.eventPayload = MetaEventPayload(setTempo: payloadData)
            case .stmpeOffset: self.eventPayload = MetaEventPayload(smtpeOffset: payloadData)
            case .timeSignature: self.eventPayload = MetaEventPayload(timeSignature: payloadData)
            case .keySignature: self.eventPayload = MetaEventPayload(keySignature: payloadData)
            case .sequencerSpecificMetaEvent: self.eventPayload = MetaEventPayload(sequencerSpecificMetaEvent: payloadData)
            }
        }
    }
}
