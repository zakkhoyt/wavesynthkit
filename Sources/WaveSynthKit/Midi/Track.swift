//
//  Midi+Track.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/7/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Midi.File {
    public class TrackPayload: DataParsable, MidiChunkPayload {
        public class TimeEvent: CustomStringConvertible {
            public enum EventType: CustomStringConvertible {
                case midi
                case sysex
                case meta
                case unknown // TODO: Can we remove this?
                
                public var description: String {
                    switch self {
                    case .midi: return "Midi Event"
                    case .sysex: return "System Event"
                    case .meta: return "Meta Event"
                    case .unknown: return "Unknown Event"
                    }
                }
            }
            
            public let eventType: EventType
            public let deltaTime: UInt32
            public let midiEvent: MidiTrackEvent
            
            public var description: String {
                "eventType: \(eventType.description), midiEvent: \(midiEvent.description), deltaTime: \(deltaTime.description)"
            }
            
            init(
                deltaTime: UInt32,
                midiEvent: MidiTrackEvent
            ) {
                self.eventType = {
                    switch midiEvent {
                    case is MidiEvent: return .midi
                    case is SystemEvent: return .sysex
                    case is MetaEvent: return .meta
                    default: return .unknown
                    }
                }()
                self.deltaTime = deltaTime
                self.midiEvent = midiEvent
            }
        }
        
        var index: Int
        var data: Data
        
        public private(set) var events: [TimeEvent] = []
        public private(set) var lengthInClocks: UInt32 = 0
        
        required init?(data: Data) {
            self.data = data
            self.index = 0
            
            while let pair = readPair() {
                let event = TimeEvent(deltaTime: pair.0, midiEvent: pair.1)
                lengthInClocks += event.deltaTime
                Logger.debug("Appending event #\(events.count): \(event.description)")
                events.append(event)

                guard index < data.count else {
                    Logger.debug("Reached the end of track events")
                    break
                }
            }
        }
        
        private func readPair() -> (UInt32, MidiTrackEvent)? {
            guard let deltaTime = readVariableLengthQuantity() else {
                Logger.error("Failed ot read varable length")
                return nil
            }
            guard let event = readEvent() else {
//                Logger.error("Failed to read event")
                Logger.warn("Failed to read event")
                return nil
            }
            return (deltaTime, event)
        }
        
        private var runningStatusByte: UInt8?
        
        private func readEvent() -> MidiTrackEvent? {
            let byte: UInt8 = readByte()
            let statusByte = byte & 0xF0

            if statusByte >= 0x80 && statusByte <= 0xE0 {
                index -= 1
                guard let event = TimeEvent.MidiEvent(data: Data(data[index..<data.count])) else {
                    Logger.error("Failed to parse MidiEvent")
                    return nil
                }
                index += event.lengthInBytes
                // TODO: WE need a way to cancel the runningStatusByte
                runningStatusByte = statusByte
                return event
            } else if byte >= 0x78 && byte <= 0x7F {
                index -= 1
                guard let event = TimeEvent.MidiEvent(data: Data(data[index..<data.count])) else {
                    Logger.error("Failed to parse MidiEvent")
                    return nil
                }
                index += event.lengthInBytes
                return event
            } else if byte == 0xF0 || byte == 0xF7 {
                guard let event = TimeEvent.SystemEvent(data: Data(data[index..<data.count])) else {
                    Logger.warn("Failed to parse SystemEvent")
                    return nil
                }
                index += event.lengthInBytes
                return event
            } else if byte == 0xFF {
                guard let event = TimeEvent.MetaEvent(data: Data(data[index..<data.count])) else {
                    return nil
                }
                index += event.lengthInBytes
                return event
            } else if let statusByte = runningStatusByte, statusByte >= 0x80, statusByte <= 0xE0 {
                // Finally see if we have a running status byte.
                // https://www.csie.ntu.edu.tw/~r92092/ref/midi/midi_messages.html#running
                index -= 1
                var eventData = Data(data[index..<data.count])
                eventData.insert(statusByte, at: 0)
                guard let event = TimeEvent.MidiEvent(data: eventData) else {
                    Logger.error("Failed to parse MidiEvent")
                    return nil
                }
                index += event.lengthInBytes - 1
                return event
            } else {
                Logger.warn("Failed to extract any type of event byte: \(byte.hexString) statusByte: \(statusByte.hexString)")
                return Midi.File.TrackPayload.TimeEvent.UnknownEvent(data: Data(data[index..<data.count]))
            }
        }
    }
}
