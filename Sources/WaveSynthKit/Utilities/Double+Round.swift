//
//  Double+Round.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/23/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Double {
    public func round(nearest: Double) -> Double {
        let n = 1 / nearest
        let numberToRound = self * n
        return numberToRound.rounded() / n
    }
}
