//
//  Displayable.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/22/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Mirror {
    public static func reflectInstances<T>(
        of target: Any,
        matchingType type: T.Type = T.self
    ) -> [T] {
        Mirror(reflecting: target)
            .children
            .compactMap {
                guard let value = $0.value as? T else {
                    return nil
                }
                return value
            }
    }

    public static func reflectProperties<T>(
        of target: Any,
        matchingType type: T.Type = T.self
    ) -> [(String, T)] {
        Mirror(reflecting: target)
            .children
            .compactMap {
                guard let label = $0.label, let value = $0.value as? T else {
                    return nil
                }
                return (label, value)
            }
    }
}
