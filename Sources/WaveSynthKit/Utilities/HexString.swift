//
//  HexString.swift
//  Nightlight
//
//  Created by Zakk Hoyt on 8/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

extension BinaryFloatingPoint {
    var hexString: String {
        let intensity = Int(self)
        return String(format: "0x%02X", intensity)
    }
}

extension FixedWidthInteger where Self: CVarArg {
    var hexString: String {
        let size = MemoryLayout<Self>.size
        let formatString = "0x%0\(size)X"
        return String(format: formatString, self)
    }

    var binaryString: String {
        let bytes = UInt(MemoryLayout<Int>.size)
        let bits = bytes * 8
        let binaryString = String(self, radix: 2)
        func pad(_ string: String, length: UInt) -> String {
            var padded = string
            for _ in 0..<(length - UInt(string.count)) {
                padded = "0" + padded
            }
            return padded
        }
        return "0b" + pad(binaryString, length: bits)
    }

    var boolString: String {
        self > 0 ? "true" : "false"
    }
}
