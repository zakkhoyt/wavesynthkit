//
//  PLockArray.swift
//  SyncrhonziedApp
//
//  Created by Zakk Hoyt on 5/20/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class PLockArray<Element: Equatable> {
    private var array: [Element] = []
    private let lock = PRWLock()
    
    public func append(_ newElement: Element) {
        defer { lock.unlockWrite() }
        lock.lockWrite()
        array.append(newElement)
    }
    
    public func remove(at index: Int) {
        defer { lock.unlockWrite() }
        lock.lockWrite()
        array.remove(at: index)
    }
    
    public var count: Int {
        defer { lock.unlockRead() }
        lock.lockRead()
        return array.count
    }
    
    public var first: Element? {
        defer { lock.unlockRead() }
        lock.lockRead()
        return array.first
    }
    
    public func first(where predicate: (Element) throws -> Bool) rethrows -> Element? {
        defer { lock.unlockRead() }
        lock.lockRead()
        return try array.first(where: predicate)
    }
    
    public subscript(index: Int) -> Element {
        set {
            defer { lock.unlockWrite() }
            lock.lockWrite()
            array[index] = newValue
        }
        get {
            defer { lock.unlockRead() }
            lock.lockRead()
            return array[index]
        }
    }
    
    public func enumerated() -> EnumeratedSequence<[Element]> {
        defer { lock.unlockRead() }
        lock.lockRead()
        return array.enumerated()
    }
    
    public func removeAll() {
        defer { lock.unlockWrite() }
        lock.lockWrite()
        array.removeAll()
    }
    
    public func contains(_ element: Element) -> Bool {
        defer { lock.unlockRead() }
        lock.lockRead()
        return array.contains(element)
    }
    
    public func contains(where predicate: (Element) throws -> Bool) rethrows -> Bool {
        defer { lock.unlockRead() }
        lock.lockRead()
        return try array.contains(where: predicate)
    }
}
