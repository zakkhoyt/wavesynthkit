//
//  FileService.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/29/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

public class FileService {
    public struct File {
        public let name: String
        public let fileSize: String
        public let url: URL
        public let createDate: Date
        
        public init(
            name: String,
            fileSize: String,
            url: URL,
            createDate: Date
        ) {
            self.name = name
            self.fileSize = fileSize
            self.url = url
            self.createDate = createDate
        }
    }
    
    class func fileExists(url: URL) -> Bool {
        FileManager.default.fileExists(atPath: url.path)
    }
    
    class func ensureDirectoryExists(url: URL) {
        if FileManager.default.fileExists(atPath: url.path) == false {
            do {
                try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: [:])
                print("Created directory at: " + url.absoluteString)
            } catch {
                print("Failed to create directory at url: " + url.absoluteString + ". " + error.localizedDescription)
            }
        }
    }
    
    @discardableResult
    class func deleteFileAt(url: URL) -> Bool {
        guard FileManager.default.fileExists(atPath: url.path) else {
            return true
        }
        do {
            try FileManager.default.removeItem(at: url)
            print("Removed file at: " + url.absoluteString)
            return true
        } catch {
            print("Failed to remove file at url: " + url.absoluteString + ". " + error.localizedDescription)
            return false
        }
    }
    
    public class func retrieveFiles(from url: URL) -> [File] {
        var output: [File] = []
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [])
            
            // Filter to log files
            let urls = directoryContents.filter { $0.pathExtension == "json" }
            
            for url in urls {
                let name = url.deletingPathExtension().lastPathComponent
                do {
                    let attributes = try FileManager.default.attributesOfItem(atPath: url.path)
                    guard let sizeNumber = attributes[FileAttributeKey.size] as? NSNumber else { continue }
                    guard let createdDate = attributes[FileAttributeKey.creationDate] as? Date else {
                        assertionFailure()
                        continue
                    }
                    let sizeInBytes = Double(sizeNumber.uint64Value)
                    let fileSize: String
                    if sizeInBytes < 1000 {
                        fileSize = String(format: "%.0f bytes", sizeInBytes)
                    } else if sizeInBytes < (1000 * 1000) {
                        let sizeInKilobytes = sizeInBytes / 1000.0
                        fileSize = String(format: "%.1f KB", sizeInKilobytes)
                    } else {
                        let sizeInMegabytes = sizeInBytes / (1000.0 * 1000.0)
                        fileSize = String(format: "%.1f MB", sizeInMegabytes)
                    }
                    output.append(File(
                        name: name,
                        fileSize: fileSize,
                        url: url,
                        createDate: createdDate
                    ))
                } catch {
                    print(error.localizedDescription)
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        let sortedByCreateDate = output.sorted { item1, item2 -> Bool in
            item1.createDate > item2.createDate
        }
        
        return sortedByCreateDate
    }
}

extension FileService {
    public static var synthDirUrl: URL {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        var url = URL(fileURLWithPath: documentsPath!)
        url.appendPathComponent("Synthesizers")
        
        return url
    }
    
    public static func ensureSynthDirExists() {
        ensureDirectoryExists(url: FileService.synthDirUrl)
    }
    
    public static func synthFiles() -> [File] {
        retrieveFiles(from: synthDirUrl)
    }
    
    @discardableResult
    public static func save(waveSynthesizer: WaveSynthesizer, name: String) -> Bool {
        ensureSynthDirExists()
        let url = synthDirUrl.appendingPathComponent(name).appendingPathExtension("json")
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(waveSynthesizer)
            try data.write(to: url)
            return true
        } catch {
            print("Failed to write waveSynthesizer to url: \(url.absoluteString)")
            return false
        }
    }
}
