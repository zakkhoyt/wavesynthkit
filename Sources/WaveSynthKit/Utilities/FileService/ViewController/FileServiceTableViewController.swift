//
//  FileServiceTableViewController.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/29/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

public protocol SaveFileServiceTableViewControllerDelegate: AnyObject {
    func fileServiceTableViewController(
        _ fileServiceTableViewController: FileServiceTableViewController,
        didSaveToUrl url: URL
    )
}

public protocol LoadFileServiceTableViewControllerDelegate: AnyObject {
    func fileServiceTableViewController(
        _ fileServiceTableViewController: FileServiceTableViewController,
        didLoadData data: Data,
        fromFile file: FileService.File
    )
}

public class FileServiceTableViewController: UITableViewController {
    public enum Intent {
        case load
        case save
    }
    
    private var data: Data?
    private var intent: Intent
    private weak var saveDelegate: SaveFileServiceTableViewControllerDelegate?
    private weak var loadDelegate: LoadFileServiceTableViewControllerDelegate?
    private var files: [FileService.File] = []
    private var cancelBarb: UIBarButtonItem?
    private var addBarb: UIBarButtonItem?
    
    init(
        saveData data: Data,
        delegate: SaveFileServiceTableViewControllerDelegate
    ) {
        self.data = data
        self.intent = .save
        self.saveDelegate = delegate
        super.init(nibName: nil, bundle: nil)
    }

    init(delegate: LoadFileServiceTableViewControllerDelegate) {
        self.intent = .load
        self.loadDelegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        guard let bundle = Bundle(identifier: "com.vaporwarewolf.WaveSynthesizer") else { return }
        let nib = UINib(nibName: FileTableViewCell.identifier, bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: FileTableViewCell.identifier)
        reloadTableView()

        cancelBarb = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelBarbAction))
        navigationItem.leftBarButtonItem = cancelBarb
        
        if intent == .save {
            addBarb = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBarbAction))
            navigationItem.rightBarButtonItem = addBarb
        }
        
        switch intent {
        case .save: navigationItem.title = "Save File"
        case .load: navigationItem.title = "Open File"
        }
    }
    
    private func reloadTableView() {
        files = FileService.synthFiles()
        tableView.reloadData()
    }
    
    @objc
    private func addBarbAction(sender: UIBarButtonItem) {
        let ac = UIAlertController(title: "Save File as...", message: "Enter a file name", preferredStyle: .alert)
        
        ac.addTextField { _ in
        }
        
        ac.addAction(UIAlertAction(title: "Save", style: .default, handler: { [weak self] _ in
            guard let safeSelf = self else { return }
            guard let textField = ac.textFields?.first else { return }
            guard let text = textField.text else { return }
            let url = FileService.synthDirUrl.appendingPathComponent(text).appendingPathExtension("json")
            safeSelf.save(data: safeSelf.data, url: url)
        }))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(ac, animated: true, completion: nil)
    }
    
    @objc
    private func cancelBarbAction(sender: UIBarButtonItem) {
        navigationController?.dismiss(animated: true, completion: nil)
    }

    private func save(data: Data?, url: URL) {
        guard let data = self.data else {
            let error = ErrorFactory.errorWith(localDescription: "Data to save is nil")
            alert(error: error)
            return
        }
        if FileService.fileExists(url: url) {
            FileService.deleteFileAt(url: url)
        }

        FileService.ensureSynthDirExists()
        
        do {
            try data.write(to: url)
            navigationController?.dismiss(animated: true, completion: { [weak self] in
                guard let safeSelf = self else { return }
                safeSelf.saveDelegate?.fileServiceTableViewController(safeSelf, didSaveToUrl: url)
            })
            return
        } catch {
            print(error.localizedDescription)
            alert(error: error)
            return
        }
    }
    
    // MARK: - Table view data source

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        files.count
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FileTableViewCell.identifier, for: indexPath) as! FileTableViewCell
        let file = files[indexPath.row]
        cell.file = file
        return cell
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file = files[indexPath.row]
        switch intent {
        case .save:
            save(data: data, url: file.url)
        case .load:
            do {
                let data = try Data(contentsOf: file.url)
                navigationController?.dismiss(animated: true, completion: { [weak self] in
                    guard let safeSelf = self else { return }
                    safeSelf.loadDelegate?.fileServiceTableViewController(safeSelf, didLoadData: data, fromFile: file)
                })
            } catch {
                print(error.localizedDescription)
                alert(error: error)
            }
        }
    }
    
    override public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        var actions: [UIContextualAction] = []
        
        let file = files[indexPath.row]
        actions.append(UIContextualAction(
            style: .destructive,
            title: "Delete",
            handler: { [weak self] _, _, completion in
                guard let safeSelf = self else { return }
                if FileService.deleteFileAt(url: file.url) {
                    safeSelf.files.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                    completion(true)
                } else {
                    completion(false)
                }
            }
        ))
        
        return UISwipeActionsConfiguration(actions: actions)
    }
}

extension FileServiceTableViewController {
    public static func presentSaveFileBrowser(
        data: Data,
        delegate: SaveFileServiceTableViewControllerDelegate,
        fromViewController from: UIViewController
    ) {
        let vc = FileServiceTableViewController(saveData: data, delegate: delegate)
        let nc = UINavigationController(rootViewController: vc)
        from.present(nc, animated: true, completion: nil)
    }
    
    public static func presentLoadFileBrowser(
        delegate: LoadFileServiceTableViewControllerDelegate,
        fromViewController from: UIViewController
    ) {
        let vc = FileServiceTableViewController(delegate: delegate)
        let nc = UINavigationController(rootViewController: vc)
        from.present(nc, animated: true, completion: nil)
    }
}

extension UIViewController {
    func alert(
        error: Error,
        completion: (() -> Void)? = nil
    ) {
        alert(title: "Error", message: error.localizedDescription, completion: completion)
    }
    
    func alert(
        title: String,
        message: String?,
        completion: (() -> Void)? = nil
    ) {
        let title = title
        let message = message
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Okay", style: .default, handler: { _ in
            completion?()
        }))
        present(ac, animated: true, completion: nil)
    }
}
