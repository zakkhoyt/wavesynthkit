//
//  FileTableViewCell.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 6/29/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

class FileTableViewCell: UITableViewCell {
    static let identifier = "FileTableViewCell"
    var file: FileService.File? {
        didSet {
            updateUI()
        }
    }

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!

    private func updateUI() {
        guard let file else { return }
        nameLabel.text = file.name
        sizeLabel.text = file.fileSize
    }
}
