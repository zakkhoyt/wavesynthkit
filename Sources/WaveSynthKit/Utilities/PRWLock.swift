//
//  PLock.swift
//  SyncrhonziedApp
//
//  Created by Zakk Hoyt on 5/20/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//
//  Adds an easy Swift wrapper around pthread_rwlock_t
//

import Foundation

public class PRWLock {
    private var lock: pthread_rwlock_t = {
        var lock = pthread_rwlock_t()
        pthread_rwlock_init(&lock, nil)
        return lock
    }()
    
    deinit {
        pthread_rwlock_destroy(&lock)
    }
    
    func lockRead() {
        pthread_rwlock_rdlock(&lock)
    }
    
    func unlockRead() {
        pthread_rwlock_unlock(&lock)
    }
    
    func lockWrite() {
        pthread_rwlock_wrlock(&lock)
    }
    
    func unlockWrite() {
        pthread_rwlock_unlock(&lock)
    }
}
