//
//  ErrorFactory.swift
//  RTPAudioEngine
//
//  Created by Zakk Hoyt on 3/29/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import Foundation

class ErrorFactory {
    private static let domain = "com.vaporwarewolf.wavesynthesizer"
    class func errorWith(localDescription: String, code: Int = 0) -> Error {
        let userInfo = [NSLocalizedDescriptionKey: localDescription]
        let error = NSError(domain: domain, code: code, userInfo: userInfo)
        return error
    }
}

extension Error {
    var code: Int { (self as NSError).code }
    var domain: String { (self as NSError).domain }
}
