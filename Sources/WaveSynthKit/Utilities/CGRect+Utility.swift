//
//  CGRect+Utility.swift
//  WaveSynthesizer-iOS
//
//  Created by Zakk Hoyt on 7/9/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

#if os(iOS) || os(tvOS)

import UIKit

extension CGRect {
    public var leftHalf: CGRect {
        CGRect(
            x: origin.x,
            y: origin.y,
            width: width / 2,
            height: height
        )
    }
    
    public var rightHalf: CGRect {
        CGRect(
            x: origin.x + width / 2,
            y: origin.y,
            width: width / 2,
            height: height
        )
    }
    
    public var topHalf: CGRect {
        CGRect(
            x: origin.x,
            y: origin.y,
            width: width,
            height: height / 2
        )
    }
    
    public var bottomHalf: CGRect {
        CGRect(
            x: origin.x,
            y: origin.y + height / 2,
            width: width,
            height: height / 2
        )
    }
    
    public var centerPoint: CGPoint {
        CGPoint(
            x: origin.x + width / 2.0,
            y: origin.y + height / 2.0
        )
    }
}
#endif
