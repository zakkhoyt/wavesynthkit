// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "WaveSynthKit",
    platforms: [
        .iOS(.v14),
        .macOS(.v12)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "WaveSynthKit",
            targets: ["WaveSynthKit"]
        ),
        .plugin(
            name: "MySwiftlintPlugin",
            targets: ["MySwiftlintPlugin"]
        )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/realm/SwiftLint", branch: "main")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "WaveSynthKit",
            dependencies: [],
            exclude: [
                "Documents/",
                "Utilities/FileService/ViewController/",
                "View/"
            ],
            // https://useyourloaf.com/blog/add-resources-to-swift-packages/
            resources: [
                // .process("Resources/Strings/Localizable.stringsdict"),
                // .copy("Resources/Fonts/Fonts.bundle"),
            ]
        ),
        .executableTarget(
            name: "WaveSynthKitTestApp",
            dependencies: [
                .target(name: "WaveSynthKit")
            ]
            // This exposes the official Swiftlint plugin (a buildTool plugin).
//            plugins: [
//                .plugin(name: "SwiftLintPlugin", package: "SwiftLint")
//            ]
        ),
        .testTarget(
            name: "WaveSynthKitTests",
            dependencies: ["WaveSynthKit"]
        ),
        // https://theswiftdev.com/beginners-guide-to-swift-package-manager-command-plugins/
        // To use:
        // swift package plugin --list
        //   swift package format-source-code
        .plugin(
            name: "MySwiftlintPlugin",
            capability: .command(
                intent: .sourceCodeFormatting(),
                permissions: [
                    .writeToPackageDirectory(reason: "This command reformats source files")
                ]
            ),
            dependencies: [
                .product(name: "swiftlint", package: "SwiftLint")
            ]
        )
    ]
)
