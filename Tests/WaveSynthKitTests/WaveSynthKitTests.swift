//
//  WaveSynthesizerTests.swift
//  WaveSynthesizerTests
//
//  Created by Zakk Hoyt on 6/8/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

@testable import WaveSynthKit
import XCTest

final class WaveSynthKitTests: XCTestCase {
//    override func setUp() {
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//
//    override func tearDown() {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//    }
    
    func runTest(
        description: String,
        duration: TimeInterval,
        work: () -> Void,
        cleanUp: (() -> Void)? = nil
    ) {
        let expectation = XCTestExpectation(description: description)
        
        work()
        
        DispatchQueue.global().asyncAfter(deadline: .now() + duration) {
            cleanUp?()
            expectation.fulfill()
        }

        // We will never get here
        wait(for: [expectation], timeout: duration + 1.0)
    }

    // }
//
    // class WaveSynthesizerTests: SyntTestCase {

    let waveSynth = WaveSynthesizer(sampleRate: 22050)
    let duration = TimeInterval(1)
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        waveSynth.start()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        waveSynth.stop()
    }

    func testToneDefault() {
        runTest(
            description: "Play default tone",
            duration: duration,
            work: {
                let voice = Voice(name: "test")
                waveSynth.addVoice(voice)
            }
        ) {}
    }

    func testNote() {
        runTest(
            description: "Play note a7",
            duration: duration,
            work: {
                let note = Notes.note(name: .a7)
                let voice = Voice(name: "test")
                voice.frequency.note = note
                waveSynth.addVoice(voice)
            }
        ) {}
    }
    
    func testADSR() {
        let voice = Voice(name: "test")
        
        runTest(
            description: "Play note c5 with adsr",
            duration: duration,
            work: {
                let note = Notes.note(name: .c5)
                    
                voice.frequency.note = note
                voice.adsr = ADSR()
                waveSynth.addVoice(voice)
                voice.keyDown()
            }
        ) {}
    }

    func keyDownUp(
        description: String,
        keyDownDuration: TimeInterval,
        keyDown: () -> Void,
        keyUpDuration: TimeInterval,
        keyUp: @escaping () -> Void,
        completion: @escaping () -> Void
    ) {
        let deadSpace: TimeInterval = 0.1
        let duration = keyDownDuration + keyUpDuration + deadSpace
        let expectation = XCTestExpectation(description: description)
        
        keyDown()
        
        DispatchQueue.global().asyncAfter(deadline: .now() + keyDownDuration) {
            keyUp()
        }
        
        DispatchQueue.global().asyncAfter(deadline: .now() + duration) {
            expectation.fulfill()
            completion()
        }
        
        // We will never get here
        wait(for: [expectation], timeout: duration)
    }
    
    func testKeyDownUp() {
        let voice = Voice(name: "test")
        voice.adsr = ADSR()
        
        let note = Notes.note(name: .c5)
        voice.frequency.note = note
        
        waveSynth.addVoice(voice)

        keyDownUp(
            description: "ADSR",
            keyDownDuration: 5.0,
            keyDown: { voice.keyDown() },
            keyUpDuration: 1.0,
            keyUp: { voice.keyUp() }
        ) {}
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

    func testParameterCoding() {
        let volume = Volume()
        volume.amplitude.value = 0.5
        do {
            let data = try JSONEncoder().encode(volume)
            let decoded = try JSONDecoder().decode(Volume.self, from: data)
            
            guard let json = decoded.jsonDescription else {
                XCTAssert(false)
                return
            }
            print("json: \(json)")
        } catch {
            XCTAssert(false, error.localizedDescription)
        }
    }
    
    func testTimedItemCoding() {
        let sweep = Pop()
        do {
            let data = try JSONEncoder().encode(sweep)
            
            do {
                let decoded = try JSONDecoder().decode(Pop.self, from: data)
                
                guard let json = decoded.jsonDescription else {
                    XCTAssert(false)
                    return
                }
                print("json: \(json)")
            } catch {
                if let jsonString = String(data: data, encoding: .utf8) {
                    print("jsonString: \(jsonString)")
                }
                XCTAssert(false, error.localizedDescription)
            }
        } catch {
            XCTAssert(false, error.localizedDescription)
        }
    }

    func testNoteCoding() {
        let sweep = Notes.note(name: .a5)
        do {
            let data = try JSONEncoder().encode(sweep)
            
            do {
                let decoded = try JSONDecoder().decode(Note.self, from: data)
                
                guard let json = decoded.jsonDescription else {
                    XCTAssert(false)
                    return
                }
                print("json: \(json)")
            } catch {
                if let jsonString = String(data: data, encoding: .utf8) {
                    print("jsonString: \(jsonString)")
                }
                XCTAssert(false, error.localizedDescription)
            }
        } catch {
            XCTAssert(false, error.localizedDescription)
        }
    }

    func testVoiceCoding() {
        let xVoice = Voice(name: "codingVoice")
        xVoice.adsr = ADSR.stringed()
        let square = Square()
        square.dutyCycle.value = 0.01
        xVoice.waveformRenderer.waveform.enumValue = .square
        
        let sweep = Sweep()
        sweep.delay.value = 0.1
        sweep.duration.value = 0
        sweep.endSteps.value = 24
        xVoice.appendOptionalEffect(effect: sweep)
        
        let sweep2 = Sweep()
        sweep2.delay.value = 0.2
        sweep2.duration.value = 0
        xVoice.appendOptionalEffect(effect: sweep2)
        
        xVoice.keyDown()
        
        do {
            let data = try JSONEncoder().encode(xVoice)
            
            do {
                let decoded = try JSONDecoder().decode(Voice.self, from: data)
                
                guard let json = decoded.jsonDescription else {
                    XCTAssert(false)
                    return
                }
                print("json: \(json)")
            } catch {
                if let jsonString = String(data: data, encoding: .utf8) {
                    print("jsonString: \(jsonString)")
                }
                XCTAssert(false, error.localizedDescription)
            }
        } catch {
            XCTAssert(false, error.localizedDescription)
        }
    }

    func testWaveSynthCoding() {
        for i in 0..<4 {
            let voice = Voice(name: "voice\(i)")
            voice.adsr = ADSR.stringed()
            let square = Square()
            square.dutyCycle.value = 0.01
            voice.waveformRenderer.waveform.enumValue = .square
            
            let sweep = Sweep()
            sweep.delay.value = 0.1
            sweep.duration.value = 0
            sweep.endSteps.value = 24
            voice.appendOptionalEffect(effect: sweep)
            
            let sweep2 = Sweep()
            sweep2.isEnabled.enumValue = .yes
            sweep2.delay.value = 0.2
            sweep2.duration.value = 0
            voice.appendOptionalEffect(effect: sweep2)
            
            voice.keyDown()
            
            waveSynth.addVoice(voice)
        }
        
        do {
            let data = try JSONEncoder().encode(waveSynth)
            
            do {
                let decoded = try JSONDecoder().decode(WaveSynthesizer.self, from: data)
                
                guard let json = decoded.jsonDescription else {
                    XCTAssert(false)
                    return
                }
                print("json: \(json)")
            } catch {
                if let jsonString = String(data: data, encoding: .utf8) {
                    print("jsonString: \(jsonString)")
                }
                XCTAssert(false, error.localizedDescription)
            }
        } catch {
            XCTAssert(false, error.localizedDescription)
        }
    }

    enum Tenum: Int, CaseIterable {
        case first
        
        case second
    }

    func testEnumParameter() {
        let curve = Curve.linear
        let parameter = EnumParameter<Curve>(name: "Curve", value: curve)
        // parameter.setValue(Curve.quadratic)
        parameter.enumValue = Curve.quadratic
    }
    
//    func testMidiFileReader() {
//        let bundle = Bundle(identifier: "com.vaporwarewolf.WaveSynthesizer")
//        guard let url = bundle?.url(forResource: "died3", withExtension: "mid") else {
//            XCTAssert(false, "Failed to locate midi file in bundle")
//            return
//        }
//
//        let file = Midi.Reader.read(url: url)
//        XCTAssert(file != nil)
//    }
}
